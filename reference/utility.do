
/***********************************************************************************************************
 Author: Patrick Liu (pyliu@uw.edu)																		
 Date: 7/13/2015
 Project: ubCov
 Purpose: Utility functions
																					
***********************************************************************************************************/

/* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Table of Contents:

General utility
- rload
- load
- stop
- list_files

Local and string manipulation
- inlist2
- str_occur
- str_split
- date_split
- localsub

ubCov utility
- ubcov_path
- var_labels
- translate

Database
- create_connection_string
- run_query
- gs_load

Cluster
- qsub
- job_hold

								 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */

/* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								 						general utility
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */


cap program drop rload
program define rload

	syntax anything

	local input `anything'

	// Prep output
	tempfile output

	// Open in R and save as CSV
	!RScript --vanilla "$central_root/modules/extract/core/addons/load.r" `input' `output'

	// Open CSV
	insheet using `output', clear

end

/////////////////////////////////////////////////////

cap program drop load
program define load

	syntax anything, [delimiter(str)]

	local path `anything'

	// Set OS
	if c(os) == "Unix" {
		local path = subinstr("`path'", "J:", "$j", .)
	}
	else if c(os) == "Windows" {
		local path = subinstr("`path'", "/home/j", "$j", .)
	}
	// Detect extension
	tokenize `path', p(".")
	local file = "`1'"
	local ext = lower("`3'")
	if "`ext'" == "" {
		di as error "Path needs a file extension"
		STOP
	}
	// Open
	if "`ext'" == "dta" {
		use "`path'", clear
	} 
	else if inlist("`ext'", "csv", "tab") {
		if mi("`delimiter'") import delimited using "`path'", clear
		else import delimited using "`path'", clear delimiters("|")
	}
	else if inlist("`ext'", "txt") {
		import delimited using "`path'", clear delim(" ")
	}
	else if inlist("`ext'", "xlsx", "xls") {
		import excel using "`path'", clear first
	}
	else if inlist("`ext'", "dbf") {
		rload "`path'"
	}
	else if inlist("`ext'", "sav") {
		cap which usespss
		if _rc {
			net from http://radyakin.org/transfer/usespss/beta
			net install usespss
		}
		qui: usespss "`path'", clear
	}
	else if inlist("`ext'", "xpt") {
		clear
		fdause `path'
	}
	else if inlist("`ext'", "da") {
		infile using "`file'.dct", using("`file'.da") clear
	}
	else {
		di as error "`ext' not accepted"
	}
	// Rename lower
	cap rename *, lower

end

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

cap program drop stop
program define stop

	syntax, msg(str) [cond(str)] [warn(integer)]

	if !mi("`cond'") {
		if `cond' local continue 1
		else local continue 0
	}
	else local continue 1
	if `continue' {
		noisily: di as error "`msg'"
		if !mi("`warn'") sleep `warn'
		else STOP
	}

end

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

cap program drop list_files
program define list_files, return

	syntax, path(str) [pattern(str) recursive full]

	// Options


	// Pattern
	if mi("`pattern'") local pattern "*"

	// Files
	local files : dir "`path'" files `pattern'
	
	// If full name
	if !mi("`full'") {
		local fullfiles
		foreach i in `files' {
			local fullfiles `fullfiles' "`path'/`i'"
		}
		local files `fullfiles'
	}

	// If recursive
	if !mi("`recursive'") {
		local dirs : dir "`path'" dirs "*"
		foreach dir in `dirs' {
			list_files, path("`path'/`dir'") pattern(`patten') `recursive' `full'
			local files `files' `r(files)'
		}
	}

	return local files `files'

end

/* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								 				 local and string manipulation
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */

cap program drop inlist2
program define inlist2, rclass

	syntax, obj(str) vals(str) [cond(str) str(str) string]

	// Set condition
	if !mi("`cond'") {
		if !inlist("`cond'", "&", "|") {
			STOP
		}
		local cond `cond'	
	}
	else {
		local cond |
	}
	
	// Generate command obj == val1 | obj == val2 | ...
	local i 1
	local cmd 1==0
	foreach val in `vals' {
		if "`str'" == "obj" local cmd `cmd' `cond' "`obj'" == `val'
		else if "`str'" == "vals" local cmd `cmd' `cond' `obj' == "`val'"
		else if !mi("`string'") local cmd `cmd' `cond' "`obj'" == "`val'"
		// Checking if obj type is the same as value type, skip if missing
		else {
			cap confirm numeric variable `obj'
			if !_rc local num_obj 1
			else local num_obj 0
			cap confirm number `val'
			if !_rc local num_val 1
			else local num_val 0
			if `num_obj' == `num_val' & `num_val' == 0 local cmd `cmd' `cond' `obj' == "`val'"
			else if `num_obj' == 0 & `num_val' ==  1 local cmd `cmd' `cond' `obj' == "`val'"
			else if `num_obj' == `num_val' & `num_val' == 1 local cmd `cmd' `cond' `obj' == `val'
		}
	local ++i
	}

	return local cmd "`cmd'"

end

//////////////////////////////////////////////////////////////////////////

cap program drop str_occur
program define str_occur, rclass

	syntax, str(str) p(str)

	local count = 0
	while regexm(`"`str'"', "`p'") {
		local str : subinstr local str "`p'" " "
		local ++count
	}

	return scalar N = `count'

end

//////////////////////////////////////////////////////////////////////////

cap program drop str_split
program define str_split, rclass

	syntax anything, p(str)

	local noparse = subinstr(`"`anything'"', "`p'", " ", .)
	local i 0
	foreach val in `noparse' {
		local ++i
		return local _`i' = "`val'"
	}

	return scalar N = `i'

end

//////////////////////////////////////////////////////////////////////////

cap program drop date_split
program define date_split

	syntax anything, format(str) [newstem(str)]

	// Setup
	local var `anything'
	local labels day month year
	local chars d m y

	// Set stem of new vars
	if !mi("`newstem'") local stem `newstem'
	else local stem `var'

	// STATA date vars
	if regexm("`format'", "%") {
		foreach label in `labels' {
			local cmd `label'(`var')
			gen `stem'_`label' = `cmd'
		}
	}

	// Otherwise if string
	else {
	// Make sure string and if not set string tempvar
	cap confirm string variable `var'
	if _rc {
		tostring `var', gen(`var'_kittens)
		local var `var'_kittens
		local temp `var'
	}
	
	// Add leading zero where needed
	local n = strlen("`format'") // Make this better
	replace `var' = "0" + `var' if strlen(`var') < `n'

	// Split
	foreach char in `chars' {
		gettoken label labels: labels
		local first = strpos("`format'", "`char'")
		local last = strlen("`format'")-strpos(strreverse("`format'"),"`char'")+1
		local length = `last'-`first'+1
		di "`char': `first' `last' `length'"
		if `first' != 0 {
			gen `stem'_`label' = substr(`var', `first', `length')
			destring `stem'_`label', replace force
		}
	}

	// Drop temp variable
	cap drop `temp'
	}

end

//////////////////////////////////////////////////////////////////////////

cap program drop localsub
program define localsub, rclass

	syntax, local(str) sub(str)

	local out
	foreach item in `local' {
		if regexm("`item'", "`sub'") {
			local out `out' `item'
		}
	}

	return local sub `out'

end

//////////////////////////////////////////////////////////////////////////

cap program drop localregexs
program define localregexs, rclass

	syntax, local(str) pattern(str)

	local out
	foreach item in `local' {
		if regexm("`item'", "`pattern'") {
			local hit = regexs(0) 
			local out `out' `hit'
		}
	}

	return local str `out'

end



/* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								 						ubcov utility
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */

cap program drop ubcov_path
program define ubcov_path

	syntax

	// Find path.csv
	local pwd = subinstr(c(pwd), "\", "/", .)
	preserve
		clear
		set obs 1
		gen root = "`pwd'"
		split root, p("ubcov_central")
		local root = root1[1] + "/ubcov_central"
	restore
	
	// Load paths.csv
	insheet using "`root'/paths.csv", clear names

	// Detect
	qui: ds
	local npath: word `c(k)' of `r(varlist)'
	while regexm("`npath'", "[a-z]") {
		local npath = regexr("`npath'", "[a-z]", "")
	}

	// Set paths as globals
	forvalues i = 1/`npath' {
	forvalues n = 1/`=_N' {
		local obj = object[`n']
		local path = path`i'[`n']
		if "`path'" != "" {
			if regexm("`path'", ",") {
				tokenize "`path'", p(" ,")
				global `obj' "${`1'}`3'"
			}
			else {
				global `obj' `path'
			}
			// OS
			if c(os) == "Unix" {
				global `obj' = subinstr("${`obj'}", "J:", "/home/j", .)
				global `obj' = subinstr("${`obj'}", "H:", "/snfs2/HOME/`c(username)'", .)
			}
			else if c(os) == "Windows" {
				global `obj' = subinstr("${`obj'}", "/home/j", "J:", .)
				global `obj' = subinstr("${`obj'}", "/snfs2/HOME/`c(username)'", "H:", .)
			}
		}
	}
	}

end

//////////////////////////////////////////////////////////////////////////

cap program drop var_labels
program define var_labels
	
	syntax [, lookfor(str)]
qui {
	// Browse
	cap drop var 
	cap drop label
	gen var = ""
	gen label = ""
	quietly ds
	local i 1
	foreach var in `r(varlist)' {
		replace var = "`var'" in `i'
		local label : variable label `var'
		replace label = "`label'" in `i'
	local ++i
	}
}

	if !mi("`lookfor'") {
		br var label if regexm(var, "`lookfor'") | regexm(label, "`lookfor'")
	}
	else br var label

end


/* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								 						  database
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */


/*
This function determines which connection string to use for sql queries.
It's intended to be used whenever a function needs to be portable,
ie, you need a function to make few assumptions about dsn and other
machine specific settings.

Usage:
create_connection_string
local conn_string = r(conn_string)

odbc, load ("select foo from baz") `conn_string'

Arguments: None required.
Optional arguments are server, database, user, password
Defaults are modeling-cod-db, shared and our readonly user/pw
Returns: r(conn_string)

Author: Joe Wagner

*/

cap program drop create_connection_string
program create_connection_string, rclass
   syntax, [server(string) database(string) ///
     user(string) password(string)]

   // set unix odbc manager
   if c(os) == "Unix" {
        set odbcmgr unixodbc
        local j = "/home/j"
        }
   else {
        local j = "J:"
   }

   preserve

   local DEFAULT_CREDENTIALS = "`j'/temp/central_comp/credentials/default.csv"
	 
   // set default arguments, if not specified
   if "`server'" == ""   local server = "modeling-cod-db"
   if "`database'" == "" local database = "shared"
   if "`user'" == "" & "`password'" == "" {
        import delimited using "`DEFAULT_CREDENTIALS'", clear varnames(1)
        qui levelsof user, local(user) cl
        qui levelsof pw, local(password) cl
   }

   // assign drivers to attempt to connect with
   local driver1 = `"MySQL ODBC 5.2 Unicode Driver"'
   local driver2 = `"MySQL ODBC 5.3 Unicode Driver"'
   
   // Loop through all connection strings and see if any work
   local passes = 0
   foreach driver in "`driver1'" "`driver2'" {

      local conn_string = `"conn("DRIVER={`driver'};"' ///
        + `"SERVER=`server'.ihme.washington.edu;DATABASE=`database';"' ///
        + `"UID=`user';PWD=`password';")"'

      cap odbc exec("select 1"), `conn_string'

      if !_rc {
         return local conn_string "`conn_string'"
		 local passes = 1
         }
	  
      }

   restore

   if `passes' != 1 {
      di as error "Invalid connection arguments supplied"
      error(999)
      }
   
end

/////////////////////////////////////////////////////////////////////////

cap program drop run_query
program define run_query

	syntax, host(str) db(str) query(str)

	create_connection_string, server(`host') database(`db')
	local con `r(conn_string)'
	# delim ;
 	odbc load, exec(`"`query'"') `con' clear;
 	# delim cr 

 end

//////////////////////////////////////////////////////////////////////////

cap program drop gs_load
program define gs_load

	syntax, key(str)

	preserve
		clear
		set obs 1
		gen blah = 1
		tempfile temp
		save `temp', replace
	restore

	local link "https://docs.google.com/spreadsheets/d/`key'/export?format=csv"
	copy "`link'" `temp'
	import delimited using `temp', clear varnames(1) stringcols(_all) charset("utf-8")

end

/* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								 						cluster utility
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */

cap program drop qsub
program define qsub

	syntax, job_name(str) script(str) slots(integer) memory(integer) [arguments(str) hold(str) cluster_project(str) logs(str)]

	local cmd qsub

	// Required
		// Job name
		local job_name -N `job_name'
		// Detect language and check if py, r, do
		tokenize "`script'", p(".")
		local lang = lower("`3'")
		if !inlist("`lang'", "py", "r", "do") {
			di as error "Unidentified language, please consult a translator"
			STOP
		}
		// Get path to shell script
		qui: ubcov_path
		local shell "$shells_root/`lang'.sh"
		// Slots
		local slots -pe multi_slot `slots'
		local memory -l mem_free=`memory'g

	// Build base command
	local cmd `cmd' `job_name' `slots' `memory'

	// Optional arguments
		// Cluster project
		if !mi("`cluster_project'") {
			local cluster_project -P `cluster_project'
			local cmd `cmd' `cluster_project'
		}
		// Hold
		if !mi("`hold'") {
			local hold -hold_jid `hold'
			local cmd `cmd' `hold'
		}
		// Logs
		if !mi("`logs'") {
			local logs -o `logs' -e `logs'
			local cmd `cmd' `logs'
		}


	// Adding script and arguments to command
		// Shell file
		local cmd `cmd' `shell'
		// Script
		local cmd `cmd' `script'
		// Arguments, first argument is path to script
		if !mi("`arguments'") {
			local cmd `cmd' `arguments'
		}

	// Submit job
	!`cmd'

end

//////////////////////////////////////////////////////////////////////////

cap program drop job_check
program define job_check, return

	syntax, job_name(str)

	// Setup temp
	tempfile temp

	// Command
	local cmd "qstat -r | grep `job_name' | wc -l >> `temp'"
	!`cmd'

	// Retrieve output
	tempname fh
	file open `fh' using "`temp'", read
	file read `fh' line
	local n `line'

	return local pass `n'
end

//////////////////////////////////////////////////////////////////////////

cap program drop job_hold
program define job_hold


	syntax, job_name(str)

	// Give it a sec to launch
	sleep 3000

	// Start timer
	timer clear 1
	timer on 1

	// Check command
	local cmd "qstat -r | grep `job_name' | wc -l"

	// Wait for job to finish
	local pass 0
	while !`pass' {
		// Check if job is done
		job_check, job_name(`job_name')
		if `r(pass)' == 0 {
			local pass 1
		}
		else {
			sleep 5000
		}
	}

	// End timer
	timer off 1
	timer list 1

end






