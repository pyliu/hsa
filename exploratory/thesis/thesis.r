

## Load packages
pacman::p_load(data.table, dplyr, ggplot2, grid, gridExtra, knitr, kableExtra)

## Settings
temp_root <- "~/Desktop/thesis/data" #"/home/j/temp/pyliu/scratch/thesis/"

## Load data
df.cov <- paste0(temp_root, "/summary/coverage.rds") %>% readRDS
df.cov <- df.cov[me_name != "vacc_dpt3time"]
df.diff <- readRDS(paste0(temp_root, '/summary/vacc_dpt1diff.rds'))
df.time <- readRDS(paste0(temp_root, '/summary/vacc_dpt3time.rds'))
df.cov <- rbind(df.cov, df.diff, df.time)

## Load figures
figs <- lapply(list.files(paste0(temp_root, "/figures"), full.names=TRUE), readRDS)
names(figs) <- list.files(paste0(temp_root, "/figures")) %>% gsub(".rds", "", .)

#--FUNCTIONS------------------------------
np <- function(type, me, year, loc) {
  if (type=="exp") df <- df.cov[me_name==me & location_name==loc & year_id==year, .(mean, lower, upper)]
  col <- c("mean", "lower", "upper")
  df <- df[, (col) := lapply(.SD, function(x) round(x*100, 1)), .SDcols=col]
  str <- paste0(df$mean, " (95% UI: ", df$lower, "-", df$upper, ")")
  return(str)
}

## Tables
## Format coverage table
t1 <- df.cov %>% copy
vacc <- c("vacc_bcg", "vacc_mcv1", "vacc_dpt3", "vacc_polio3", "vacc_hib3", "vacc_hepb3", "vacc_pcv3", "vacc_rotac")
t1 <- t1[level==3 & me_name %in% vacc & year_id %in% seq(1980, 2015, 5), .(me_name, location_name, year_id, mean, lower, upper)]
## Format coverage estimates
t1 <- t1[, est := paste0(round(mean*100, 2), " (", round(lower*100, 2), "-", round(upper*100, 2), ") ")]
t1 <- t1[, est := ifelse(mean==0 & lower == 0 & upper == 0, "---", est)]
t1 <- t1[, c("mean", "lower", "upper") := NA]
## Reshape
t1 <- dcast(t1, me_name + location_name ~ year_id, value.var="est")
## Assign
table1 <- lapply(vacc, function(x) {
  tf <- t1[me_name==x]
  tf$me_name <- NULL
  return(tf)
})
names(table1) <- vacc
