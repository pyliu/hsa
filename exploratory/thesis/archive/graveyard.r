---
title: "Graveyard"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```




## Burden analysis

### Overview

We utilized the comparative risk assessment (CRA) framework developed for the GBD to estimate the burden attributable in children under 5 to low vaccination coverage. Attributable burden can be interpreted as the reduction in current disease burden that would have been possible under a counterfactual distribution of coverage--the theoretical minimum risk level (TMREL). For vaccinations, we consider the TMREL to be the complete population coverage for a given vaccination. For newer vaccinations (Hib, Hepb, PCV, and Rota), we only calculate attributable burden after the first year of introduction (x for Hib, y for Hepb, z for PCV, and 2005 for Rota).

We do not report HepB, vaccine introduced in x and not enough countries have reached the period at which we would see an effect

Risk outcome pairs

### Effect size estimation

We estimated the effectiveness of immunizations on the mortality of vaccine preventable diseases with two general strategies: for PCV, Hib, Rota, we estimated the effectiveness through meta-analyses on effect sizes obtained from systematic reviews of the literature (inclusion-exclusion diagrams ); for DPT, MCV, because there are not recent studies on the effectiveness of the vaccinations on their respective diseases, we estimated the effectiveness by modeling their respective contribution to declines in cause-specific mortality using the GBD 2016 natural history models of the respective diseases.

Summary tables of the systematic reviews for PCV, Hib, Rota can be found. Estimates of the effect size used in this analysis can be found in Table X. 

### Estimation of population attributable fraction and attributable burden

The population attributable fraction (PAF), represents the proportion of risk that would be reduced in a given year if exposure fo the risk factor in the past were reduced to the TMREL. The $PAF_{v,d,c,t}$ is defined as:

$$ PAF_{v,d,c,t} = \frac{ \sum{RR_{v,d,c}P_{v,c,t}}  - RR_{v,c,t}}{\sum{RR_{v,d,c}P_{v,c,t}}}$$

where $PAF_{v,d,c,t}$ is the populaiton attributable fraction for cause $d$ due to low coverage of vaccination $v$ for country $c$ and year $t$. $RR_{v,d,c}$ is the relative risk (equivalent to 1- effect size) for cause $d$, vaccination $v$, and country $c$. $P_{v,c,t}$ is the proportion of children not vaccinated with vaccine $v$ in country $c$ and time $t$.

Estimates of attributable burden as deaths for each disease vaccine pair was generated using the following equation:

$$AB_{v,c,t} = \sum{Deaths_{v,d,c,t}PAF_{v,d,c,t}}$$
where $AB_{v,c,t}$ is the attributable burden for low coverage with vaccination $v$, in country $c$ and year $t$; $Deaths_{v,d,c,t}$ is the total deaths for 
