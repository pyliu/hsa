###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: SDI/HAQI effect on change in child mortality
###  
###
###				
###########################################################














#--SETUP--------------------------------------------------------------------------------

rm(list=ls())

## Setup and prep
model_root <- '/snfs2/HOME/pyliu/git/ubcov_central/modules/model'
setwd(model_root)
source('init.r')

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

file.path(j, "temp/central_comp/libraries/current/r/get_model_results.R") %>% source
file.path(j, "temp/central_comp/libraries/current/r/get_outputs.R") %>% source

## package
library(ggplot2)
library(grid)
library(gridExtra, lib=paste0(h, "/R/lib/3.3"))
library(scales)
library(ggthemes, lib="/snfs2/HOME/pyliu/R/x86_64-unknown-linux-gnu-library/3.1")
library(rje, lib="/snfs2/HOME/pyliu/R/x86_64-unknown-linux-gnu-library/3.1")
library(extrafont, lib="/snfs2/HOME/pyliu/R/x86_64-unknown-linux-gnu-library/3.1")


## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Map functions
source(paste0(code_root, "/diagnostics/map/global_map.R"))
source(db_tools)


#--PULL DATA----------------------------------------------------------------------


## PULL SDI
sdi <- get_covariates('sdi')
sdi <- sdi[, c("age_group_id", "sex_id") := NULL]


## PULL HAQI
haqi <- get_covariates('haqi')
haqi <- haqi[, c("age_group_id", "sex_id") := NULL]

## Pull PAFs
path <- "/ihme/epi/risk/bmgf/paf"
files <- list.files(path, pattern=".csv", full.names=TRUE)
pafs <- mclapply(files, function(x) {
    df <- fread(x)
    df <- df[rei_id==169, .(cause_id, location_id, year_id, sex_id, age_group_id, rei_id, mean_paf)]
    return(df)
}, mc.cores=10) %>% rbindlist


## Pull mortality rate
locs <- unique(pafs$location_id)

mort <-     get_outputs(topic='cause',
                  metric_id=3,
                  measure_id=1,
                  cause_id=cause, 
                  location_id=locs,
                  age_group_id=c(2, 3, 4, 5),
                  year_id = seq(1990, 2015, 5),
                  sex_id = c(1, 2),
                  version = 'best',
                  gbd_round_id = 3)

mort <- mort[, .(location_id, year_id, age_group_id, sex_id, cause_id, cause_name, val)]
setnames(mort, 'val', 'mt')


## Pull deaths
deaths <-     get_outputs(topic='cause',
                  metric_id=1,
                  measure_id=1,
                  cause_id=x, 
                  location_id=locs,
                  age_group_id=c(2, 3, 4, 5),
                  year_id = seq(1990, 2015, 5),
                  sex_id = c(1, 2),
                  version = 'best',
                  gbd_round_id = 3)
deaths <- deaths[, .(location_id, year_id, age_group_id, sex_id, cause_id, val)]
setnames(deaths, 'val', 'deaths')


#--CLEAN---------------------------------------------------------------------------

## Merge
df <- merge(pafs, mort, by=c("location_id", "year_id", "sex_id", "age_group_id", "cause_id"), all.x=TRUE)
df <- merge(df, deaths, by=c("location_id", "year_id", "sex_id", "age_group_id", "cause_id"), all.x=TRUE)

## Offset paf for random demo location for malaria
df <- df[location_id==177 & year_id == 2010 & cause_id == 345, mean_paf := 0.99]

## Calculate underlying mortality (mu)
df <- df[, mu := mt*(1-mean_paf)]

## Get denominator for mt
df <- df[, denom := deaths/mt]

## Get risk-deleted deaths
df <- df[, deaths_rd := deaths * (1-mean_paf)]

## Aggregate up by age, sex, location
df <- df[!is.na(mu) & mt != 0]
cols <- c("deaths", "deaths_rd", "denom")
df <- df[, (cols) := lapply(.SD, sum), .SDcols=cols, by=c("cause_id", "location_id", "year_id")]

## Recalculate mu, mt
df <- df[, mt := deaths/denom]
df <- df[, mu := deaths_rd/denom]

## Merge on SDI, HAQI
df <- merge(df, sdi, by=c("location_id", "year_id"), all.x=TRUE)
df <- merge(df, haqi, by=c("location_id", "year_id"), all.x=TRUE)

#--MODEL---------------------------------------------------------------------------

mod.fm <- "log(mu) ~ 1 + sdi + (1|location_id)" %>% as.formula
mod <- lm(mod.fm, data=df)
df <- df[, mu_hat := predict(mod, new.data=df) %>% exp]

#----------------------------------------------------------------------------------



sub.w <- dcast(sub, location_id + cause_id ~ year_id, value.var=c("mu", "mu_hat", "mt"))

## Run for each time band
years <- list(c(1995, 2000), c(2000, 2005), c(2005, 2010), c(2010, 2015), c(1990, 2000), c(2000, 2015), c(1990, 1995), c(1990, 2015))

##------Calculate % change in mu of mt due to SDI--------------------------
df.out <- lapply(years, function(i) {
    
start <- i[[1]]
end <- i[[2]]
    
sub.w <- sub.w[, delta_mu_hat := get(paste0("mu_hat_", year_start_id)) - get(paste0("mu_hat_", year_end_id))]
sub.w <- sub.w[, delta_mt := get(paste0("mt_", year_start_id)) - get(paste0("mt_", year_end_id))]
sub.w <- sub.w[, pct := delta_mu_hat/delta_mt]

out <- sub.w[, `:=` (year_start_id = start, year_end_id = end)]
out <- out[, .(location_id, cause_id, year_start_id, year_end_id, delta_mu_hat, delta_mt, pct)]
    
return(out)

}) %>% rbindlist
##--------------------------------------------------------------------------
    
## Hack for cause_id 387 which is full attribution
if (cause %in% c(387, 390)) sub <- sub[, mu := 0]; sub <- sub[, pct := 0]   

return(df.out)
    
}) %>% rbindlist