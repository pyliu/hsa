---
output:
  pdf_document:
    includes:
      in_header: header.tex
  html_document: default
---

```{r global_options, include=FALSE}
knitr::opts_chunk$set(fig.pos = 'h')
```

`r source('~/Desktop/thesis/hsa/exploratory/thesis/thesis.r')`

# Appendix

## Supplementary Figures

\listoffigures

\newpage


```{r fig.width=9, fig.height=6, echo=FALSE, fig.align="center", fig.cap='Time series estimates of complete vaccination coverage at the global and regional level from 1980-2016.'} 
grid.arrange(figs[['full_ts']])
```

\newpage

```{r fig.width=9, fig.height=6, echo=FALSE, fig.align="center", fig.cap='Time series estimates of timely DPT3 vaccination at the global and regional level from 1980-2016.'} 
grid.arrange(figs[['timely_ts']])
```

\newpage

## Supplementary Tables
\listoftables



\blandscape
\tiny

\newpage

`r kable(table1[["vacc_bcg"]], caption="Country-level vaccination coverage of BCG by 5 year period from 1980-2015")`

\newpage
`r kable(table1[["vacc_mcv1"]], caption="Country-level vaccination coverage of MCV1 by 5 year period from 1980-2015") `

\newpage
`r kable(table1[["vacc_dpt3"]], caption="Country-level vaccination coverage of DPT3 by 5 year period from 1980-2015") `

\newpage
`r kable(table1[["vacc_polio3"]], caption="Country-level vaccination coverage of Polio3 by 5 year period from 1980-2015") `

\newpage
`r kable(table1[["vacc_hepb3"]], caption="Country-level vaccination coverage of Hepb3 by 5 year period from 1980-2015") `

\newpage
`r kable(table1[["vacc_hib3"]], caption="Country-level vaccination coverage of Hib3 by 5 year period from 1980-2015") `

\newpage
`r kable(table1[["vacc_pcv3"]], caption="Country-level vaccination coverage of PCV3 by 5 year period from 1980-2015") `

\newpage
`r kable(table1[["vacc_rotac"]], caption="Country-level vaccination coverage of RotaC by 5 year period from 1980-2015") `


\elandscape