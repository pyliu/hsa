###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: 
###				
###########################################################

###################
### Setting up ####
###################
library(data.table); library(dplyr)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Path locals
setwd(paste0(j, "/WORK/01_covariates/02_inputs/hsa/code"))
code.root <- paste0(unlist(strsplit(getwd(), "hsa"))[1], "hsa/code")
paths.file <- paste0(code.root, "/paths.csv"); paths <- fread(paths.file)
source(paths[obj=="ubcov_tools", 2, with=F] %>% gsub("J:/", j, .) %>% unlist)
path_loader(paths.file)
source(ubcov_tools)
source(db_tools)

##--UTILITY-----------------------------------------------

path.clean <- function(path) {
	os <- .Platform$OS.type
	if (os == "windows") {
	  j <- "J:/"
	  h <- "H:/"
	} else {
	  j <- "/home/j/"
	  h <- paste0("/snfs2/HOME/", Sys.info()[["user"]])
	}
	#-----------------------------------------
	if (os == "windows") {
		path <- gsub("/home/j/", j, path)
		path <- gsub(paste0("/snfs2/HOME/", Sys.info()[["user"]]), h, path)
	} else {
		path <- gsub("J:/", j, path)
		path <- gsub("H:/", h, path)
	}
	return(path)
}
