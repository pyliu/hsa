###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Prep vaccination TMREL for upload to save_results
###				    Setting TMREL to 0 prior to introduction date for Hib, PCV, ROta
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr, parallel, readxl, ggplot2, boot, lme4, pscl, purrr, splines, binom)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Source 
source(db_tools)
me.db <- fread(me_db)

##--FUNCTIONS----------------------------------------------------------------------------

save.tmrel_draws <- function(df, save.path) {
  unlink(save.path)
  dir.create(save.path, showWarnings=FALSE)
  ## Save as format tmrel_[location_id]_[year_id]_[sex_id]
  locs <- unique(df$location_id)
  years <- unique(df$year_id)
  sexes <- unique(df$sex_id)
  ## Loop and save
  for (loc in locs) {
    for (year in years) {
      for (sex in sexes){
        sub <- df[location_id == loc & year_id == year & sex_id == sex]
        path <- paste("tmrel", loc, year, sex, sep="_") %>% paste0(., ".csv") %>% paste0(save.path, "/", .)
        fwrite(sub, path, row.names=F, na="")
      }
    }
  }
}

prep.tmrel_draws <- function(me, intro_year, save.root) {
  ## Grab TMREL me_id
  me_id <- me.db[me_name==me]$tmrel_me_id
  ## Create square frame with location 1, years, sexes
  age_group_ids <- get_ages(age_group_set_id = 12)$age_group_id
  years <- c(year_start:year_end) ## Loaded from path.csv
  df <- expand.grid(location_id=1, year_id= years, age_group_id=age_group_ids, sex_id=c(1:2), draw = paste0("tmrel_", 0:999))
  df <- data.table(df)
  ## Set TMREL to 0 if less than introduction year, 1 if otherwise
  df <- df[, tmrel := ifelse(year_id < intro_year, 0 , 1)]
  ## Reshape wide
  df.w <- dcast(df, location_id + year_id + age_group_id + sex_id ~ draw, value.var="tmrel")
  ## Save
  path <- paste0(save.root, "/tmrel/", me)
  save.tmrel_draws(df.w, path)
}

##--RUN----------------------------------------------------------------------------------

## From WHO/UNICEF Intro Years
prep.tmrel_draws(me="vacc_hib3", intro_year=1986, save.root=draws_root) ## 1986
prep.tmrel_draws(me="vacc_pcv3", intro_year=2000, save.root=draws_root) ## 2000
prep.tmrel_draws(me="vacc_rotac", intro_year=2006, save.root=draws_root) ## 2006