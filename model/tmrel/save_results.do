///////////////////////////////////////////////////////////
/// Author: Patrick Liu (pyliu@uw.edu)
/// Date: 
/// Project: hsa
/// Purpose: Upload draws to epi database
///				
///////////////////////////////////////////////////////////

/* -------------------------------------------------------------------------
	SETTINGS
---------------------------------------------------------------------------*/
	
// Set os as local
if c(os) == "Unix" {
local j "/home/j"
local h "/snfs2/HOME/`c(username)'"
set odbcmgr unixodbc
}
else if c(os) == "Windows" {
local j "J:"
local h "H:"
}

// Activate gbd env
!source /ihme/code/central_comp/miniconda/bin/activate gbd_env

// Load save results
run "/home/j/temp/central_comp/libraries/current/save_results.do"

// run_log and me_db
local me_db "`j'/WORK/01_covariates/02_inputs/hsa/code/reference/me_db.csv"
local run_log "`j'/WORK/01_covariates/02_inputs/hsa/code/reference/run_log.csv"
foreach file in me_db run_log {
	import delimited using ``file'', clear
	tempfile `file'
	save ``file'', replace
}

/* -------------------------------------------------------------------------
	RUN
---------------------------------------------------------------------------*/

if !mi("`1'") {
// me name
local me_name `1'
// Find me_id
use `me_db', clear
keep if me_name == "`me_name'"
local me_id = `=exp_me_id[1]'
// Get best run
use `run_log', clear
keep if me_name == "`me_name'" & is_best ==1 
if (_N == 0 | _N > 1) STOP
// Grab run_id, notes, directory
local run_id = "`=run_id[1]'"
local model_id = "`=model_id[1]'"
local data_id = "`=data_id[1]'"
local path = "`j'/temp/pyliu/scratch/draws/`me_name'"
local notes = "ST-GPR (run_id: `run_id' | model_id : `model_id' | data_id : `data_id') ||||| `=notes[1]'"
// Years
local years
forvalues i = 1980/2016 { 
	local years `years' `i' 
} 
// Override me_id
if !mi("`2'") local me_id `2'
di "`me_name' `me_id' `years' `path' `notes' `me_id'"

// Save Results
save_results, modelable_entity_id(`me_id') years(`years') sexes(1 2) ///
	in_dir(`path') file_pattern("{location_id}.csv") ///
	description(`notes') mark_best("yes") env("prod")
}
