///////////////////////////////////////////////////////////
/// Author: Patrick Liu (pyliu@uw.edu)
/// Date: 
/// Project: hsa
/// Purpose: Upload draws to epi database
///				
///////////////////////////////////////////////////////////

/* -------------------------------------------------------------------------
	SETTINGS
---------------------------------------------------------------------------*/
	
// Set os as local
if c(os) == "Unix" {
local j "/home/j"
local h "/snfs2/HOME/`c(username)'"
set odbcmgr unixodbc
}
else if c(os) == "Windows" {
local j "J:"
local h "H:"
}

// Activate gbd env
!source /ihme/code/central_comp/miniconda/bin/activate gbd_env

// Load save results
run "/home/j/temp/central_comp/libraries/current/save_results.do"

// run_log and me_db
local me_db "`j'/WORK/01_covariates/02_inputs/hsa/code/reference/me_db.csv"
local run_log "`j'/WORK/01_covariates/02_inputs/hsa/code/reference/run_log.csv"
foreach file in me_db run_log {
	import delimited using ``file'', clear
	tempfile `file'
	save ``file'', replace
}

// save root
local root = "`j'/temp/pyliu/scratch/draws/"

/* -------------------------------------------------------------------------
	RUN
---------------------------------------------------------------------------*/

if !mi("`1'") {
//--------------------------------------------------------------------------
// Arguments
local type `1'
local me_name `2'
// Years
local years
forvalues i = 1980/2016 { 
	local years `years' `i' 
}

////////////////////////////////////////////////////////////////////////////
if "`type'" == "exp" {
	// Find me_id
	use `me_db', clear
	keep if me_name == "`me_name'"
	if (_N == 0 | _N > 1) STOP
	local me_id = `=exp_me_id[1]'
	// Override me_id
	if !mi("`3'") local me_id `3'
	local path = "`root'/exp/`me_name'"
	// Get best run
	use `run_log', clear
	keep if me_name == "`me_name'" & is_best ==1 
	if (_N == 0 | _N > 1) STOP
	// Grab run_id, notes, directory
	local run_id = "`=run_id[1]'"
	local model_id = "`=model_id[1]'"
	local data_id = "`=data_id[1]'"
	local notes = "ST-GPR (run_id: `run_id' | model_id : `model_id' | data_id : `data_id') | `=notes[1]'"
	// Save Results
	save_results, modelable_entity_id(`me_id') years(`years') sexes(1 2) ///
	in_dir(`path') file_pattern("{location_id}.csv") ///
	description(`notes') mark_best("yes") env("prod")
}
////////////////////////////////////////////////////////////////////////////
if "`type'" == "rr" {
	// Find me_id
	use `me_db', clear
	keep if me_name == "`me_name'"
	if (_N == 0 | _N > 1) STOP
	local me_id = "`=rr_me_id[1]'"
	local path = "`root'/rr/`me_name'"
	// Save results
	save_results, modelable_entity_id(`me_id') years(`years') sexes(1 2) ///
	in_dir(`path') risk_type(rr) ///
	description("Temp RR for `me_name'") mark_best("yes") env("prod")
}
////////////////////////////////////////////////////////////////////////////
if "`type'" == "tmrel" {
	// Find me_id
	use `me_db', clear
	keep if me_name == "`me_name'"
	if (_N == 0 | _N > 1) STOP
	local me_id = "`=tmrel_me_id[1]'"
	local path = "`root'/tmrel/`me_name'"
	// Save results
	save_results, modelable_entity_id(`me_id') years(`years') sexes(1 2) ///
	in_dir(`path') risk_type(tmrel) ///
	description("Custom TMREL for `me_name'") mark_best("yes") env("prod")
}
//-----------------------------------------------------------------------------
}
