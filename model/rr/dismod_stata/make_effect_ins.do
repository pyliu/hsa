clear all

local prjfolder	$prjfolder
//local datafile	$datafile

di "`prjfolder'"
local studycovs $studycovs
local prevalencecc $prevalencecc

local integrands incidence mtall mtexcess mtother mtspecific mtstandard mtwith prevalence relrisk remission

insheet using "`prjfolder'/data_in.csv", comma case clear
//use "`datafile'", clear




levelsof super,local(supers) 
levelsof region, local(regions) 
levelsof subreg, local(subregs) 
levelsof integrand, local(integs) c
foreach integ of local integs {
	levelsof super if integrand == "`integ'" & super != "none",local(`integ's) 
	levelsof region if integrand == "`integ'" & region != "none",local(`integ'r) 
	levelsof subreg if integrand == "`integ'" & subreg !="none",local(`integ'sb) 
	
}

di "`integs'"
foreach integ of local integs {
	local `integ'sc
	foreach cov in `studycovs' {
		tab `cov' if integrand == "`integ'"
		if `r(r)' > 1 local `integ'sc "``integ'sc' `cov'"
	}	
}

di "`incidencesc'"

*/
clear
set obs 1
gen integrand = ""
gen effect = ""
gen name = ""
local i 0
foreach integ of local integrands {
	local i = `i' + 1
	set obs `i'
	replace integrand = "`integ'" in `i'
}
local i 0
foreach area in super region subreg gamma {
	local i = `i' + 1
	replace effect = "`area'" in `i'
}
local i 0
foreach area in super region subreg none cycle {
	local i = `i' + 1
	replace name = "`area'" in `i'
}
fillin integrand effect name
drop _fillin
drop if integrand == "" | name == "" | effect == ""
drop if effect == "gamma" & (name == "cycle" | name == "none")
//drop if name == effect
drop if (name == "region" | name == "super" | name == "subreg") & (effect == "region" | effect == "super" | effect == "subreg")
gen lower = "1" if effect == "gamma"
gen upper = "1" if effect == "gamma"
gen mean = "1" if effect == "gamma"
gen std = "inf"
replace lower = "0" if name == "none" | name == "cycle"
replace upper = "0" if name == "none" | name == "cycle"
replace mean = "0" if name == "none" | name == "cycle"
tempfile main
save `main'

clear
set obs 1
gen integrand = ""
gen effect = ""
gen name = ""
gen lower = ""
gen upper = ""
gen mean = ""
gen std = ""
local i 0
qui foreach integ of local integs {
	foreach area of local `integ's {
		local i = `i' + 1
		set obs `i'
		replace integrand = "`integ'" 	in `i'
		replace effect = "super" 		in `i'
		replace name = "`area'" 		in `i'
		replace lower = "-2" 			in `i'
		replace upper = "2" 			in `i'
		replace mean = "0" 				in `i'
		replace std = "1" 			in `i'
	}
		foreach area of local `integ'r {
		local i = `i' + 1
		set obs `i'
		replace integrand = "`integ'" 	in `i'
		replace effect = "region" 		in `i'
		replace name = "`area'" 		in `i'
		replace lower = "-2" 			in `i'
		replace upper = "2" 			in `i'
		replace mean = "0" 				in `i'
		replace std = "1" 			in `i'
	}
		foreach area of local `integ'sb {
		local i = `i' + 1
		set obs `i'
		replace integrand = "`integ'" 	in `i'
		replace effect = "subreg" 		in `i'
		replace name = "`area'" 		in `i'
		replace lower = "-2" 			in `i'
		replace upper = "2" 			in `i'
		replace mean = "0" 				in `i'
		replace std = "1" 				in `i'
	}
}
append using `main'
save `main', replace

clear
set obs 1
gen integrand = ""
gen effect = ""
gen name = ""
gen lower = ""
gen upper = ""
gen mean = ""
gen std = ""
local i 0
qui foreach integ of local integs {
		local i = `i' + 1
		set obs `i'
		replace integrand = "`integ'"	in `i'
		replace effect = "zcov" 		in `i'
		replace name = "x_ones" 		in `i'
		replace lower = "0" 			in `i'
		replace upper = "1" 			in `i'
		replace mean = "0.1" 			in `i'
		replace std = "inf" 			in `i'
		
		foreach cov of local `integ'sc {
			local i = `i' + 1
			set obs `i'
			replace integrand = "`integ'"	in `i'
			replace effect = "xcov" 		in `i'
			replace name = "`cov'"	 		in `i'
			replace lower = "-2" 			in `i'
			replace upper = "2" 			in `i'
			replace mean = "0" 				in `i'
			replace std = "inf" 			in `i'
		}
/*		foreach cov of local `integ'cc {
			local i = `i' + 1
			set obs `i'
			replace integrand = "`integ'"	in `i'
			replace effect = "xcov" 		in `i'
			replace name = "x_`cov'"	 	in `i'
			replace lower = "-2" 			in `i'
			replace upper = "2" 			in `i'
			replace mean = "0" 				in `i'
			replace std = "inf" 			in `i'
		}*/
}
append using `main'
save `main', replace
drop if name == ""
outsheet using "`prjfolder'/effect_in.csv", comma replace 

		
