clear all
cap log close


local prjfolder		$prjfolder
local datafile		$datafile
local proportion	$proportion

local mesh $mesh 


local l:list sizeof mesh
local last = word("`mesh'",`l')

if `l' > 8 local l2 `l'
else local l2 8

set obs `l2'
gen type = ""
gen age = .
replace type = "omega" in 1
replace type = "domega" in 2
replace type = "chi" in 3
replace type = "dchi" in 4
replace type = "rho" in 5
replace type = "drho" in 6
replace type = "iota" in 7
replace type = "diota" in 8

local i 0
foreach a of local mesh {
	local i = `i' + 1
	replace age = `a' in `i'
}
	
fillin type age
cap drop _fill
drop if type == "" | age == .
drop if substr(type,1,1) == "d" & age == `last'

gen lower = "0"
gen upper = "2"
gen mean = "1"
gen std = "inf"

replace lower = "_inf" if substr(type,1,1) == "d"
replace upper = "inf" if substr(type,1,1) == "d"
replace mean = "0" if substr(type,1,1) == "d"
replace std = "inf" if substr(type,1,1) == "d"


if `proportion' == 1 {
		replace upper = "0" if type == "chi" | type == "omega" | type == "rho" 	
		replace mean = "0" if type == "chi" | type == "omega" | type == "rho"		
}

outsheet using "`prjfolder'/rate_in.csv", comma replace 

