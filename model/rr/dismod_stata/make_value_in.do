clear all
clear all
cap log close


local prjfolder	$prjfolder
local datafile	$datafile
local sample_interval 	$sample_interval
local num_sample 		$num_sample

local watch_interval = `num_sample'/20
set obs 22
gen name = ""
gen value= ""

replace name = "data_like" 			in 1
replace value = "in_data_file" 		in 1
replace name = "prior_like" 		in 2
replace value = "gaussian" 			in 2
replace name = "sample_interval" 	in 3
replace value = "`sample_interval'" in 3
replace name = "num_sample" 		in 4
replace value = "`num_sample'" 		in 4
replace name = "watch_interval" 	in 5
replace value = "`watch_interval'" 	in 5
replace name = "random_seed" 		in 6
replace value = "0" 				in 6
replace name = "kappa_iota" 		in 7
replace value = "1.00E-05" 			in 7
replace name = "kappa_rho" 			in 8
replace value = "1.00E-05" 			in 8
replace name = "kappa_chi" 			in 9
replace value = "1.00E-05" 			in 9
replace name = "kappa_omega" 		in 10
replace value = "1.00E-05" 			in 10
replace name = "integrate_method" 	in 11
replace value = "eigenvector" 		in 11
replace name = "eta_mtexcess" 		in 12
replace value = "3.87E-05" 			in 12
replace name = "eta_incidence" 		in 13
replace value = "4.61E-06" 			in 13
replace name = "eta_mtall" 			in 14
replace value = "9.94E-05" 			in 14
replace name = "eta_remission" 		in 15
replace value = "0.000683" 			in 15
replace name = "eta_prevalence" 	in 16
replace value = "5.72E-05" 			in 16
replace name = "integrate_step" 	in 17
replace value = "5" 				in 17
replace name = "eta_mtspecific" 	in 18
replace value = "0.000001" 			in 18
replace name = "eta_mtother" 		in 19
replace value = "0.000001" 			in 19
replace name = "eta_mtwith" 		in 20
replace value = "0.000001" 			in 20
replace name = "eta_mtstandard" 	in 21
replace value = "0.01" 				in 21
replace name = "eta_relrisk" 		in 22
replace value = "0.01" 				in 22

outsheet using "`prjfolder'/value_in.csv", comma replace 

