clear all
clear all
cap log close


local prjfolder	$prjfolder
local datafile	$datafile


set obs 5
gen name = ""
gen lower = .
gen upper = .
gen mean = .
gen std = ""
replace name = "p_zero" if _n == 1
replace name = "xi_omega" if _n == 2
replace name = "xi_chi" if _n == 3
replace name = "xi_iota" if _n == 4
replace name = "xi_rho" if _n == 5
replace lower = 0 if name == "p_zero"
replace upper = 1 if name == "p_zero"
replace mean = 0.1 if name == "p_zero"
replace std = "inf" if name == "p_zero"
replace lower = 0.1 if name == "xi_omega"
replace upper = 0.1 if name == "xi_omega"
replace mean = 0.1 if name == "xi_omega"
replace std = "inf" if name == "xi_omega"
replace lower = 0.1 if name == "xi_chi"
replace upper = 0.1 if name == "xi_chi"
replace mean = 0.1 if name == "xi_chi"
replace std = "inf" if name == "xi_chi"
replace lower = 0.1 if name == "xi_iota"
replace upper = 10 if name == "xi_iota"
replace mean = 1 if name == "xi_iota"
replace std = "inf" if name == "xi_iota"
replace lower = 0.1 if name == "xi_rho"
replace upper = 0.1 if name == "xi_rho"
replace mean = 0.1 if name == "xi_rho"
replace std = "inf" if name == "xi_rho"

outsheet using "`prjfolder'/plain_in.csv", comma replace 




