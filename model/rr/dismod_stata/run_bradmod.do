//////////////////////////////////////////////////////////////////////////////
////
//// Purpose: Prepare and Run RRs through BradMod (DisMod without the hierarchy)
//// Author: Originally developed by Mehrdad, adapted by Marissa
//// Date: 7/3/2016
//// Project: GBD Risk Factors
//// 
//// do /home/j/WORK/05_risk/risks/metab_bmi/bmi_code/dismod/prep_in_files.do
//////////////////////////////////////////////////////////////////////////////

//////////////////
//// 1. Setup OS
//////////////////

* set j and path to the dismod suite
if c(os)=="Unix" {
	global j "/home/j"
	local dismodlink /usr/local/dismod_ode/bin
}

else {
	global j "J:"
	local dismodlink J:\temp\windismod_ode\bin
}

////////////////////
//// 2. Set Paths
////////////////////

//	local cause 	`1'
//	local risk 		`2'

local cause lri
local risk  vacc_pcv3

	local project 			`risk'_`cause'
	di in red "`cause'_`risk'"
    //sy:making new folder for keeping stuff
	//local prjfolder		"$j/WORK/05_risk/risks/metabolics/data/rr/dismod/`risk'/`cause'"
	local prjfolder		"$j/WORK/01_covariates/02_inputs/hsa/data/rr/dismod/`risk'/`cause'"

	global prjfolder 	"`prjfolder'"
	cap mkdir "`prjfolder'"
	
	local datafile 		"$j/WORK/01_covariates/02_inputs/hsa/data/rr/rr_extraction.xlsx"
	global datafile		`datafile'

	local codefolder	"$j/WORK/01_covariates/02_inputs/hsa/code/model/dismod"

///////////////////////////////
//// 3. Set DisMod Parameters
///////////////////////////////

	local sample_interval	10 // Default 10
	local num_sample		4000 // Default 4000
	global proportion 1 // Default 1
	local midmesh 0 0.1 1 5 10 20 40 70 // Default 0 20 30 40 50 60 70 80 90; I RESET IT AFTER OPENING DATA IF n <= 4
	local integrand "relrisk"
	local data_transform "gaussian"
	local data_type_pred "gaussian"
	numlist "0(10)100"
	local doses = `"`r(numlist)'"'


///////////////////////////////
//// 4. Prep data_in.csv
///////////////////////////////

import excel using "$datafile", clear firstrow

	// Clean
	drop if _n == 1
	rename study_name_short study_name
	rename acause cause
	gen sd = .
	gen sex_id = .
		replace sex_id = 1 if lower(sex)=="male"
		replace sex_id = 2 if lower(sex)=="female"
		replace sex_id = 3 if lower(sex)=="both"
	keep nid cause risk study_name year_start year_end age_start age_end sex_id mean lower upper sd
	destring mean age_start age_end year_start year_end, replace


	keep if  risk == "`risk'" & cause == `"`cause'"'

	// Clear output folder
	!rm -rf `prjfolder'
	!mkdir `prjfolder'

	// Other cleaning	
	gen meas_value = mean
	gen meas_stdev = (ln(upper) - ln(lower))/(2 * 1.96)
	replace meas_stdev = (exp( meas_stdev) - 1) * mean
	tostring meas_stdev, replace force
	gen parameter = "relrisk"
	gen age_lower = age_start
	gen age_upper = age_end
	gen time_lower = year_start
	gen time_upper = year_start
	gen integrand = "`integrand'"

	// Random Effects
	gen super = "none"
	gen region = "none"
	gen subreg = study_name

	// Fixed Effects
	cap gen x_sex = 0
	cap gen x_ones = 1
	
	// Add those required extra rows (mtall)
	local o = _N
	local age_s 0
	di `o'
	local n : list sizeof doses

	qui forval i = 1/3 {
		gettoken dose doses: doses
		local o = `o' + 1
		set obs `o'
		replace integrand = "relrisk" in `o'
		replace super = "none" in `o'
		replace region = "none" in `o'
		replace subreg = "none" in `o'
		replace time_lower = 2000 in `o'
		replace time_upper = 2000 in `o'
		replace age_lower = `age_s' in `o'
		local age_s = `age_s' + 20
		replace age_upper = `age_s' in `o'
		replace x_sex = 0 in `o'
		replace x_ones = 0 in `o'
		replace meas_value = .01 in `o'
		replace meas_stdev = "inf" in `o'
	}

	// Finally clean up the data file
	qui sum age_upper
	local maxage = r(max)
	if `maxage' > 90 local maxage 100
	replace age_upper = `maxage' in `o'
	gen data_like = "`data_transform'"

	cap gen ihme_loc_id = ""
	cap gen nid = .	
	cap keep nid ihme_loc_id super region subreg integrand data_like time_* age_* meas_* x_* lower upper
	
// Save data_in.csv
outsheet using "`prjfolder'/data_in.csv", comma replace

//////////////////////////////////////////////////////////
//// 4. Prep effect_in, rate_in, value_in, and plain_in
//////////////////////////////////////////////////////////

	local studycovs
	foreach var of varlist x_* {
					local studycovs "`studycovs' `var'"
	}
	qui sum age_upper
	global mesh `midmesh' `maxage'
	global sample_interval `sample_interval'
	global num_sample `num_sample'
	global studycovs `studycovs'
	qui:		do "`codefolder'/make_effect_ins.do"
	qui:		do "`codefolder'/make_rate_ins.do"
	qui:		do "`codefolder'/make_value_in.do"
	qui:		do "`codefolder'/make_plain_in.do"

	insheet using "`prjfolder'/rate_in.csv", comma clear case
	replace lower = "1" if type == "iota"
	replace upper = "5" if type == "iota"
	outsheet using "`prjfolder'/rate_in.csv", comma replace

	

//////////////////////////////////////////////////////////
//// 4. Prep predication frame
//////////////////////////////////////////////////////////

insheet using "$j/temp/dismod_ode/pred_in.csv", comma clear case

	if $proportion == 1 keep if integrand == "relrisk"
	foreach var of local studycovs {
			cap gen `var' = 0
	}
qui {
	gen age = age_lower
	keep if  mod(age_lower,5) == 0  | age_lower <4
	drop if age_lower>80
	replace age_upper = 0.01 if age == 0
	replace age_lower = 0.01 if age == 1
	replace age_upper = 0.1 if age == 1
	replace age_lower = 0.1 if age == 2
	replace age_upper = 1 if age == 2
	replace age_lower = 1 if age == 3
	replace age_upper = 5 if age == 3
	replace age_upper = age_lower + 5 if age_upper >=5
	replace age_upper = 5 if age_lower == 1
	replace age_upper = 100 if age_upper == 85
	replace age = age_lower
}
	gen data_like = "`data_type_pred'"

outsheet using "`prjfolder'/pred_in.csv", comma replace

	cd "`prjfolder'"
	
	! `dismodlink'/sample_post.py

*/
insheet using "$j/temp/windismod_ode/draw_in.csv", comma clear
	foreach var of local studycovs {
			cap gen `var' = 0
	}
	gen row_name = "null"
	rename x_y_2013 x_y_2015
outsheet using "`prjfolder'/draw_in.csv", comma replace

	! `dismodlink'/stat_post.py scale_beta=false	
	! `dismodlink'/data_pred data_in.csv value_in.csv plain_in.csv rate_tmp.csv effect_in.csv sample_out.csv data_pred.csv
	! `dismodlink'/data_pred pred_in.csv value_in.csv plain_in.csv rate_tmp.csv effect_in.csv sample_out.csv pred_out.csv
	! `dismodlink'/model_draw value_in.csv plain_in.csv rate_in.csv effect_in.csv sample_out.csv draw_in.csv draw_out.csv
	! `dismodlink'/predict_post.py 10
	
insheet using  "`prjfolder'/model_in.csv", comma clear
	gen data_like = "gaussian"
outsheet using  "`prjfolder'/model_in.csv", comma replace	


	! `dismodlink'/data_pred model_in.csv value_in.csv plain_in.csv rate_tmp.csv effect_in.csv sample_out.csv model_out.csv
	! `dismodlink'/plot_post.py  "`project'"
	! `dismodlink'/model_draw  draw_in.csv value_tmp.csv plain_tmp.csv rate_tmp.csv effect_tmp.csv sample_out.csv draw_out.csv

		insheet using "draw_out.csv", comma clear
				local n = _N
				keep if _n == 1 | _n > `n' - 1000
				xpose,clear
				gen id = _n
				tempfile draw_out
				save `draw_out'
		insheet using "draw_in.csv", comma clear
				gen id = _n
				merge 1:1 id using `draw_out'		
		//		rename v1 age
				drop v1
qui				forval i = 1/1000 {
						local j = `i' + 1
				//		replace v`j' = 1 - v`j' 
						rename v`j' draw`i'
				}
//				drop if age >80
				tostring age,force replace
				replace age = ".01" if age == ".0099999998"
				replace age = ".1" if age == ".1000000015"
				egen mean = rowmean(draw*)
				egen float lower = rowpctile(draw*), p(2.5)
				egen float upper = rowpctile(draw*), p(97.5)

				save reshaped_draws.dta, replace

			* Clean up drawfile for save_results
				tempfile temp
				save `temp', replace

				import delimited using "$j/WORK/01_covariates/02_inputs/hsa/data/rr/dismod/age_map.csv", clear
				tempfile age_map
				save `age_map', replace

				use `temp', clear
				merge m:1 age_lower using `age_map', nogen keep(3)
				gen sex_id = 1 if x_sex == .5
				replace sex_id = 2 if x_sex == -.5
				replace sex_id = 3 if x_sex == 0

				keep age_group_id sex_id draw* mean lower upper
				
				local counter = 0
				foreach draw_num of varlist draw* {
					rename `draw_num' draw_`counter'
					local counter = `counter' + 1
				}
				
				gen location_id = 1

				order location_id age_group_id sex_id mean lower upper

				export delimited using "`prjfolder'/draws_clean.csv", replace
				
		
				
