//////////////////////////////////////////////////////////////////////////////
////
//// Purpose: Prepare and Run RRs through BradMod (DisMod without the hierarchy)
//// Author: Originally developed by Mehrdad, adapted by Marissa
//// Date: 7/3/2016
//// Project: GBD Risk Factors
//// 
//////////////////////////////////////////////////////////////////////////////

clear all
set more off

//////////////////
//// 1. Setup OS
//////////////////

* set j and path to the dismod suite
if c(os)=="Unix" {
	global j "/home/j"
	local dismodlink /usr/local/dismod_ode/bin
}

else {
	global j "J:"
	local dismodlink J:\temp\windismod_ode\bin
}

////////////////////
//// 2. Set Paths
////////////////////

local code_root "$j/WORK/01_covariates/02_inputs/hsa/code/model/dismod"
local data_path "$j/WORK/01_covariates/02_inputs/hsa/data/rr/rr_extraction.xlsx"

//sy:the folder above got deleted, redirecting
local shell_script "$j/WORK/01_covariates/common/ubcov_central/functions/shells/do.sh"

////////////////////
//// 3. Launch
////////////////////

// Import
import excel using "`data_path'", clear firstrow
drop if _n == 1

// Get unique risk-outcome pairs
drop if risk == ""
keep risk acause
duplicates drop risk acause, force
tempfile risk_cause

// Launch bradmod
local n_row = _N
//forval i = 1/`n_row' {
	local acause = "lri" //acause[`i']
	local risk = "vacc_pcv3" //risk[`i']

	
	di "`acause' `risk'"
	!qsub -o "/home/j/temp/pyliu/logs" -P "proj_custom_models" -N `acause'_`risk' `shell_script' "`code_root'/run_bradmod.do" `acause' `risk'
//}




