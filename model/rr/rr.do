// *********************************************************************************************************************************************************************
// *********************************************************************************************************************************************************************
// Author: 	pyliu
// Date: 	3/21/2017
// Modified:		
// Project:	HSA
// Purpose:	Update relative risks for VPDs

** **************************************************************************
** RUNTIME CONFIGURATION
** **************************************************************************
// Set preferences for STATA
	// Clear memory and set memory and variable limits
		clear all
		macro drop _all
		set maxvar 32000
	// Set to run all selected code without pausing
		set more off
	// Set to enable export of large excel files
		set excelxlsxlargefile on
	// Set graph output color scheme
		set scheme s1color
	// Remove previous restores
		cap restore, not
		 set varabbrev off
		

** Set directories
	if c(os) == "Windows" {
		global j "J:"
		set mem 1g
	}
	if c(os) == "Unix" {
		global j "/home/j"
		set mem 2g
		set odbcmgr unixodbc
	}

do J:/WORK/01_covariates/02_inputs/hsa/code/reference/utility.do

local host modeling-cod-db
local db shared
local query "SELECT * FROM shared.location_hierarchy_history WHERE location_set_version_id=149"
run_query, host(`host') db(`db') query(`query')
keep ihme_loc_id super_region_id super_region_name region_id region_name
tempfile locs
save `locs', replace

** **************************************************************************
** Meta-analysis
** **************************************************************************

// Load parings
insheet using J:/WORK/01_covariates/02_inputs/hsa/code/reference/risk_outcome.csv, clear names
tempfile risk_outcome
keep if type == "metan"
local N = _N
save `risk_outcome', replace

// Load data
import excel using "J:/WORK/01_covariates/02_inputs/hsa/data/rr/rr_extraction.xlsx", clear firstrow
keep if _n != 1
destring *, replace
//gen study_name = cond(review_name != "", review_name + " | " + study_name_short, study_name_short)

***** DROP TAGGED AGE GROUPS ********
drop if drop_age == 1 | drop == 1

// Convert to effect size
foreach var in mean lower upper {
	replace `var' = (1 - `var')*100
}
rename upper lowerkittens
rename lower upperkittens
rename *kittens *

// Merge region/super_region
merge m:1 ihme_loc_id using `locs', keep(1 3)

tempfile data
save `data', replace

// Run Meta-analyses
use `risk_outcome', clear 
//local i = 4
//local risk = risk[`i']
//local acause = acause[`i']

use `data'
local risk = "vacc_hib3" 
local acause = "lri"
keep if risk == `"`risk'"' &  acause == `"`acause'"' 
if `"`risk'"' == "vacc_pcv3" {
	gen vaccine_type = dc_risk
	sort dc_risk study_type
	local vaccine_type vaccine_type 
}
else {
	local vaccine_type 
	sort study_type
}
if `"`risk'"' == "vacc_pcv3" & `"`acause'"' == "lri_pneumo" local study_name study_name_short
else local study_name study_name
metan mean lower upper, random label(namevar=`study_name') title(Outcome (`acause')) astext(70) rcols(`vaccine_type' study_type) by(acause) 

