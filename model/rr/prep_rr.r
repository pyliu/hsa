###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Prep vaccination RR for upload to save_results
###				   
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr, parallel, readxl)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Source 
source(db_tools)
me.db <- fread(me_db)

## Data and reference
df.ro <- paste0(code_root, "/reference/risk_outcome.csv") %>% fread
df.direct <- paste0(data_root, "/rr/prepped/direct.csv") %>% fread
dismod.root <- paste0(data_root, "/rr/dismod")

##--FUNCTIONS-------------------------------------------------------

make.draws <- function(df, n, meanvar, lowervar, uppervar, dist) {
  N <- length(df$mean)
  df <- df[, se := (get(uppervar)-get(lowervar))/3.92]
  if (dist=="norm") {
    draws <- matrix(rnorm(n*N, mean=df[[meanvar]], sd=df$se), nrow=N, ncol=n) %>% data.table
  } else if (dist=="lognorm") {
    mu <- df[[meanvar]]
    se <- df$se
    location <- log(mu^2 / sqrt(se^2 + mu^2))
    shape <- sqrt(log(1 + (se^2 / mu^2)))
    draws <- matrix(rlnorm(n*N, meanlog=location, sdlog=shape), nrow=N, ncol=n) %>% data.table
  } else if (dist == "logitnorm") {
    se <- (df$se)^2 / (1/(df[[meanvar]]*(1-df[[meanvar]])))^2
    draws <- matrix(rlogitnorm(n=n*N, mu=logit(df[[meanvar]]), sigma=se), nrow=N, ncol=n) %>% data.table
  }
  df <- df[, c("se", meanvar, lowervar, uppervar) := NULL]
  names(draws) <- paste0("rr_", 0:(n-1))
  df <- cbind(df, draws)
  return(df)
}

##-----------------------------------------------------------------------------

save.rr_draws <- function(df, save.path) {
  unlink(save.path)
  dir.create(save.path, showWarnings=FALSE)
  ## Save as format tmrel_[location_id]_[year_id]_[sex_id]
  locs <- unique(df$location_id)
  years <- unique(df$year_id)
  sexes <- unique(df$sex_id)
  ## Loop and save
  for (loc in locs) {
    for (year in years) {
      for (sex in sexes){
        sub <- df[location_id == loc & year_id == year & sex_id == sex]
        path <- paste("rr", loc, year, sex, sep="_") %>% paste0(., ".csv") %>% paste0(save.path, "/", .)
        fwrite(sub, path, row.names=F, na="")
      }
    }
  }
}

prep.rr_draws <- function(me) {
  ## Settings
  cols <- c("location_id", "year_id", "age_group_id", "sex_id", 
            "risk", "rei_id", "acause", "cause_id", 
            "parameter", "mortality", "morbidity")
  ## Load data
  df <- df.direct[risk==me, .(me_id, risk, rei_id, acause, cause_id, lower, mean, upper)]
  if (nrow(df)==0) stop("Misspecified me_name, returned 0 rows")
  df[, `:=` (parameter = 'cat2', mortality = 1, morbidity = 1)]
  df <- make.draws(df, 1000, "mean", "lower", "upper", "lognorm")
  ## Prep square
  square <- expand.grid(location_id=1, year_id = c(year_start: year_end), sex_id=c(1,2), age_group_id=c(2:5)) %>% data.table
  ## Apply square to each risk outcome
  df <- lapply(1:nrow(df), function(x) cbind(square, df[x])) %>% rbindlist
  ## Duplicate out for cat1, where rr = 1
  df.cat1 <- df %>% copy
  df.cat1[, parameter := 'cat1']
  cols <- grep("rr_", names(df), value=TRUE)
  df.cat1[, (cols) := 1]
  ## Rbind to df
  df <- rbind(df, df.cat1)
  ## Save
  path <- paste0(draws_root, "/rr/", me)
  save.rr_draws(df, path)
}

prep.rr_pcv <- function(me) {
  ## Load dismod estimates
    files <- list.files(paste0(dismod.root, "/", me), "pred.out.csv", recursive=TRUE, full.names=TRUE)
    df <- lapply(files, function(x) {
      df <- fread(x)
      cause <- strsplit(dirname(x), "/")[[1]] %>% tail(., n=1)
      df <- df[, `:=` (risk=me, acause=cause)]
    }) %>% rbindlist
    ## Clean
    df <- df[, .(age_lower, pred_lower, pred_median, pred_upper, risk, acause)]
    age.map <- paste0(data_root, "/rr/dismod/age_map.csv") %>% fread
    df <- merge(df, age.map, by='age_lower', all.x=TRUE)
    df <- merge(df, df.ro[, .(risk, me_id, acause, cause_id, rei_id)], by=c("risk", "acause"), all.x=TRUE)
    setnames(df, c("pred_lower", "pred_median", "pred_upper"), c("lower", "mean", "upper"))
    df <- df[, .(me_id, risk, rei_id, acause, cause_id, lower, mean, upper, age_group_id)]
    df <- make.draws(df, 1000, "mean", "lower", "upper", "lognorm")
    ## Square
    square <- expand.grid(location_id=1, year_id = c(year_start:year_end), sex_id = c(1,2)) %>% data.table
    ## Apply to each risk outcome
    df <- lapply(1:nrow(df), function(x) cbind(square, df[x])) %>% rbindlist
    df.dismod <- df %>% copy
  ## Load metan estimates
    ## Load data
    df <- df.direct[risk==me, .(me_id, risk, rei_id, acause, cause_id, lower, mean, upper)]
    if (nrow(df)==0) stop("Misspecified me_name, returned 0 rows")
    df <- make.draws(df, 1000, "mean", "lower", "upper", "lognorm")
    ## Prep square
    square <- expand.grid(location_id=1, year_id = c(year_start: year_end), sex_id=c(1,2), age_group_id=c(2:5)) %>% data.table
    ## Apply square to each risk outcome
    df <- lapply(1:nrow(df), function(x) cbind(square, df[x])) %>% rbindlist
  ## Rbind dismod (lri and pneumo) with metan (otitis)
    df <- rbind(df, df.dismod)
    df[, `:=` (parameter = 'cat2', mortality = 1, morbidity = 1)]
  ## Duplicate out for cat1, where rr = 1
    df.cat1 <- df %>% copy
    df.cat1[, parameter := 'cat1']
    cols <- grep("rr_", names(df), value=TRUE)
    df.cat1[, (cols) := 1]
    df <- rbind(df, df.cat1)
  ## Save
    path <- paste0(draws_root, "/rr/", me)
    save.rr_draws(df, path)
}

##--RUN----------------------------------------------------------

## Get RR Estimates for (DPT, MCV1/2, Hib, Rota from GBD VPDs and Stata's metan)
me_names <- unique(df.direct$risk)
me_names <- me_names[!me_names %in% "vacc_pcv3"]
lapply(me_names, prep.rr_draws)

## Get RR Estimates for PCV from dismod age trend
prep.rr_pcv("vacc_pcv3")