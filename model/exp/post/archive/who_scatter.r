###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: WHO Scatter
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr, parallel, readxl, ggplot2, boot, lme4, pscl, purrr, splines, binom)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Path locals
setwd(paste0(j, "/WORK/01_covariates/02_inputs/hsa/code"))
code.root <- paste0(unlist(strsplit(getwd(), "hsa"))[1], "hsa/code")
paths.file <- paste0(code.root, "/paths.csv"); paths <- fread(paths.file)
source(paths[obj=="ubcov_tools", 2, with=F] %>% gsub("J:/", j, .) %>% unlist)
path_loader(paths.file)

## Source
source(db_tools)

me.db <- paste0(code_root, "/reference/me_db.csv") %>% fread
locs <- get_location_hierarchy(location_set_version_id)[level>=3]

## WHo
who.est <- paste0(data_root, "/raw/who_estimates/coverage_estimates_series.xls")

## Suppress messages on readxl
read_excel <-  function(...) {
  quiet_read <- purrr::quietly(readxl::read_excel)
  out <- quiet_read(...)
  if(length(c(out[["warnings"]], out[["messages"]])) == 0)
    return(out[["result"]])
  else readxl::read_excel(...)
}


prep.who_admin <- function(me) {
  who <- me.db[me_name==me]$who_name
  df <- read_excel(who.est, sheet=who) %>% data.table
  drop <- c("Region", "Cname", "Vaccine")
  df <- df[, (drop) := NULL]
  df <- melt(df, id="ISO_code", variable.name="year_id", value.name="data")
  df$year_id <- df$year_id %>% as.character %>% as.numeric
  df <- df[, me_name := me]
  df <- df[, data := data/100]
  df <- df[!is.na(data)]
  df <- df[, cv_whoest := 1]
  setnames(df, "ISO_code", "ihme_loc_id")
  ## Duplicate Demark for Greenland
  df.grl <- df[ihme_loc_id =="DNK"] %>% copy
  df.grl <- df.grl[, ihme_loc_id := "GRL"]
  df <- rbind(df, df.grl)
  ## Drop unmapped locations
  drop.locs <- df[!(ihme_loc_id %in% locs$ihme_loc_id)]$ihme_loc_id %>% unique
  if (length(drop.locs)>0 ) {
    print(paste0("UNMAPPED LOCATIONS (DROPPING): ", toString(drop.locs)))
    df <- df[!(ihme_loc_id %in% drop.locs)]
  }
  return(df)
}

df.who <- prep.who_admin("vacc_dpt3")
setnames(df.who, "data", "whoest")
df <- paste0(data_root, "/exp/modeled/best/vacc_dpt3.rds") %>% readRDS %>% data.table
df <- merge(df, locs, by="location_id", all.x=TRUE)
df <- merge(df, df.who, by=c("ihme_loc_id", "year_id", "me_name"), all.x=TRUE)

df.vacc <- readRDS("J:/WORK/01_covariates/02_inputs/hsa/data/exp/to_model/vaccination.rds")
df.vacc$location_id <- NULL
df.vacc <- df.vacc[, .(ihme_loc_id, year_id, me_name, data, cv_admin, cv_survey, cv_lit)]
df <- merge(df, df.vacc, by=c('me_name', 'ihme_loc_id', 'year_id'), all=TRUE)


## SCATTER
years <- c(1980, 1990, 2000, 2005, 2010, 2015)
for (year in years) {
png(paste0("C:/Users/pyliu/Desktop/", year, ".png"), w=15, h=10, unit="in", res=100)
p <- ggplot(df[year_id==year]) +
  geom_point(aes(y=data, x=gpr_mean, group=super_region_name, color=region_name)) +
  geom_abline(slope=1, intercept=0) +
  theme_bw()+ 
  coord_cartesian(ylim=c(0, 1), xlim=c(0,1)) + 
  xlab(paste0("GBD Estimate (", year, ")")) + ylab(paste0("WHO Estimate (", year, ")")) +
  labs(color="Region")
print(p)
dev.off()
}


## TIME SERIES
df <- df[!is.na(location_name)]
locsub <- c("AFG","BRA", "CHN", "KHM", "MEX", "USA")
ggplot(df[ihme_loc_id %in% locsub]) +
  geom_line(aes(y=gpr_mean, x=year_id, color="GBD Estimate")) +
  geom_ribbon(aes(ymin=gpr_lower, ymax=gpr_upper, x=year_id, fill="GBD Estimate"), alpha=0.2) +
  geom_line(aes(y=whoest, x=year_id, color="WHO Estimate")) +
  geom_point(data=df[cv_admin==1 & ihme_loc_id %in% locsub & me_name=="vacc_dpt3"], aes(y=data, x=year_id, shape="Admin"), color="grey") +
  geom_point(data=df[(cv_survey==1|cv_lit==1) & ihme_loc_id %in% locsub & me_name=="vacc_dpt3"], aes(y=data, x=year_id, shape="Survey"), color="black") +
  theme_bw()+ 
  xlab("Year") + ylab("Coverage") +
  labs(shape="Data", color="Estimate") +
  facet_wrap(~location_name)
 


df <- fread("J:/temp/pyliu/sum.csv")


ggplot(df) +
  geom_line(data=df[is.na(global)], aes(y=gpr_mean, x=year_id, group=super_region_name, color=super_region_name), size=1, linetype="dashed") +
  geom_line(data=df[!is.na(global)], aes(y=gpr_mean, x=year_id, color="Global"), size=1) +
  geom_ribbon(data=df[!is.na(global)], aes(ymin=gpr_lower, ymax=gpr_upper, x=year_id), fill="gold",  alpha=0.2) +
  theme_bw()+ 
  xlab("Year") + ylab("Coverage") +
  labs(color="Location")
  