###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Crosswalks for vaccinations
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr, parallel, readxl)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Path locals
setwd(paste0(j, "/WORK/01_covariates/02_inputs/hsa/code"))
code.root <- paste0(unlist(strsplit(getwd(), "hsa"))[1], "hsa/code")
paths.file <- paste0(code.root, "/paths.csv"); paths <- fread(paths.file)
source(paths[obj=="ubcov_tools", 2, with=F] %>% gsub("J:/", j, .) %>% unlist)
path_loader(paths.file)

## Source
source(db_tools)

## Location hierarchy
locs <- get_location_hierarchy(location_set_version_id)

## Me database
me.db <- paste0(code_root, "/reference/me_db.csv") %>% fread

###########################################################################################################################
# 													  multiply out
###########################################################################################################################

############################################
## Clean up ratio model

clean_model_ratio <- function(num, denom) {
## Open
df <- paste0(data_root, "/exp/modeled/vacc_", num, "_", denom, "_ratio.rds") %>% readRDS
keep <- c("me_name", "location_id", "year_id", "age_group_id", "sex_id","gpr_mean", "gpr_lower", "gpr_upper", "data", "variance", "cv_intro")
df <- df[, keep, with=FALSE]
df <- df[!duplicated(df, by=c("location_id", "year_id", "age_group_id", "sex_id"))]
## Copy over cv_intro for CHN
cv_intro.chn <- df[location_id==354, .(cv_intro)][1]
df <- df[location_id==6, cv_intro := cv_intro.chn]
## Set 0's
df <- df[year_id < cv_intro, c("gpr_mean", "gpr_lower", "gpr_upper") := 0]
## Copy nationals to subnationals
df <- merge(df, locs[, .(location_id, ihme_loc_id)], by='location_id', all.x=TRUE)
df <- df[grepl("_", ihme_loc_id), parent_loc_id := tstrsplit(ihme_loc_id, "_", fixed=TRUE)[1]]
subnats <- df[!is.na(parent_loc_id)]$parent_loc_id %>% unique
df.p <- df[ihme_loc_id %in% subnats, .(ihme_loc_id, year_id, gpr_mean, gpr_lower, gpr_upper)]
old <- grep("gpr", names(df.p), value=T)
new <- gsub("gpr", "parent", old)
setnames(df.p, c('ihme_loc_id', old), c('parent_loc_id', new))
df <- merge(df, df.p, by=c("parent_loc_id", "year_id"), all.x=TRUE)
df <- df[!is.na(parent_mean), gpr_mean := parent_mean]
df <- df[!is.na(parent_mean), gpr_lower := parent_lower]
df <- df[!is.na(parent_mean), gpr_upper := parent_upper]
## Clean
drop <- grep("parent", names(df), value=T)
df <- df[, (drop) := NULL]
return(df)
}

############################################################
## Multiply out ratio model

multiply_model_ratio <- function(df, num, denom) {
denom_cov_name <- me.db[me_short==denom]$covariate_name_short
cov <- pull_covariate(denom_cov_name)
setnames(cov, denom_cov_name, denom)
df <- merge(df, cov, by=c("location_id", "year_id", "age_group_id", "sex_id"), all.x=TRUE)
vars <- c("gpr_mean", "gpr_lower", "gpr_upper")
df <- df[, (vars) := lapply(.SD, function(x) df[[denom]] * x), .SDcols=vars]
df <- df[, me_name := paste0("vacc_", num)]
df <- df[, c("data", "variance") := NULL]
return(df)
}

###########################################################################################################################
# 													  run
###########################################################################################################################


num <- "mcv2"
denom <- "mcv1"
df.ratio <- clean_model_ratio(num, denom)
## Save
saveRDS(df.ratio, paste0(data_root, "/exp/modeled/vacc_", num, "_", denom, "_edit.rds"))
df.out <- multiply_model_ratio(df.ratio, num, denom)
saveRDS(df.out, paste0(data_root, "/exp/modeled/vacc_", num, ".rds"))

############################################################


