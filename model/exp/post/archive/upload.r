###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Prep for upload
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr, readxl)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Path locals
setwd(paste0(j, "/WORK/01_covariates/02_inputs/hsa/code"))
code.root <- paste0(unlist(strsplit(getwd(), "hsa"))[1], "hsa/code")
paths.file <- paste0(code.root, "/paths.csv"); paths <- fread(paths.file)
source(paths[obj=="ubcov_tools", 2, with=F] %>% gsub("J:/", j, .) %>% unlist)
path_loader(paths.file)

## Me DB
me.db <- paste0(code_root, "/reference/me_db.csv") %>% fread


## Date
best.root <- paste0(data_root, "/exp/modeled/best")

source(db_tools)

###########################################################################################################################
# 														thingy
###########################################################################################################################

prep_covariate_upload <- function(me, path) {
  df <- paste0(best.root, "/", me, ".rds") %>% readRDS %>% data.table 
  cov.short <- me.db[me_name==me]$covariate_name_short
  cov.id <- me.db[me_name==me]$covariate_id
  df <- df[, covariate_name_short := cov.short]
  df <- df[, covariate_id := cov.id]
  old <- c("gpr_mean", "gpr_lower", "gpr_upper")
  new <- c("mean_value", "lower_value", "upper_value")
  setnames(df, old, new)
  ## QUICK FIX
  if (me %in% c("vacc_dpt3", "vacc_mcv1", "vacc_hib3", "vacc_pcv3", "vacc_rotac")) {
  df <- df[mean_value < lower_value & lower_value == 0.99, mean_value := 0.99]
  df <- df[mean_value < lower_value & lower_value > 0.98, mean_value := 0.99]
  df <- df[mean_value > upper_value & upper_value == 0.99, mean_value := 0.99]
  df <- df[mean_value > upper_value & upper_value > 0.99, mean_value := 0.99]
  print(head(df[mean_value<lower_value]))
  if (nrow(df[mean_value < lower_value])>0) stop(print("Mean < Lower"))
  print(head(df[mean_value>upper_value]))
  if (nrow(df[mean_value > upper_value])>0) stop(print("Mean > upper"))
  }
  ## CLEAN
  keep <- c("location_id", "year_id", "age_group_id", "sex_id", "mean_value", "lower_value", "upper_value", "covariate_id", "covariate_name_short")
  df <- df[, keep, with=F]
  df <- df[!duplicated(df, by=c("location_id", "year_id", "age_group_id", "sex_id"))]
  write.csv(df, paste0(path, "/", cov.short, ".csv"), na="", row.names=FALSE)
  print(summary(df))
  print(paste0("Saved ", cov.short, " for covariate upload in ", path))
}

get_cov <- function(me) {
  cov.name <- me.db[me_name==me]$covariate_name_short
  cov.id <- me.db[me_name==me]$covariate_id
  df <- get_covariates(cov.name, ci=TRUE)
  setnames(df, paste0(cov.name, c("", "_lower", "_upper")), c("mean_value", "lower_value", "upper_value"))
  df <- df[, covariate_name_short := cov.name]
  df <- df[, covariate_id := cov.id]
  return(df)
}


china_fix <- function(df, me) {
  locs <- get_location_hierarchy(location_set_version_id)
  df <- merge(df, locs[, .(location_id, ihme_loc_id)], by='location_id', all.x=TRUE)
  df.2015 <- get_cov(me)
  df.2015 <- merge(df.2015,locs[, .(location_id, ihme_loc_id)], by='location_id', all.x=TRUE)
  ## DROP CURRENT CHINA
  df <- df[!grepl("CHN", ihme_loc_id)]
  ## Add old china
  df.2015.chn <- df.2015[grepl("CHN", ihme_loc_id)]
  df <- rbind(df, df.2015.chn)
  df[, ihme_loc_id := NULL]
  return(df)
}


path <- paste0(data_root, "/exp/upload/covariates/2017-04-24") 
mes <- c("vacc_dpt3", "vacc_mcv1", "vacc_hib3", "vacc_pcv3", "vacc_rotac")
lapply(mes, function(x) prep_covariate_upload(x, path))

#-------------------------------------------------------

# path <- paste0(data_root, "/exp/upload/covariates/2017-03-09") 

# ## Rota introduction
# df <- readRDS(paste0(data_root, "/exp/reference/vaccine_intro.rds"))
# df <- df[me_name=="vacc_rota1"]
# df <- df[, mean_value := ifelse(cv_intro_years >= 1, 1, 0)]
# df <- df[, lower_value := mean_value]
# df <- df[, upper_value := mean_value]
# ## Drop
# df <- df[, c("ihme_loc_id", "cv_intro", "cv_intro_years", "me_name") := NULL]
# ## Demo
# df <- df[, `:=` (age_group_id = 22, sex_id = 3)]
# meta <- data.table(covariate_id = 140, covariate_name_short = "rotavirus_vaccine_intro")
# df <- cbind(meta, df)
# 
# write.csv(df, paste0(path, "/rotavirus_vaccine_intro.csv"), na="", row.names=F)



