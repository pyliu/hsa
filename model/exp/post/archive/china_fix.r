###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Shiny to viz data and trends
###				
###########################################################

##########################################################################################
## SETUP
rm(list=ls())
## Packages
pacman::p_load(shiny, data.table, shinydashboard, plotly, DT)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Path locals
setwd(paste0(j, "/WORK/01_covariates/02_inputs/hsa/code"))
code.root <- paste0(unlist(strsplit(getwd(), "hsa"))[1], "hsa/code")
paths.file <- paste0(code.root, "/paths.csv"); paths <- fread(paths.file)
source(paths[obj=="ubcov_tools", 2, with=F] %>% gsub("J:/", j, .) %>% unlist)
path_loader(paths.file)

## Source
source(db_tools)

locs <- get_location_hierarchy(location_set_version_id)

mes <- c("vacc_mcv1", "vacc_dpt3", "vacc_pcv3", "vacc_rota1", "vacc_hib3")


## LOAD ALL DATA
files <- list.files(paste0(data_root, "/exp/to_model"), pattern="maternal", full.names=TRUE)
df.mat <- lapply(files, fread) %>% rbindlist(., fill=TRUE)
df.mat$location_id <- NULL
df.vacc <- readRDS("J:/WORK/01_covariates/02_inputs/hsa/data/exp/to_model/vaccination.rds")
df.vacc <- df.vacc[me_name %in% mes]
df.data <- rbind(df.mat, df.vacc, fill=TRUE)
 
## APPEND AND MERGE
df <- lapply(mes, function(me) {
  path <- paste0("J:/WORK/01_covariates/02_inputs/hsa/data/exp/modeled/best/", me, ".rds")
  if (file.exists(path)) {
    df <- readRDS(path)
    df <- df[, .(me_name, location_id, year_id, sex_id, age_group_id, gpr_mean, gpr_lower, gpr_upper)] %>% unique
    return(df)
  }
}) %>% rbindlist(., fill=TRUE)

## Merge
df <- merge(df, locs[level>=3,.(ihme_loc_id, location_id)], by='location_id', all.x=TRUE)
df <- merge(df, df.data, by=c('me_name', 'ihme_loc_id', 'year_id', 'age_group_id', 'sex_id'), all=TRUE)
df <- df[cv_intro_years == 0, c("gpr_mean", "gpr_lower", "gpr_upper") := 0.001]

##########################################################################################
## FUNCTIONS

data.coverage <- function(df) {
  ## Get location hierarchy
  locs <- get_location_hierarchy(149)[level>=3, .(location_id, ihme_loc_id, location_name_short)]
  ## Count datapoints per ihme_loc_id
  cols <- "ihme_loc_id"
  counts <- df[, lapply(.SD, length), .SDcols=cols, by=cols]
  names(counts) <- c(cols, "datapoints")
  ## Merge onto location hierarchy
  counts <- merge(locs, counts, by="ihme_loc_id", all.x=TRUE)
  counts <- counts[is.na(datapoints), datapoints := 0]
  return(counts)
}

vacc.graph <- function(df) {
  p <- ggplot(df) +
    ## Survey
    geom_point(data=df[cv_survey==1], aes(x=year_id, y=data, color=survey_name, label=nid)) +
    ## Admin
    geom_point(data=df[cv_admin==1], aes(x=year_id, y=data, label=nid), color="red") +
    ## Original Admin
    geom_point(aes(x=year_id, y=cv_admin_orig, label=nid), color="red", shape=3) +
    ## Outlier
    geom_point(aes(x=year_id, y=cv_outlier, label=nid, label1=survey_name), shape=2) +
    ## Introduction year
    geom_vline(aes(xintercept=cv_intro), linetype="dashed") 
  
  ## BIAS
  if (nrow(df[!is.na(cv_admin_orig)])) {
    p <- p +geom_ribbon(data=df[cv_admin==1], aes(x=year_id, ymax=cv_admin_orig, ymin=data), alpha=0.1)
  }
  
  ## GPR
  if (any(grepl("gpr", names(df)))) {
    p <- p + geom_line(aes(x=year_id, y=gpr_mean), color="#218944") + geom_ribbon(aes(ymin=gpr_lower, ymax=gpr_upper, x=year_id), fill="#218944", alpha=0.2)
  }
  
  ## AESTHETICS
  p <- p + coord_cartesian(ylim=c(0, 1), xlim=c(1980, 2016)) + 
    theme_bw()+ 
    theme(legend.position="none") +
    facet_wrap(~me_name)
  
  return(p)
}

maternal.graph <- function(df) {
  p <- ggplot(df) +
    ## Survey
    geom_point(data=df[cv_survey==1], aes(x=year_id, y=data, color=survey_name, label=nid)) +
    ## Admin
    geom_point(data=df[cv_admin==1], aes(x=year_id, y=data, label=nid), color="red") +
    ## Outlier
    geom_point(aes(x=year_id, y=cv_outlier, label=nid, label1=survey_name), shape=2) 
  
  ## GPR
  if (any(grepl("gpr", names(df)))) {
    p <- p + geom_line(aes(x=year_id, y=gpr_mean), color="#218944") + geom_ribbon(aes(ymin=gpr_lower, ymax=gpr_upper, x=year_id), fill="#218944", alpha=0.2)
  }
  
  ## AESTHETICS
  p <- p + coord_cartesian(ylim=c(0, 1), xlim=c(1980, 2016)) + 
    theme_bw()+ 
    theme(legend.position="none") +
    facet_wrap(~me_name)
  return(p)
}

##########################################################################################
## UI

ui <- dashboardPage(
  ## HEADER
  dashboardHeader(
    title="Intervention Viz"
  ),
  ## SIDEBAR
  dashboardSidebar(
    sidebarMenu(
      menuItem("Time Series", tabName = "tab_tsplot"), 
      menuItem("Data Coverage", tabName = "tab_datacoverage")
    )
  ),
  ## BODY
  dashboardBody(
    tabItems(
      tabItem(tabName="tab_tsplot",
              selectInput("ihme_loc_id", "Location", choices=unique(df$ihme_loc_id)),
              plotlyOutput("plot_vacc"),
              plotlyOutput("plot_mat")
      )
    )
  )
)

##########################################################################################
## SERVER

server <- function(input,output,session) {
  # TIME SERIES PLOT
  output$plot_vacc <- renderPlotly({
    df.s <- df[ihme_loc_id==input$ihme_loc_id & grepl("vacc", me_name)]
    p <- vacc.graph(df.s)
    ggplotly(p)
  })
  
  output$plot_mat <- renderPlotly({
    df.s <- df[ihme_loc_id==input$ihme_loc_id & grepl("maternal", me_name)]
    p <- maternal.graph(df.s)
    ggplotly(p)
  })
}

shinyApp(ui,server)