###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Make Draws
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(Rcpp, splitstackshape, data.table, dplyr, parallel, readxl, boot, logitnorm)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Source
source(db_tools)

## ME DB
me.db <- fread(me_db)
run.log <- fread(run_log)

## FOR VACC RR
vacc.rr <- '/ihme/forecasting/data/paf/best/rrmax'
vacc.map <- paste0(code_root, "/reference/forecast_map.csv") %>% fread

## Output path
path <- paste0(bmgf_temp, "/draws/")


## ARGS
args <- commandArgs(trailingOnly = TRUE)

#-------------------------------------------------------------------
# FUNCTIONS
#-------------------------------------------------------------------

make_draws <- function(df, n, meanvar, lowervar, uppervar, dist) {
  N <- length(df$mean)
  df <- df[, se := (get(uppervar)-get(lowervar))/3.92]
  if (dist=="norm") {
    draws <- matrix(rnorm(n*N, mean=df[[meanvar]], sd=df$se), nrow=N, ncol=n) %>% data.table
  } else if (dist=="lognorm") {
    mu <- df[[meanvar]]
    se <- df$se
    location <- log(mu^2 / sqrt(se^2 + mu^2))
    shape <- sqrt(log(1 + (se^2 / mu^2)))
    draws <- matrix(rlnorm(n*N, meanlog=location, sdlog=shape), nrow=N, ncol=n) %>% data.table
  } else if (dist == "logitnorm") {
    se <- (df$se)^2 / (1/(df[[meanvar]]*(1-df[[meanvar]])))^2
    draws <- matrix(rlogitnorm(n=n*N, mu=logit(df[[meanvar]]), sigma=se), nrow=N, ncol=n) %>% data.table
  }
  df <- df[, c("se", meanvar, lowervar, uppervar) := NULL]
  names(draws) <- paste0("draw_", 0:(n-1))
  df <- cbind(df, draws)
  return(df)
}

covariate_draws <- function(me) {
  df <- pull_covariate(me, ci=TRUE)
  old <- paste0(me, c("", "_lower", "_upper"))
  new <- c("mean", "lower", "upper")
  setnames(df, old, new)
  df <- make_draws(df, 1000, "mean", "lower", "upper", "logitnorm")
}


clean_vacc <- function(x) {
  ## CLEAN
  name <- basename(x) %>% gsub(".csv", "", .)
  df <- fread(x)[, V1 := NULL]
  old <- c("point_estimate", "lower_bound", "upper_bound")
  if ("year_id" %in% names(df)) df <- df[, year_id := NULL]
  
  ## REVERSE RELATIVE RISKS
  df <- df[, (old) := lapply(.SD, function(x) 1/x), .SDcols=old]
  new <- c("mean", "upper", "lower") ## SWITCHED ORDER
  setnames(df, old, new)
  
  ## MAP ME_NAMES
  df <- cbind(map=name, df)
  df <- merge(df, vacc.map, by='map', all.x=TRUE); df <- df[, map := NULL]
  risk <- unique(df$risk)
  cause <- unique(df$acause)
  
  ## EXPAND LOCATIONS
  if (length(unique(df$location_id))==1) {
    df <- df[, location_id := 1]
  } else if (risk=="vacc_pcv3") {
    ## PCV ONLY USING 10
    df <- df[location_id==43]
    df <- df[, location_id := 1]
  } else if (cause == "tetanus") {
    ## TETANUS USING 1-4 females
    df <- df[location_id==6]; df <- df[, location_id := 1]
  } else {
    ## Rota
    if (risk == "vacc_rota1") {
      child <- list(m33=c(32, 42, 56, 65, 70, 73, 96, 100, 138), 
                    m6=c(5, 9, 21), 
                    m105=c(104, 120, 124, 134), 
                    m161=c(159), 
                    m168=c(167,174,192,199))
      parent <- c(33, 6, 105, 161, 168)
    }
    
    expand <- data.table(location_id=rep(parent, sapply(child, length)), map_id=unlist(child))
    df <- merge(df, expand, by='location_id')
    df <- df[, location_id := map_id]; df <- df[, map_id := NULL]
  } 
  
  ## DEMOGRAPHICS
  if (!("age_group_id" %in% names(df))) df <- df[, `:=` (age_group_id = 22, sex_id =3)]
  
  ## ORDER
  cols <- c("location_id", "age_group_id", "sex_id", "risk", "acause", "mean", "lower", "upper")
  setcolorder(df, cols)
  
  return(df)
}

#-------------------------------------------------------------------
# RUN
#-------------------------------------------------------------------

if (length(args)!=0) {


## ME
me <- args[2]

## RELATIVE RISK
if (args[1] == "rr") {
####################################################################
## Clear path
unlink(paste0(path, "/rr/"), recursive=TRUE)
dir.create(paste0(path, "/rr/"))
## RR Extractions
df <- paste0(data_root, "/rr/bmgf/rr_bmgf.xlsx") %>% read_excel; df <- df[-1,] %>% data.table
keep <- c("risk", "acause", "mean", "lower", "upper")
df <- df[!is.na(risk), keep, with=F]
df$mean <- as.numeric(df$mean)
## Set metadata
meta <- data.table(location_id=1, age_group_id=22, sex_id=3)
df <- cbind(meta, df)
## VACC RR (Shifted to updated reviews)
# files <- files <- list.files(vacc.rr, full.names=TRUE, pattern=".csv")
# df.vacc <- lapply(files, clean_vacc) %>% rbindlist
# df <- rbind(df, df.vacc)
## APPEND AND CLEAN
setnames(df, "risk", "me_name")
me.meta <- me.db[, .(me_name, rei_id, rr_me_id)]; setnames(me.meta, "rr_me_id", "me_id")
df <- merge(df, me.meta, by="me_name", all.x=TRUE)
## SAVE COPY OF THIS
write.csv(df, paste0(data_root, "/rr/rr_full.csv"), na="", row.names=F)
## Make draws
df <- make_draws(df, 1000, "mean", "lower", "upper", "lognorm")
## Expand to get 5 years (1990 by 5 + 2016)
df <- expandRows(df, 7, count.is.col=FALSE)
df <- df[, year_id := 1:.N , by=c("me_name", "acause", "location_id", "age_group_id", "sex_id")]
df <- df[, year_id := ifelse(year_id!=7, 1990 + 5*(year_id-1), 2016)]
## Save by me_name
me_names <- unique(df$me_name)
lapply(me_names, function(x) {
  sub <- df[me_name==x]
  write.csv(sub, paste0(path, "/rr/", x, ".csv"), na="", row.names=F)
})
print('done')
####################################################################  
}



## EXPOSURE
if (args[1] == "exp") {
####################################################################
## Vaccine and maternal
if (grepl("vacc|maternal", me)) {
  me.list <- c("vacc_dpt3", "vacc_mcv1", "vacc_rota1", "vacc_hib3", "vacc_pcv3", "maternal_anc1", "maternal_ifd", "maternal_sba")
  for (me in me.list) {
  ## Covariate
  meta <- me.db[me_name==me, .(me_name, rei_id, exp_me_id)]
  setnames(meta, "exp_me_id", "me_id")
  cov.name <- me.db[me_name==me]$covariate_name_short
  ## Get draws
  draws <- covariate_draws(cov.name)
  ## Add id columns
  draws <- cbind(meta, draws)
  ## Save big file
  write.csv(draws, paste0(path, "/exp/", me, ".csv"), na="", row.names=F)
  }
  print("DONE")
}
####################################################################  

####################################################################
## Diarrhea and LRI
if (grepl("diarrhea|lri", me)) {
  me.list <- c("diarrhea_ors", "diarrhea_zinc", "diarrhea_antibiotics", "lri_antibiotics")
  for (me in me.list) {
  ## Covariate
  meta <- me.db[me_name==me, .(me_name, rei_id, exp_me_id)]
  setnames(meta, "exp_me_id", "me_id")
  run_id <- run.log[me_name==me & is_best==1]$run_id
  ## Get draws from best run
  files <- list.files(paste0("/ihme/covariates/ubcov/model/output/", run_id, "/draws_temp_1"), full.names=TRUE)
  draws <- mclapply(files, fread, mc.cores=5) %>% rbindlist
  ## Add id columns
  draws <- cbind(meta, draws)
  ## Save big file
  write.csv(draws, paste0(path, "/exp/", me, ".csv"), na="", row.names=F)
  }
  print("done")
}
#################################################################### 
}
}
