###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Prep vaccination for ST-GPR
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr, ggplot2, stats, boot)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Source
source(db_tools)

## Reference
locs <- get_location_hierarchy(location_set_version_id)[, .(location_id, ihme_loc_id)]
inputs <- c("vacc_dpt3", "vacc_mcv1", "maternal_anc1", "maternal_anc4", "maternal_sba", "maternal_ifd")
oecd <- c("AUS", "AUT", "BEL", "CAN", "CHL", "CZE", "DNK", "EST", "FIN", "FRA", "DEU", 
          "GRC", "HUN", "ISL", "IRL", "ISR", "ITA", "JPN", "KOR", "LUX", "MEX", "NLD", 
          "NZL", "NOR", "POL", "PRT", "SVK", "SVN", "ESP", "SWE", "CHE", "TUR", "GBR", "USA")

## UTILITY
lnormalize <- function(input) {
  log.input <- log(input)
  log.mean <- mean(log.input)
  log.sd <- sd(log.input)
  return((log.input-log.mean)/log.sd)
}

pca.factors <- function(df, inputs) {
  ## Calculate PCA
  formula <- paste0("~", paste0(inputs, collapse=" + ")) %>% as.formula
  df.pca <- prcomp(formula, data=df)
  ## Store loadings of 1st Factor
  lf <- df.pca[2] %>% data.frame %>% data.table
  lf$me_name <- row.names(df.pca[2] %>% data.frame)
  setnames(lf, "rotation.PC1", "pc1")
  return(list(lf[,.(me_name, pc1)], df.pca))
}

###########################################################################################################################
# SETUP
###########################################################################################################################

## Load data
paths <- paste0(data_root, "/exp/modeled/best/", inputs, ".rds")
df <- lapply(paths, function(x) readRDS(x) %>% data.table) %>% rbindlist(., fill=TRUE)
drop <- c("measure_id", "gpr_lower", "gpr_upper")
df <- df[, (drop) := NULL]
setnames(df, "gpr_mean", "mean")

## TEMP FIX ##
df <- df[mean < 0, mean := 0.001]
df <- df[mean > 1, mean := 0.999]

## Create a separate frame that is normalized by me_name for HSA2
df.norm <- df %>% copy
df.norm <- df.norm[, mean := lnormalize(mean), by="me_name" ]

## Set base frame to be logit transformed
df <- df[, mean := logit(mean), by="me_name"]

## DCAST
df.w <- dcast(location_id + year_id + age_group_id + sex_id ~ me_name, data=df, value.var="mean")
df.w.norm <- dcast(location_id + year_id + age_group_id + sex_id ~ me_name, data=df.norm, value.var="mean")

###########################################################################################################################
# PCA
###########################################################################################################################

lf.hsa <- pca.factors(df.w, inputs)
lf.hsa2 <- pca.factors(df.w.norm, inputs)

###########################################################################################################################
# Calculate different versions of HSA
###########################################################################################################################

## HSA --> Basic sum of mean * component weights by LYAS
df <- merge(df, lf.hsa[[1]], by='me_name')
df <- df[, c("gpr_mean", "gpr_lower", "gpr_upper") := sum(mean*pc1), by=c("location_id", "year_id", "age_group_id", "sex_id")]
df <- df[, me_name := "hsa"]
df <- df[, .(me_name, location_id, year_id, age_group_id, sex_id, gpr_mean, gpr_lower, gpr_upper)] %>% unique

## HSA2
df.norm <- merge(df.norm, lf.hsa2[[1]], by='me_name')
df.norm <- df.norm[, c("gpr_mean", "gpr_lower", "gpr_upper") := sum(mean*pc1), by=c("location_id", "year_id", "age_group_id", "sex_id")]
df.norm <- df.norm[, me_name := "hsa2"]
df.norm <- df.norm[, hsa2 := sum(mean*pc1), by=c("location_id", "year_id", "age_group_id", "sex_id")]
df.norm <- df.norm[, .(me_name, location_id, year_id, age_group_id, sex_id, gpr_mean, gpr_lower, gpr_upper)] %>% unique

## HSA CAPPED (Using HSA2, cap each year at the minimum OECD--Why? I have no ideaaaa)
gprcols <- c("gpr_mean", "gpr_lower", "gpr_upper")
df.min <- df.norm %>% copy
df.min <- merge(df.min, locs, by='location_id', all.x=TRUE)
df.min <- df.min[ihme_loc_id %in% oecd]
df.min <- df.min[, min := min(gpr_mean), by='year_id']
df.min <- df.min[, .(year_id, min)] %>% unique
df.cap <- merge(df.norm, df.min, by='year_id')
df.cap <- df.cap[gpr_mean >= min, (gprcols) := min]
## Exponentiate
df.cap <- df.cap[, (gprcols) := lapply(.SD, function(x) 2.71828^x), .SDcols=gprcols]
df.cap <- df.cap[, me_name := "hsa_capped"]

## SAVE
saveRDS(df, paste0(data_root, "/exp/modeled/best/hsa.rds"))
saveRDS(df.norm, paste0(data_root, "/exp/modeled/best/hsa2.rds"))
saveRDS(df.cap, paste0(data_root, "/exp/modeled/best/hsa_capped.rds"))

