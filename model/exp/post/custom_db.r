###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Upload data bundles for Custom Database (GBD2016)
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Bundles : anc1, anc4, sba, ifd, dpt, hib, measles, pcv, rota
mes <- c("maternal_anc1", "maternal_anc4", "maternal_sba", "maternal_ifd", "vacc_dpt3", "vacc_hib3", "vacc_mcv1", "vacc_pcv3", "vacc_rotac")
paths <- paste0(data_root, "/exp/to_model/", mes, ".csv")

for (path in paths) {

me <- basename(path) %>% gsub(".csv", "", .)

## Open up
df <- fread(path)

## Add seq column
df <- df[, seq := NA]

## Add year start end column
df <- df[, `:=` (year_start = year_id, year_end = year_id)]

## Save
write.csv(df, paste0(data_root, "/exp/upload/custom_db/", me, ".csv"))

}



