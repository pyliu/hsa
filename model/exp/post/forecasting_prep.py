import pandas as pd
## Path to DPT3 forecasted draws and summary
path = '/ihme/forecasting/data/vaccine_coverage/coverage/20170425_new_sims/dtp3_draws.h5'
output = '/home/j/WORK/01_covariates/02_inputs/hsa/data/exp/forecasting/'
## Load and save draws and flatfile
sum = pd.read_hdf(path, key="data_sum")
sum = sum.loc[sum.response_var=="y_T"]
sum.to_csv('%s/dpt3_sum.csv' %(output), index=False)
draws = pd.read_hdf(path, key="data")
draws.to_csv('%s/dpt3_draws.csv' %(output), index=False)