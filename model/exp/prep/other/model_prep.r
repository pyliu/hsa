###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Model Prep
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr, parallel, readxl, binom)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Path locals
setwd(paste0(j, "/WORK/01_covariates/02_inputs/hsa/code"))
code.root <- paste0(unlist(strsplit(getwd(), "hsa"))[1], "hsa/code")
paths.file <- paste0(code.root, "/paths.csv"); paths <- fread(paths.file)
source(paths[obj=="ubcov_tools", 2, with=F] %>% gsub("J:/", j, .) %>% unlist)
path_loader(paths.file)


## Source
source(db_tools)

## Location hierarchy
locs <- get_location_hierarchy(149)[level>=3, .(ihme_loc_id, location_id)]

## Me DB
me.db <- paste0(code_root, "/reference/me_db.csv") %>% fread

## DHS Data
df.dhs <- read_excel(paste0(data_root, "/raw/vaccination/survey/statcompiler/statcompiler.xlsx")) %>% data.table

###########################################################################################################################
# 														model prep
###########################################################################################################################

get_con <- function(dbname, host) {
  con <- suppressWarnings(src_mysql(dbname = dbname, host = host, user = "dbview", password = "E3QNSLvQTRJm"))
  return(con)
}

run_query <- function(dbname, host, query) {
  con <- get_con(dbname, host)
  return(con %>% tbl(sql(query)) %>% collect(n=Inf) %>% data.table)
  disconnect()
}

disconnect <- function() { 
  lapply( dbListConnections(MySQL()), function(x) dbDisconnect(x) )
}


get_citations <- function(path=TRUE, file=FALSE) {
  dbname <- "shared"
  host <- "modeling-cod-db.ihme.washington.edu"
  if (path) query <- paste0("SELECT DISTINCT nid, file_location FROM shared.mv_nid_file ")
  if (file) query <- paste0("SELECT DISTINCT nid, file_name FROM shared.mv_nid_file ")
  run_query(dbname, host, query)
}


prep_dhs <- function(data, bundle){
  ## Prep DHS
  df <- data %>% copy
  old <- c("Country Name", "Survey Year", "Indicator", "By Variable", "Characteristic Category")
  new <- c("location_name", "year_id", "dhs_name", "by_var", "dhs_data")
  setnames(df, old, new)
  df <- df[, new, with=F]
  ## Merge me_name
  df <- merge(df, me.db[, .(dhs_name, me_name)], by='dhs_name', all.x=TRUE)
  df <- df[!is.na(me_name) & me_name != "" & !is.na(dhs_data)]
  df <- df[, dhs_data := dhs_data/100]
  ## Subset to 5 years prior
  df <- df[by_var=="Five years preceding the survey"]
  df <- df[, c("by_var", "dhs_name") := NULL]
  ## Location names
  locs <- get_location_hierarchy(location_set_version_id)
  df <- df[location_name == "Congo Democratic Republic", location_name := "Democratic Republic of the Congo"]
  df <- df[location_name == "Gambia", location_name := "The Gambia"]
  df <- df[location_name == "Kyrgyz Republic", location_name := "Kyrgyzstan"]
  df <- merge(df, locs[, .(location_name, ihme_loc_id)], by='location_name', all.x=TRUE)
  ## Subset bundle
  df <- df[grepl(bundle, me_name)]
  return(df)
}

get.old_data <- function(bundle) {
  path <- paste0(j, "WORK/01_covariates/02_inputs/hsa/archive/gbd2015/data/input/to_model")
  files <- list.files(path, pattern=bundle, full.names=TRUE)
  df <- lapply(files, fread) %>% rbindlist(., fill=TRUE)
  df <- df[!is.na(data)]
  ## Grab nids
  df$nid <- NULL
  nids.path <- get_citations(path=TRUE)
  nids.path$file_location <- gsub("[\\]", "/", nids.path$file_location)
  nids.file <- get_citations(file=TRUE, path=FALSE)
  ## Fix file_path in dataset
  df$j_path <- gsub("/$", "", df$j_path)
  ## TRY TO MERGE FILE FIRST
  df <- merge(df, nids.file, by.x='j_name', by.y='file_name', all.x=TRUE)
  ## NEXT TRY TO MERGE PATHS
  df.paths <- df[is.na(nid), .(j_path)] %>% unique
  df.paths <- merge(df.paths, nids.path, by.x='j_path', by.y='file_location', all.x=TRUE)
  ## Subset to the ones that end in years
  df.paths <- df.paths[grepl("[0-9]$", j_path)]
  setnames(df.paths, "nid", "newnids")
  df <- merge(df, df.paths, by='j_path', all.x=TRUE)
  df <- df[is.na(nid) & !is.na(newnids), nid := newnids]; df$newnids <- NULL
  df <- df[, file_path := paste0(j_path, "/", j_name)]
  cols <- c("me_name", "year_id", "age_group_id", "sex_id", "ihme_loc_id", "admin", "nid", "file_path", "data", "sample_size", "variance")
  df <- df[, (cols), with=FALSE]
  setnames(df, "admin", "cv_admin")
  return(df)
}

model_prep <- function(df) {
  
  ## Location cleaning
  df <- df[ihme_loc_id=="PAL", ihme_loc_id := "PSE"]
  df <- merge(df, locs, by='ihme_loc_id', all.x=T)
  table(df[is.na(location_id), .(ihme_loc_id)])
  df <- df[!is.na(location_id)]
  
  ## Sex id
  df <- df[, sex_id := 3]
  
  ## Age group id
  df <- df[, age_group_id := 22]
  
  ## Year_id
  if (!("year_id" %in% names(df))) df <- df[, year_id := floor((year_start + year_end)/2)]
  
  ## Wilson interval adjustment for means of 0
  if (nrow(df[data %in% c(0,1)]) > 0) {
  df.w <- df[data %in% c(0,1) & !is.na(sample_size)]
  sample_size <- df.w$sample_size
  n <- ifelse(df.w$data==0, 0, sample_size)
  ci <- binom.confint(n, sample_size, conf.level = 0.95, methods = "wilson")
  se <- (ci$upper - ci$lower)/3.92
  df[data %in% c(0,1) & !is.na(sample_size)]$standard_error <- se
  }
  
  ## Save
  for (i in unique(df$me_name)) {
    path <- paste0(data_root, "/exp/to_model/", i, ".csv")
    write.csv(df[me_name==i], path, row.names=FALSE, na="")
    print(paste0("Saved: ", path))
  }
           
  return(df)
}

outlier.data <- function(df, bundle) {
  ## Get outlier frame
  df.o <- fread(paste0(code_root, "/reference/outliers/", bundle, ".csv")) %>% unique
  df.o <- df.o[, outlier := 1]
  ## Set conditions
  cond <- list(
    list('me_name==""', c("nid")),
    list('batch_outlier==1 & me_name != ""', c("me_name", "nid")),
    list('batch_outlier==0', c("me_name", "nid", "ihme_loc_id", "year_id"))
  )
  ## Loop through and outlier
  for (i in cond) {
    condition <- i[[1]]
    vars <- i[[2]]
    ## Subset
    o.sub <- df.o[eval(parse(text=condition)), (vars), with=FALSE]
    o.sub <- o.sub[, outlier := 1]
    ## Merge
    df <- merge(df, o.sub, by=vars, all.x=TRUE)
    ## Set outlier
    df <- df[outlier==1, cv_outlier := data]
    ## Clean
    df <- df[outlier==1, data := NA]
    df <- df[, outlier := NULL]
  }
  return(df)
}


###########################################################################################################################
# 														running
###########################################################################################################################

args <- commandArgs(trailingOnly = TRUE)

if (length(args)!=0) {
  
###########################################################################################################################

if (args[1] == "diarrhea") {
## DIARRHEA
df <- readRDS(paste0(data_root, "/exp/collapsed/diarrhea.rds"))[['data']]
## ADD STATCOMPILER DATA
df.diarrhea <- prep_dhs(df.dhs, "diarrhea")
df.diarrhea <- df.diarrhea[,survey_name := "MACRO_DHS"]
df <- merge(df, df.diarrhea, by=c("year_id", "ihme_loc_id", "me_name", "survey_name"), all=TRUE)
df <- df[is.na(data) & !is.na(dhs_data), nid := 0]
df <- df[is.na(data) & !is.na(dhs_data), data := dhs_data]
## FOR ZINC, put in intercept shift for locations with data > .30
square <- expand.grid(ihme_loc_id=locs$ihme_loc_id, year_id = year_start:year_end, me_name="diarrhea_zinc")
df <- merge(df, square, by=c("ihme_loc_id", "year_id", "me_name"), all=TRUE)
df <- df[me_name=="diarrhea_zinc", cv_shift := ifelse(any(!is.na(data)) & max(data, na.rm=TRUE) > 0.30, 1, 0), by="ihme_loc_id"]
save <- model_prep(df)
}

###########################################################################################################################
  
if (args[1] == "maternal") {
## GET DATA
df <- readRDS(paste0(data_root, "/exp/collapsed/maternal.rds"))[['data']]
## ADD STATCOMPILER DATA
df.maternal <- prep_dhs(df.dhs, "maternal")
df.maternal <- df.maternal[,survey_name := "MACRO_DHS"]
df <- merge(df, df.maternal, by=c("year_id", "ihme_loc_id", "me_name", "survey_name"), all=TRUE)
df <- df[is.na(data) & !is.na(dhs_data), data := dhs_data]
## ADD OLD DATA
df.old <- get.old_data("maternal")
## Remove datapoints that are in in new
new.nids <- df[!is.na(nid)]$nid %>% unique
df.old <- df.old[!(nid %in% new.nids)]
df <- rbind(df, df.old, fill=TRUE)
df <- df[me_name %in% c("maternal_anc1", "maternal_anc4", "maternal_sba", 'maternal_ifd')]
## SET ADMIN SURVEY CV
df <- df[, cv_admin := ifelse(!is.na(cv_admin), 1, 0)]
df <- df[, cv_survey := ifelse(cv_admin==1, 0, 1)]
## OUTLIER DATA
df <- outlier.data(df, "maternal")
save <- model_prep(df)
}
  
###########################################################################################################################
}