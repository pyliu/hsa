###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: LRI Crosswalks
###
###       Crosswalking differnet LRI coverage definitions
###       based on symptoms used to define LRI.
###        
###          As per Chris Troger's suggestion, I've calculated
###          coverage using 4 definitions and used the notation lri`n' for convenience
###            
###          1. lri1: Cough, difficulty breathing, chest, fever
###          2. lri2: Cough, difficulty breathing, chest
###          3. lri3: Cough, difficulty breathing, fever
###          4. lri4: Cough, difficulty breathing
###
###          where lri1 is the gold standard.
###        
###          This code explores feasibility of crosswalk and performs. 
###				
###########################################################

###################
### Setting up ####
###################

rm(list=ls())
pacman::p_load(data.table, dplyr, lme4, ggplot2, boot, readxl)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Path locals
setwd(paste0(j, "/WORK/01_covariates/02_inputs/hsa/code"))
code.root <- paste0(unlist(strsplit(getwd(), "hsa"))[1], "hsa/code")
paths.file <- paste0(code.root, "/paths.csv"); paths <- fread(paths.file)
source(paths[obj=="ubcov_tools", 2, with=F] %>% gsub("J:/", j, .) %>% unlist)
path_loader(paths.file)

## Model Prep
source(paste0(code_root, "/model/prep/model_prep.r"))

## Data
df <- readRDS(paste0(data_root, "/exp/collapsed/lri.rds"))[['data']] %>% data.table

## Levels
levels <- c(1, 2, 3, 4)

###########################################################################################################################
# 														clean and prep data
###########################################################################################################################

## Subset
keep <- c('nid', 'survey_name', 'ihme_loc_id', 'year_start', 'year_end', 'survey_module', 'file_path', 
          'data', 'variance', 'sample_size', 'age_group_id', 'year_id', 
          'me_name', 'bundle')
df <- df[, keep, with=FALSE]

## Generate id for utility
cols <- c('nid', 'survey_name', 'ihme_loc_id', 'year_start', 'year_end', 'survey_module', 'file_path')
df <- df[, id := .GRP, by=cols]
## Store meta
meta <- df[, c(cols, "id"), with=FALSE] %>% unique
df <- df[, -c(cols), with=FALSE]

## Check for duplicates by "id" and "me_name"
check <- df[duplicated(df, by=c("id", "me_name"))]
if (nrow(check) > 0) stop("Duplicates detected")

## Remove zeros 
df <- df[data != 0]

## Reshape wide
df.w <- dcast(df, id ~ me_name, value.var = c("data", "variance"))

###########################################################################################################################
# 														Crosswalk
###########################################################################################################################

## Regressions
mods <- lapply(levels[2:4], function(x) {
  formula <- paste0("logit(data_lri1)~logit(data_lri", x, ")") %>% as.formula
  lm(formula, data=df.w)
})
names(mods) <- paste0("lm", levels[2:4])

## Predict
df.w <- df.w[, predict_lri4 := predict(mods[['lm4']], newdata=df.w) %>% inv.logit]
df.w <- df.w[, predict_lri3 := predict(mods[['lm3']], newdata=df.w) %>% inv.logit]
df.w <- df.w[, predict_lri2 := predict(mods[['lm2']], newdata=df.w) %>% inv.logit]

## Fill in estimate based on priority (lri1 > lri2 > lri3...)
df.w$data <- as.numeric(NA)
for (i in levels) {
  ## Settings
  fill <- paste0("predict_lri", i)
  if (i == 1) fill <- "data_lri1"
  var <- paste0("variance_lri", i)
  mod <- paste0("lm", i)
  ## Store what definition is used
  df.w <- df.w[is.na(data) & !is.na(get(fill)), cv_cw := paste0("lri", i)]
  ## Propagate variance from model (new variance = data variance + beta_variance*data^2 + intercept_variance)
  if (i > 1) {
    beta_variance <-coef(summary(mods[[mod]]))[2,2]**2
    intercept_variance <- coef(summary(mods[[mod]]))[1,2]**2
    df.w <- df.w[is.na(data) & !is.na(get(fill)), variance := get(var) + beta_variance*get(fill)^2 + intercept_variance]
  } else {
    df.w <- df.w[, variance := get(var)]
  }
  ## Store the value
  df.w <- df.w[is.na(data) & !is.na(get(fill)), data := get(fill)]
}

###########################################################################################################################
# 														Clean and output
###########################################################################################################################

## Clean data
out <- df.w[, .(id, data, variance, cv_cw)]
out <- out[data != 0] ## Dropping rows with 0

## Merge onto metadata
save <- merge(meta, out, by='id')
save <- save[, id := NULL]
save <- save[, me_name := "lri_antibiotics"]

## Temp fix
save <- save[, sample_size := NA]

## Model prep and save
save <- model_prep(save)



