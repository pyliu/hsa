###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Prep vaccination for ST-GPR
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Path locals
setwd(paste0(j, "/WORK/01_covariates/02_inputs/hsa/code"))
code.root <- paste0(unlist(strsplit(getwd(), "hsa"))[1], "hsa/code")
paths.file <- paste0(code.root, "/paths.csv"); paths <- fread(paths.file)
source(paths[obj=="ubcov_tools", 2, with=F] %>% gsub("J:/", j, .) %>% unlist)
path_loader(paths.file)

## Source
source(db_tools)

###########################################################################################################################
# Prep Ratios
###########################################################################################################################

## Prep ratio
prep.ratio <- function(num, denom, have, header) {

## SET NAMES
num.short <- gsub(header, "", num)
denom.short <- gsub(header, "", denom)
ratio.name <- paste0(header, num.short, "_", denom.short, "_ratio")

## WHAT DO I WANT TO MODEL
want <- setdiff(c(num, denom), have)

## GRAB DATA
df.want <- paste0(data_root, "/exp/to_model/", want, ".csv") %>% fread ## FROM PREP
df.have <- paste0(data_root, "/exp/modeled/best/", have, ".rds") %>% readRDS %>% data.table ## FROM POST MODEL

## MERGE
df.have <- df.have[, .(location_id, year_id, age_group_id, sex_id, gpr_mean)]
df <- merge(df.want, df.have, by=c("location_id", "year_id", "age_group_id", "sex_id"), all.x=TRUE)

## TAKE RATIO
if (num==have) df <- df[, data := gpr_mean/data]
if (denom==have) df <- df[, data := data/gpr_mean]
df$gpr_mean <- NULL

## CAP RATIO
df <- df[data >= 1, data := 0.999]
df <- df[, me_name := ratio.name]

## SAVE
write.csv(df, paste0(data_root, "/exp/to_model/", ratio.name, ".csv"), na="", row.names=F)

print(paste0("Saved ", data_root, "/exp/to_model/", ratio.name, ".csv"))
return(df)
}


## DPT3/DPT1 RATIO
#df <- prep.ratio(num="vacc_dpt3", denom="vacc_dpt1", have="vacc_dpt3", header="vacc_")

## ANC4/ANC1 RATIO
df <- prep.ratio(num="maternal_anc4", denom="maternal_anc1", have="maternal_anc1", header="maternal_")



