###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: 
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Path locals
setwd(paste0(j, "/WORK/01_covariates/02_inputs/hsa/code"))
code.root <- paste0(unlist(strsplit(getwd(), "hsa"))[1], "hsa/code")
paths.file <- paste0(code.root, "/paths.csv"); paths <- fread(paths.file)
source(paths[obj=="ubcov_tools", 2, with=F] %>% gsub("J:/", j, .) %>% unlist)
path_loader(paths.file)

source(db_tools)

###########################################################################################################################
# PREP PAB
###########################################################################################################################

df <- paste0(data_root, "/raw/pab/WHS4_128.csv") %>% fread
locs <- get_location_hierarchy(location_set_version_id)

## SUBSET
df <- df[, c(7, 16, 19), with=F]
names(df) <- c("year_id", "ihme_loc_id", "data")
## DIVIDE BY 100
df <- df[, data := data/100]
df <- df[, variance := 0.01]
## Setup for STGPR
df <- df[, `:=` (age_group_id = 22, sex_id = 3, me_name = "maternal_pab", sample_size = NA, nid = NA)]
df <- merge(df, locs[, .(ihme_loc_id, location_id)], by="ihme_loc_id", all.x=TRUE)

## Save
write.csv(df, paste0(data_root, "/exp/to_model/maternal_pab.csv"), na="", row.names=F)