###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Collapse indicators
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr, parallel)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Source
setwd(paste0(j, "/WORK/01_covariates/common/ubcov_central"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/collapse/collapse.R"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/utilitybelt/db_tools.r"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/cluster_tools.r"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/ubcov_tools.r"))

## Bundles
bundles <- c("vaccination", "maternal", "diarrhea", "lri")

## Data Paths
extract.root <- lapply(bundles, function(x) paste0(extract_root, "/", x))
names(extract.root) <- bundles

## Output Paths
output.root <- paste0(data_root, "/exp/collapsed")

## Other reference files
  ## Get list of vaccinations
  vacc.ref <- fread(paste0(code_root, "/extraction/reference/vacc_dose.csv"))
  vacc.list <- vacc.ref$var
  ## Expand out to get full list
  vacc.short <- lapply(vacc.list, function(x) strsplit(x, "_")[[1]][1]) %>% unlist
  vacc.max <- vacc.ref$refdose
  vacc.dose <- lapply(1:length(vacc.short), function(x) paste0(vacc.short[[x]],1:vacc.max[[x]])) %>% unlist

## Indicators lists
indicators <- list()
indicators[['vaccination']] <- vacc.dose
indicators[['maternal']] <- c("anc1", "anc4", "sba", "ifd")
indicators[['diarrhea']] <- c("ors", "zinc", "antibiotics")
indicators[['lri']] <- c("lri1", "lri2", "lri3", "lri4")

## Other reference
lri.symp <- c("had_fever", "had_cough", "diff_breathing", "chest_symptoms")
## Four definitions of LRI according to Chris Troeger
  ## 1. Cough, difficulty breathing, chest, fever
  lri1 <- c('had_cough', 'diff_breathing', 'chest_symptoms', 'had_fever')
  ## 2. Cough, difficulty breathing, chest
  lri2 <- c('had_cough', 'diff_breathing', 'chest_symptoms')
  ## 3. Cough, difficulty breathing, fever
  lri3 <- c('had_cough', 'diff_breathing', 'had_fever')
  ## 4. Cough, difficulty breathing
  lri4 <- c('had_cough', 'diff_breathing')
  
## Geography variables
geo.vars <- c("ihme_loc_id", "admin_1_id", "admin_2_id")

###########################################################################################################################
# 														collapse blocks
###########################################################################################################################


#################################
## General prep for collapse:
##    - Pull out metadata from rest of frame
##    - Check for missing design variables
#################################

collapse.prep <- function(df) {
  ## Store meta
  metacols <- c("nid", "survey_name", "ihme_loc_id", "year_start", "year_end", "survey_module", "file_path")
  meta <- df[, (metacols), with=F] %>% unique
  ## Design adjustments
    ## See if any design variables are missing
    missing_design_vars <- list()
    for (var in c("strata", "psu", "pweight")) {
      if (var %in% names(df)) {
      if (all(is.na(df[[var]]))) {
        df[[var]] <- as.numeric(df[[var]])
        df <- df[, (var) := 1]
        missing_design_vars <- c(missing_design_vars, var)
      }
      } else {
        df <- df[, (var) := 1]
      }
    }
    df <- df[!is.na(pweight)]
    meta <- meta[, missing_design_vars := toString(missing_design_vars %>% unlist)]
  ## Geography
    for (i in geo.vars) {
      if (i %in% names(df)){
        ## Removing weird mapping
        if (i != "ihme_loc_id") {
          df <- df[ihme_loc_id == get(i), (i) := NA]
          df <- df[grepl("garbage", get(i)), (i) := NA]
        }
        ## Check geography variables and drop column if completely missing
        if(any(!is.na(df[[i]]))==FALSE) df <- df[, (i) := NULL]
      }
    }
  ## If child_age_year and child_age_sex, replace
  if ("child_age_year" %in% names(df)) df <- df[, age_year := child_age_year]
  if ("child_sex_id" %in% names(df)) df <- df[, sex_id := child_sex_id]
    
  return(list(df=df, meta=meta))
}

#################################
## Check for collapse
##    - Put in error checks for whether or not to skip collapse
#################################

collapse.check <- function(df, me) {
  continue <- FALSE
  error <- ""
  ## TOPIC SPECIFIC CHECKS
  if (me %in% "vaccination"){
    check1 <- ifelse(length(intersect(vacc.list, names(df))) > 0, 1, 0) ## Any vacc vars available
    if (check1 == 0) error <- paste0(error, ";No vacc vars available")
    ## Store checklist
    checklist <- c(check1)
  }
  if (me %in% "maternal") {
    source <- c("birth_attendant_mapped", "delivery_location_mapped", "anc_times", "anc_ever")
    check1 <- ifelse(length(intersect(source, names(df)))>0, 1, 0) ## If 
    if (check1 == 0) error <- paste0(error, ";No vars available")
    ## Store checklist
    checklist <- c(check1)
  }
  if (me %in% "diarrhea") {
    source <- "diarrhea_tx_type_mapped"
    indicators <- indicators[[me]]
    check1<- ifelse(source %in% names(df), 1, 0) ## If no source variable
      if (check1 == 0) error <- paste0(error, ";No source variable available")
    check2 <- ifelse(any(!is.na(df[[source]])), 1, 0) ## If source variable completely missing
      if (check2 == 0) error <- paste0(error, ";Source variable completely missing")
    check3 <- ifelse(any(grepl(paste0(indicators, collapse="|"), df[[source]])), 1, 0) ## If no zinc, ors, anti
      if (check3 == 0) error <- paste0(error, ";No indiators of interest in source variable")
    ## Store checklist
    checklist <- c(check1, check2, check3)
  }
  if (me %in% "lri") {
    source <- "lri_tx_type_mapped"
    indicators <- indicators[[me]]
    check1 <- ifelse(all(lri4 %in% names(df)), 1, 0) ## Dataset has to have at least cough and difficulty breathing
      if (check1 == 0) error <- paste0(error, ";Needs cough and difficulty breathing")
    check2 <- ifelse(source %in% names(df), 1, 0) ## If no source variable
      if (check2 == 0) error <- paste0(error, ";No source variable available")
    check3 <- ifelse(any(!is.na(df[[source]])), 1, 0) ## If source variable completely missing
      if (check3 == 0) error <- paste0(error, ";Source variable completely missing")
    check4 <- ifelse(lapply(lri4, function(x) !all(is.na(df[[x]]))) %>% unlist %>% all, 1, 0) ## Cough and difficulty breathing not entirely missing
      if (check4==0) error <- paste0(error, ";Cough and difficulty entirely missing")
    ## Store checklist
    checklist <- c(check1, check2, check3, check4)
  }
  ## GENERAL CHECKS
  check1 <- ifelse("age_year" %in% names(df), 1, 0) ## No age year variable
    if (check1 == 0) error <- paste0(error, ";No age_year var")
  if ("age_year" %in% names(df)) check2 <- ifelse(nrow(df[age_year>0 & age_year <= 5])>0, 1, 0) else check2 <- 1
    if (check2 == 0) error <- paste0(error, ";No under 5")
  checklist <- c(checklist, check1, check2)
  ## IF ANY ERROR, PASS COLLAPSE
  continue <- all(checklist==1)
  if (error != "") print(error)
  return(list(continue, error))
}

#################################
## Generate indicators for collapse
##    - Any final indicator processing
##    - Additional me specific coding, such as limiting age groups
#################################

collapse.gen <- function(df, me) {
  ## Create indicators
  if (me == "vaccination"){
    indics <- intersect(vacc.list, names(df))
    lapply(indics, function(x) {
      max <- max(df[[x]], na.rm=T)
      short <- strsplit(x, "_dose")
      for (i in 1:max) df[, (paste0(short, i)) := ifelse(get(x) >= i, 1, 0)]
    })
  }
  if (me == "maternal") {
    ## IFD
    if ("delivery_location_mapped" %in% names(df)) {
      df <- df[grepl("hospital|private|midwife|phc|clinic", delivery_location_mapped), ifd := 1]
      df <- df[grepl("home|traditional|other", delivery_location_mapped) & !grepl("hospital|private|midwife|phc|clinic", delivery_location_mapped), ifd := 0]
    }
    ## SBA
    if ("birth_attendant_mapped" %in% names(df)) {
      df <- df[grepl("doctor|nurse|midwife", birth_attendant_mapped), sba := 1]
      df <- df[grepl("none|chw|tba|family|other", birth_attendant_mapped) & !grepl("doctor|nurse|midwife", birth_attendant_mapped) , sba := 0]
    }
    ## ANC1/4
    if ("anc_ever" %in% names(df)) {
      df <- df[anc_ever==1, anc1 := 1]
      df <- df[anc_ever==0, anc1 := 0]
    }
    if ("anc_times" %in% names(df)) {
      df <- df[anc_times >= 1, anc1 := 1]
      df <- df[anc_times < 1, anc1 := 0]
      df <- df[anc_times >=4, anc4 :=1]
      df <- df[anc_times < 4, anc4 :=0]
      if ("anc_ever" %in% names(df)) df <- df[anc_ever==0, anc4:=0]
    }
    if ("anc_attendant" %in% names(df)) {
      if ("anc1" %in% names(df)){
        df <- df[grepl("chw|tba|other|none", anc_attendant) & !grepl("doctor|nurse|midwife", anc_attendant) & anc1 ==1 , anc1:=0 ]
      } 
      if ("anc4" %in% names(df)) {
        df <- df[grepl("chw|tba|other|none", anc_attendant) & !grepl("doctor|nurse|midwife", anc_attendant) & anc4 ==1 , anc4:=0 ]
      } 
    }
    
  }
  if (me == "diarrhea") {
    source <- "diarrhea_tx_type_mapped"
    indics <- indicators[[me]]
    for (i in indics) {
      if (any(grepl(i, df[[source]]))){
        if ("had_diarrhea" %in% names(df)) {
          df <- df[had_diarrhea==1, (i) := ifelse(grepl(i, get(source)), 1, 0)]
        } else if ("diarrhea_tx" %in% names(df)) {
          df <- df[diarrhea_tx==1, (i) := ifelse(grepl(i, get(source)), 1, 0)]
          df <- df[diarrhea_tx==0, (i) := 0]
        }
      }
    }
  }
  if (me == "lri") {
    source <- "lri_tx_type_mapped"
    indics <- indicators[[me]]
    for (i in indics) {
      ## Check if symptom vars present required for specific definition
      check <- all(get(i) %in% names(df))
      if (check) {
        cond <- paste0(get(i), "==1", collapse=" & ") ## Conditional statement for if all of the symptoms == 1
        df <- df[eval(parse(text=cond)), (i) := ifelse(get(source) == "antibiotics", 1, 0)]
      }
    }
  }
  ## Other custom processing
    ## Round age_year and restrict ages to < 5
    df <- df[, age_year := floor(age_year)]
    if (me != "maternal") df <- df[age_year >=0 & age_year < 5]
    ## For Diarrhea and LRI, group all <5
    if (me %in% c("diarrhea", "lri")) df <- df[, age_year := 5]
  return(df)
}



#################################
## Collapse
##    - Subsets and drops lonely people
##    - Iterates through geo vars
#################################

collapse_by.wrapper <- function(df, svy_var, by_vars) {
  sf <- df %>% copy
  ## Subset
  cols <- c(svy_var, by_vars)
  for (i in cols) sf <- sf[!is.na(get(i))] 
  ## Drop lonely
  sf <- sf[!is.na(svy_var), lonely := lapply(.SD, length), .SDcols=svy_var, by=by_vars]
  sf <- sf[lonely != 1]; sf<- sf[, lonely := NA]
  ## Collapse
  out <- collapse_by(sf, var=svy_var, by_vars)
  ## Rename
  rename <- intersect(geo.vars, names(out))
  setnames(out, rename, rep("ihme_loc_id", length(rename)))
  return(out)
}

#################################
## Collapse
##    - Cleans and prep
#################################

collapse.clean <- function(data, me) {
  df <- copy(data)
  ## Renames
  old <- c('var', 'mean')
  new <- c('me_name', 'data')
  setnames(df, old, new)
  ## Variance
  df <- df[, variance := standard_error^2]
  ## Age groups
  df <- df[, age_group_id := 22]
  ## Years
  df <- df[, year_id := floor((year_start+year_end)/2)]
  ## Bundle
  df <- df[, bundle := me]
  ## Custom cleaning
  if (me == "diarrhea") {
    df <- df[, me_name := paste0("diarrhea_", me_name)]
  }
  if (me == "maternal") {
    df <- df[, me_name := paste0("maternal_", me_name)]
    if ("age_year" %in% names(df))  df <- df[, year_id := year_id - age_year]
  }
  if (me == 'vaccination') {
    df <- df[, year_id := year_id - age_year]
  }
  return(df)
}

###########################################################################################################################
# 														collapse
###########################################################################################################################

age.check <- function(df, vars) {
  check <- 0
  for (var in vars) {
    if (var %in% names(df)) if (any(nrow(df[age_year >= 0 & age_year <=5 & !is.na(get(var))]))) check <- 1
  }
  return(check)
}

collapse <- function(df, me) {
  ## Prep
  prep <- collapse.prep(df)
  df <- prep$df
  meta <- prep$meta[, ihme_loc_id := NULL] ## Remove ihme_loc_id so can collapse by it
  ## Check
  check <- collapse.check(df, me)
  continue <- check[[1]]
  errors <- check[[2]]
  ## Collapse
  if (continue) {
    ## Process indicators and other custom processing
    df <- collapse.gen(df, me)
    ## Determine which survey_vars to collapse by
    survey.vars <- intersect(indicators[[me]], names(df))
    ## Determine which by_vars to collapse by
    by.vars <- intersect(geo.vars, names(df))
    ## For maternal, if any age_year in c(0:5) collapse by age_year else not
    if (me != "maternal") by.vars <- lapply(by.vars, function(x) c(x, "age_year"))
    if (me=="maternal")  {
      if (age.check(df, c("anc1", "anc4", "sba", "ifd"))) {
        df <- df[age_year >= 0 & age_year <= 5]
        by.vars <- lapply(by.vars, function(x) c(x, "age_year"))
      }
    } 
    ## Collapse
    out <- NULL
    for (x in survey.vars) {
      for (y in by.vars) {
        results <- collapse_by.wrapper(df, svy_var=x, by_vars=y)
        out <- rbind(out, results)
      }
    }
    out <- cbind(meta, out)
    errors <- NULL
    out <- collapse.clean(out, me)
  } else {
    out <- NULL
    errors <- cbind(meta, errors=errors)
  }
  return(list(out, errors))
}

###########################################################################################################################
# 														Clean
###########################################################################################################################

collapse.files <- function(me) {
  files <- list.files(extract.root[[me]], full.names=TRUE) %>% sort
  return(files)
}


###########################################################################################################################
# 														run
###########################################################################################################################

collapse.run <- function(me, i) {
collapsed <- NULL
error.log <- NULL
## Collapse
print(paste0(i, "/", length(collapse.files(me)), " : ", collapse.files(me)[i]))
df <- collapse.files(me)[i]%>%fread
out <- collapse(df, me)
return(out)
}


###########################################################################################################################
# 														run
###########################################################################################################################

args <- commandArgs(trailingOnly = TRUE)

if (length(args)!=0) {

###########################################################################################################################
## LAUNCH

if (args[1]=="launch") {
  ###########################
  ## SYNTAX: collapse.r launch "me" [skip|number to collapse]
  
  ## Arguments
  me <- args[2]
  ## Skip collapse and just append
  skip <- ifelse(!is.na(args[3]) & args[3]=="skip", 1, 0)
  ## Files to collapse, if arg3 given and not skip then use that pick what to collapse
  files <- collapse.files(me)
  if (!is.na(args[3]) & args[3] != "skip") to.collapse <- args[3] %>% as.integer else to.collapse <- 1:length(files)
  ## Set up folders
  out.root <- paste0('/share/scratch/users/pyliu/collapse/', me)
  temp.root <- paste0(out.root, '/data')
  error.root <- paste0(out.root, '/errors')
  if (!skip) {
  if (is.na(args[3]))  unlink(out.root, recursive = TRUE) ## Don't delete if provided an id
  dir.create(out.root)
  dir.create(temp.root)
  dir.create(error.root)
  # Launch
  for (i in to.collapse) {
    job_name <- paste0("collapse_", me, "_", i)
    script <- paste0(code_root, "/extraction/collapse.r")
    slots <- 2
    memory <- 4
    cluster_project <- "proj_covariates"
    output.root <- out.root
    arguments <- c("collapse", me, i , output.root)
    qsub(job_name=job_name, script=script, slots=slots, memory=memory, cluster_project=cluster_project, arguments=arguments)
  }
  ## Hold
  job_hold(job_name=paste0("collapse_", me))
  }
  ## Append
  temp.files <- list.files(temp.root)
  error.files <- list.files(error.root)
  completed <- c(temp.files, error.files) %>% gsub(".csv", "", .)
  check <- setdiff(to.collapse, completed)
  if (length(check)<20) {
    temp.files <- list.files(temp.root, full.names=TRUE)
    error.files <- list.files(error.root, full.names=TRUE)
    data <- lapply(temp.files, fread) %>% rbindlist(., fill=TRUE)
    if (length(error.files)>0) errors <- lapply(error.files, fread) %>% rbindlist else errors <- NULL
    save <- list(data=data, errors=errors)
    ## Save if running all
    if (is.na(args[3]) | args[3]=="skip") {
      saveRDS(save, paste0(data_root, "/exp/collapsed/", me, ".rds"))
      unlink(out.root, recursive=TRUE)
      print("Saved!")
    }
    print("DONE!")
  } else {
    fubar <- lapply(check, function(x) files[x]) %>% unlist
    print(check)
    print(fubar)
    
  }
  

}

###########################################################################################################################
## COLLAPSE

if (args[1]=="collapse") {
  ## Arguments
  me <- args[2]
  i <- args[3] %>% as.integer
  output.root <- args[4]
  ## Collapse
  out <- collapse.run(me, i)
  data <- out[[1]]
  errors <- out[[2]]
  ## Save
  if (!is.null(data)) write.csv(data, paste0(output.root, "/data/", i, ".csv"), row.names=FALSE, na="")
  if (!is.null(errors)) write.csv(errors, paste0(output.root, "/errors/", i, ".csv"), row.names=FALSE, na="")
}

###########################################################################################################################
}







       
