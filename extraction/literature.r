###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Prep Vaccination Lit Extractions
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr, parallel, readxl)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

source(db_tools)

## Literature data
   key <- "1McVHFtjwMcH9KylDJsRQgEWGZWrOT7lbiFS2lESbm4c"
   gs_download <- function(key) {
   return(fread(paste0("https://docs.google.com/spreadsheets/d/", key, "/export?format=csv")))
}
df <- gs_download(key)


#--PREPPING---------------------------------------------------------------------------------

## For DPT, MCV, HiB; If have PENTA, MMR and higher coverage swap in
duples <- list(c("vacc_dpt3", "vacc_tetra3"),
               c("vacc_dpt3", "vacc_pent3"),
               c("vacc_hib3", "vacc_pent3"),
               c("vacc_hib3", "vacc_tetra3"),
               c("vacc_mcv1", "vacc_mmr1"),
               c("vacc_mcv2", "vacc_mmr2"))
for (i in duples) {
  ## Eg. if !is.na(penta) & penta > dpt | !is.na(penta) & is.na(dpt), replace dpt with penta
  df <- df[, (i[1]) := ifelse((!is.na(get(i[2])) & get(i[2]) > get(i[1])) | (!is.na(get(i[2])) & is.na(get(i[1]))), get(i[2]), get(i[1]))]
}

#--RESHAPING---------------------------------------------------------------------------------

## Location
df <- df[, ihme_loc_id := tstrsplit(location_name_short_ihme_loc_id, "[|]")[[2]]]

## Reshape
id <- c("nid", "file_path", "ihme_loc_id", "year_start", "year_end", "age_start", "age_end", "sample_size")
vacc <-  grep("vacc", names(df), value=TRUE)

## Reshape
id <- c("nid", "file_path", "ihme_loc_id", "year_start", "year_end", "age_start", "age_end", "sample_size")
df <- melt(df, id=id, measure=patterns("^vacc"), value.name="data", variable.name="me_name")

#--CLEANING---------------------------------------------------------------------------------

## Drop rows where is.na(data)
df <- df[!is.na(data)]
## Divide coverage by 100
df <- df[, data := data/100]
## Cap coverage at 1
df <- df[data > 1, data := 1]
## Drop anything without mapped ihme_loc_id
df <- df[!is.na(ihme_loc_id)]
## Remove sample size if > 1500, usually refers to target population
df <- df[sample_size > 1500, sample_size := NA]
## Ages --> Round to single year ages, assume age_year = 1 otherwise
df <- df[, `:=`(age_start = round(age_start/12), age_end =round(age_end/12))]
df <- df[, age_length := age_end - age_start]
df <- df[age_length %in% c(0,1), age_year := age_start]
df <- df[is.na(age_length), age_year := 1]
df <- df[age_length > 1, age_year := 1]
df <- df[, year_id := year_start]; df <- df[, c("year_start", "year_end") := NULL]
## Survey Name
parsed <- df[, tstrsplit(file_path, "/", fixed=TRUE)]
parsed <- parsed[, survey_name := ifelse(nchar(V3)==3, paste0(V3, "/", V4), V3)]
df <- cbind(df, parsed$survey_name); setnames(df, "V2", "survey_name")

#--SAVE-------------------------------------------------------------------------------------

saveRDS(df, paste0(data_root, "/exp/reference/vaccine_lit.rds"))