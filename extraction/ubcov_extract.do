
if c(os) == "Unix" {
	local j "/home/j"
	local h "/snfs2/HOME/`c(username)'"
	set odbcmgr unixodbc
}
else if c(os) == "Windows" {
	local j "J:"
	local h "H:"
}

clear all
set more off
set obs 1

// Settings
local central_root "`h'/git/ubcov_central"
local output_root "/ihme/covariates/hsa/extract/raw"
local topics vaccination //maternal demographics_child


// Load functions
cd "`central_root'"
do "`central_root'/modules/extract/core/load.do"

// Subset ids with info
if regexm("`topics'", "vaccination") {
init, topics(vaccination)
get, vars
levelsof var_name if topic_name == "vaccination", l(vars) clean
local n : list sizeof vars
get, codebook
egen keep = rowmiss(`vars')
keep if keep < `n'
levelsof ubcov_id, l(ubcov_ids) clean
}
if "`topics'" == "lri" {
	init, topics(`topics')
	get, codebook
	levelsof ubcov_id if !mi(lri_tx_type), l(ubcov_ids) clean 
}
if regexm("`topics'", "maternal") {
init, topics(maternal)
get, vars
levelsof var_name if topic_name == "maternal", l(vars) clean
local n : list sizeof vars
get, codebook
egen keep = rowmiss(`vars')
keep if keep < `n'
levelsof ubcov_id, l(ubcov_ids) clean
}

// topic clean
local main_topic = word("`topics'", 1)

// Clear folders
!rm -rf "`output_root'/`main_topic'"
!mkdir "`output_root'/`main_topic'"

// Run
batch_extract, topics(`topics') ubcov_ids(`ubcov_ids') /// 
				central_root(`central_root') ///
				cluster_project(proj_covariates) ///
				output_path("`output_root'/`main_topic'") ///
				store_vals_path("`j'/temp/pyliu/scratch/maps") logs_path("`j'/temp/pyliu/scratch/logs") ///
				run_log_path("`j'/temp/pyliu/scratch") ///
				db_path("`j'/temp/pyliu/scratch/ubcov")
