###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Full Vaccination Coverage
###	
###		Script which goes back through ubCov extractions
###		and, using the national schedule by year, creates
###		binary indicators for whether or not all vaccinations in the
###		national schedule was received (full) or if DPT, MCV, Polio, BCG
###		was received (full_sub). 
###
###########################################################

###################
### Setting up ####
###################
rm(list=ls())

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Setup
sched <- readRDS(paste0(data_root, "/exp/reference/vaccine_schedule.rds"))

##--FUNCTIONS----------------------------------------------------------------

## Function to get the relevant doses
vacc.dose <- function(vacc, iso3) {
    if (vacc %in% c("dpt", "polio", "hib", "hepb", "pcv")) {
        return(paste0(vacc, "3"))
    } else if (vacc %in% ("mcv")) {
        return(paste0(vacc, "1"))
    } else if (vacc %in% ("rota")) {
        num <- sched[ihme_loc_id==iso3]$doses
        return(paste0(vacc, num))
    } else {
        return(vacc)
    }
}

## Assess microdata for completeness of vaccine scheduling
get.data_summary <- function(path) {
    df <- fread(path, nrow=1)
    meta <- c("nid", "survey_name", "ihme_loc_id", "year_start", "year_end", "survey_module", "file_path")
    cols <- grep("_dose", names(df), value=TRUE)
    cols.clean <- gsub("_dose", "", cols)
    df <- df[, (cols.clean) := 1]
    df <- df[, c(meta, cols.clean), with=F]
    df <- df[, path := path]
    return(df)    
}

## Go through each file, create full_vacc indicator, then save
calc.full_vacc <- function(frame, row, sub=FALSE) {
    ## Frame to pull from
    if (sub) {
        frame.l <-  melt(frame[row, .(bcg_intro)])
        var <- "fullsub"
    } else {
        frame.l <-  melt(frame[row, .(bcg_intro, hepb_intro, pcv_intro, rota_intro, hib_intro)])
        var <- "full"
    }
    vacc <- gsub("_intro", "", frame.l[value > 0]$variable)
    vacc <- c(vacc, "dpt", "mcv", "polio")
    vacc <- lapply(vacc, function(x) vacc.dose(iso3=frame[row]$ihme_loc_id, vacc=x)) %>% unlist
    ## Open up dataset and calculate vacc_full
    df <- frame[row]$path %>% fread
    if (all(vacc %in% names(df))) {
    subset <- paste0(paste0("!is.na(", vacc, ")"), collapse = " & ")
    cond <- paste0(paste0(vacc, "==1"), collapse = " & ")
    df <- df[eval(parse(text=subset)) , (var) := ifelse(eval(parse(text=cond)), 1, 0)]
    ## Save
    write.csv(df, frame[row]$path, na="", row.names=F)
    return(paste0("Saved ", frame[row]$path))
    }
}

##----------------------------------------------------------------------------

## Create introduction frame which contains what vaccinations are in national schedule over time

## Set up intro frame
mes <- c("vacc_dpt3", "vacc_mcv1", "vacc_bcg", "vacc_polio3", "vacc_hib3", "vacc_hepb3", "vacc_pcv3", "vacc_rotac")
intro <- readRDS(paste0(data_root, "/exp/reference/vaccine_intro.rds"))
intro <- intro[me_name %in% mes]
intro <- intro[, me_name := paste0(me_name, "_intro")]
## Create cv_intro_years frame to represent the number of years
intro <- intro[, cv_intro_years := ifelse((year_id-(cv_intro-1))>=1, year_id-(cv_intro-1), 0)]
intro <- intro[!is.na(cv_outro), cv_intro_years := ifelse((cv_outro - year_id)>0, year_id - 1980 + 1, 0)]
## Reshape wide
intro.w <- dcast(intro, location_id + year_id ~ me_name, value.var='cv_intro_years')
old <- grep("vacc", names(intro.w), value=TRUE)
new <- gsub("vacc_|[0-9]", "", old)
setnames(intro.w, old, new)
locs <- get_location_hierarchy(149)[, .(location_id, ihme_loc_id)]
intro.w <- merge(intro.w, locs, by="location_id", all.x=TRUE)
setnames(intro.w, c("year_id", "rotac_intro"), c("year_start", "rota_intro"))


## Get list of datasets and the vaccinations asked in each

files <- list.files(extract_root, full.names=TRUE)
df.list <- mclapply(files, get.data_summary) %>% rbindlist(., fill=TRUE, use.names=TRUE)


##--CALCULATE FULL-----------------------------------------------------------

df <- merge(df.list, intro.w, by=c("ihme_loc_id", "year_start"), all.x=TRUE)
vaccs <- c("dpt", "polio", "mcv", "bcg", "hepb", "hib", "pcv", "rota")

## What is missing for each
df <- df[, any_missing := 0]
for (vacc in vaccs) {
    ## Clean up
    df <- df[, (vacc) := ifelse(is.na(get(vacc)), 0, 1)]
    ## If dpt, polio, mcv
    if (vacc %in% c("dpt", "polio", "mcv")) {
        df <- df[, paste0(vacc, "_missing") := ifelse(get(vacc)==0, 1, 0)]
    }
    ## If bcg, hepb, hib, pcv, rota
    if (vacc %in% c("bcg", "hepb", "hib", "pcv", "rota")) {
        df <- df[, paste0(vacc, "_missing") := ifelse(get(vacc)==0 & get(paste0(vacc, "_intro")) > 0 , 1, 0)]
    }
    ## Any missing
    df <- df[, any_missing := ifelse(get(paste0(vacc, "_missing"))==1, 1, any_missing)]
}

## Subset to surveys that aren't missing anything
df.s <- df[any_missing==0]

## Calculate through
mclapply(1:nrow(df.s), function(x) calc.full_vacc(frame=df.s, row=x), mc.cores=10)

##--CALCULATE FULL_SUB-------------------------------------------------------

df <- merge(df.list, intro.w, by=c("ihme_loc_id", "year_start"), all.x=TRUE)
vaccs <- c("dpt", "polio", "mcv", "bcg")

## What is missing for each
df <- df[, any_missing := 0]
for (vacc in vaccs) {
    ## Clean up
    df <- df[, (vacc) := ifelse(is.na(get(vacc)), 0, 1)]
    ## If dpt, polio, mcv
    if (vacc %in% c("dpt", "polio", "mcv")) {
        df <- df[, paste0(vacc, "_missing") := ifelse(get(vacc)==0, 1, 0)]
    }
    ## If bcg, hepb, hib, pcv, rota
    if (vacc %in% c("bcg", "hepb", "hib", "pcv", "rota")) {
        df <- df[, paste0(vacc, "_missing") := ifelse(get(vacc)==0 & get(paste0(vacc, "_intro")) > 0 , 1, 0)]
    }
    ## Any missing
    df <- df[, any_missing := ifelse(get(paste0(vacc, "_missing"))==1, 1, any_missing)]
}

## Subset to surveys that aren't missing anything
df.s <- df[any_missing==0]

## Calculate through
mclapply(1:nrow(df.s), function(x) calc.full_vacc(frame=df.s, row=x, sub=TRUE), mc.cores=10)