###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Data Coverage
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr, parallel)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Source
source(db_tools)

#--FUNCTIONS----------------------------------------------------------------------

data.coverage <- function(df, by) {
  ## Location hierarchy
  locs <- get_location_hierarchy(location_set_version_id)[level>=3, .(ihme_loc_id, location_name)]
  square <- expand.grid(ihme_loc_id=locs$ihme_loc_id, year_id=year_start:year_end) %>% data.table
  ## Data
  df.sub <- df[!is.na(data), .(ihme_loc_id, year_id, data)]
  ## Coverage
  df.cov <- merge(square, df.sub, by=c("ihme_loc_id", "year_id"), all.x=TRUE)
  df.cov <- df.cov[, n := lapply(.SD, function(x) length(x[!is.na(x)])), .SDcols="data", by=by]
  ## Return
  df.cov <- df.cov[, c(by, "n"), with=F] %>% unique
  return(df.cov)
}

mes <- c("vacc_dpt3", "vacc_mcv1", "vacc_rota1", "vacc_hib3", "vacc_pcv3")

count <- 1
for (me in mes) {
  df <- paste0(data_root, "/exp/to_model/", me, ".csv") %>% fread
  df.cov <- data.coverage(df, "ihme_loc_id")
  setnames(df.cov, "n", paste0("n_", me))
  if (count==1) df.out <- df.cov
  if (count>1) df.out <- merge(df.out, df.cov, by="ihme_loc_id")
  count <- count + 1
}
