###########################################################
### Author: Zane Rankin
### Date: 1/26/2015
### Project: ubCov
### Purpose: Collapse ubcov extraction output 
### DOCUMENTATION: https://hub.ihme.washington.edu/display/UBCOV/Collapse+Documentation
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Load Functions
ubcov_central <- paste0(j, "/WORK/01_covariates/common/ubcov_central/")
setwd(ubcov_central)
source("modules/collapse/launch.r")

######################################################################################################################

args <- commandArgs(trailingOnly = TRUE)

if (length(args)!=0) {

###########################################################################################################################
## LAUNCH

topic <- args[1] #args[1] ## Subset config.csv
config.path <- paste0(j, "/WORK/01_covariates/02_inputs/hsa/code/reference/other/collapse_config.csv") ## Path to config.csv
parallel <- TRUE ## Run in parallel?
slots <- 3 ## How many slots per job (used in mclapply)
logs <- NA ## Path to logs
cluster_project <- "proj_covariates"

## Launch collapse

df <- collapse.launch(topic=topic, config.path=config.path, parallel=parallel, logs=logs, cluster_project=cluster_project)

}

