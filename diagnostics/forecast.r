###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Dashboards
###				
###########################################################

###################
### Setting up ####
###################

rm(list=ls())
pacman::p_load(data.table, dplyr, ggplot2, gridExtra, grid)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))
  
## Source
setwd(paste0(j, "/WORK/01_covariates/common/ubcov_central"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/utilitybelt/db_tools.r"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/cluster_tools.r"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/ubcov_tools.r"))

## Location
locs <- get_location_hierarchy(location_set_version_id)

## Me db
me.db <- paste0(code_root, "/reference//me_db.csv") %>% fread

## Model root
model_root <- paste0(data_root, "/exp/forecasting/modeled")

## Entities
mes <- c("vacc_pcv3", "vacc_rotac")

###########################################################################################################################
# 														function
###########################################################################################################################

g_legend <- function(a.gplot){
  tmp <- ggplot_gtable(ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}

not.missing <- function(df, names) {
  out <- NULL
  vals <- intersect(names, names(df))
  for (val in vals) if (nrow(df[!is.na(get(val))])>0) out <- c(out, val)
  return(out)
}

load.data <- function(mes) {
  
  ## LOAD MODELED OUTPUT
  files <- paste0(model_root, "/", mes, ".rds")
  df <- lapply(files, function(x) readRDS(x) %>% data.table) %>% rbindlist(., fill=TRUE)
  
  ## Load forecasted dpt3
  dpt <- fread(paste0(data_root, "/exp/forecasting/ref/dpt3_sum.csv"))
  old <- c("yhat", "lower_bound", "upper_bound")
  new <- c("gpr_mean", "gpr_lower", "gpr_upper")
  setnames(dpt, old, new)
  dpt <- dpt[,me_name := "vacc_dpt3"]
  df <- rbind(df, dpt, fill=TRUE, use.names=TRUE)
  
  ## Vaccinations
  data <- readRDS(paste0(data_root, "/exp/to_model/vaccination.rds"))[me_name %in% mes]
 
  ## Merge
  data <- data[, c("location_name", "location_id") := NULL]
  df <- merge(df, locs[level>=3, .(location_id, location_name, ihme_loc_id, region_id, level, parent_id)], by='location_id', all.x=TRUE)
  df <- merge(df, data, by=c('me_name', 'ihme_loc_id', 'year_id', 'age_group_id', 'sex_id'), all=TRUE)
  
  ## CLEAN
  df <- df[cv_admin==1, admin := data]
  df <- df[cv_admin==1, data := NA]
  
  return(df)
}

plot.graph <- function(df, loc, sex, me, legend) {
  #---------------------------------------------------------
  ## SUBSET
  df <- df[location_id==loc & sex_id == sex & me_name==me]
  #---------------------------------------------------------
  ## Setup
  obj.names <- c("data", "admin", "outlier",  "gpr_mean")
  obj.labels <- c("Data", "Admin", "Outlier",    "GPR") 
  obj.colors <- c("black", "red", "black",   "#218944")
  obj.df <- data.table(names=obj.names, labels=obj.labels, colors=obj.colors)
  ## Main Graphing Objects
  p.data <- geom_point(aes(y=data, x=year_id, color="Data"))
  p.outlier <- geom_point(aes(y=cv_outlier, x=year_id, color="Outlier"), shape=1)
  p.gpr_mean <- geom_line(aes(y=gpr_mean, x=year_id, color="GPR"))
  p.gpr_ci <- geom_ribbon(aes(ymin=gpr_lower, ymax=gpr_upper, x=year_id), fill=obj.df[names=="gpr_mean"]$color, alpha=0.2)
  ## Other
  p.cv_intro <- geom_vline(aes(xintercept=cv_intro), color="black", linetype='longdash')
  p.admin <- geom_point(aes(y=admin, x=year_id, color="Admin"))
  p.admin_orig <- geom_point(data=df[cv_admin==1], aes(y=cv_admin_orig, x=year_id, color="Admin"), shape=3)
  p.admin_bias <- geom_ribbon(data=df[cv_admin==1], aes(ymin=admin, ymax=cv_admin_orig, x=year_id), alpha=0.1)
  #--------------------------------------------------------
  ## Set axis range
  y.min <- 0
  y.max <- 1
  x.min <- 1980
  x.max <- 2040
  ## Set variables to graph
  obj.graph <- not.missing(df, obj.names)
  if (all(c("gpr_upper", "gpr_lower") %in% names(df))) obj.graph <- c(obj.graph, "gpr_ci")
  if ("cv_intro" %in% names(df)) obj.graph <- c(obj.graph, "cv_intro")
  if (nrow(df[!is.na(cv_admin_orig)])) obj.graph <- c(obj.graph, c("admin", "admin_bias"))
  if ("cv_outlier" %in% names(df)) obj.graph <- c(obj.graph, "outlier")
  #--------------------------------------------------------
  ## GRAPH
  p <- ggplot(df)
  ## Graph objects
  for (obj in obj.graph)  p <- p + get(paste0("p.", obj))
  ## Data
  p <- p + p.data
  ## Colors
  p <- p + scale_color_manual(values=setNames(obj.colors, obj.labels)) +
    ## Appearance
    xlab("") + ylab("") +
    coord_cartesian(ylim=c(y.min, y.max), xlim=c(x.min, x.max))  + 
    theme_bw()+
    theme(axis.title=element_text(),
          plot.title=element_text(size=10),
          strip.text=element_text(size=12, face ="bold"),
          strip.background=element_blank(),
          axis.text.x = element_text(size = 9),
          legend.position = legend,
          legend.title = element_blank(),
          legend.background = element_blank(),
          legend.key = element_blank()
    ) +
    ggtitle(me)
  return(p)
}

#---------------------------------------------------------------------------------------------------------------------------

plot.vacc <- function(df, loc) {
  
  ## Get location data
  vars <- c("location_name", "ihme_loc_id", "region_id", "level", "parent_id")
  meta.loc <- lapply(vars, function(x) locs[location_id==loc][[x]])
  names(meta.loc) <- vars
  
  ## Plot
  plots <- lapply(c(mes, "vacc_dpt3"), function(x) plot.graph(df, loc, 3, x, "none"))
  names(plots) <- c(mes, "vacc_dpt3")
  legend <- plot.graph(df, 102, 3, "vacc_pcv3", "bottom") %>% g_legend
  
  ## Arrange
  grob.top <- arrangeGrob(plots$vacc_dpt3, nrow=1)
  grob.bottom <- arrangeGrob(plots$vacc_pcv3, plots$vacc_rotac, nrow=1)
  grob.main <- arrangeGrob(grob.top, grob.bottom, nrow=2)
  grob.title <- textGrob(paste0(meta.loc$ihme_loc_id, " (", meta.loc$location_name, ")"), gp=gpar(fontsize=20, font=3))
  grob.full <- arrangeGrob(grob.title, grob.main, legend, nrow=3, heights=c(1, 15, 0.5))
  
  return(grob.full)
}

###########################################################################################################################
# 														run
###########################################################################################################################

args <- commandArgs(trailingOnly = TRUE)

if (length(args)!=0) {
  
  ###########################################################################################################################
  
  if (args[1]=="launch") {
    
    ## ARGUMENTS
    graph <- args[2]
    if (!is.na(args[3])) location_id <- args[3] else location_id <- get_location_hierarchy(79)[level>=3]$location_id
    output.path <- paste0(diagnostics_root, "/", graph, ".pdf")
    
    ## PREP DIRECTORIES
    temp.root <- paste0("/share/scratch/users/pyliu/graph/", graph)
    if (is.na(args[2])) unlink(temp.root, recursive=TRUE)
    dir.create(temp.root)
    
    ## LAUNCH
    for (loc in location_id) {
      job_name <- paste0("graph_", graph, "_", loc)
      script <- paste0(code_root, "/diagnostics/forecast.r")
      slots <- 2
      memory <- 4
      cluster_project <- "proj_covariates"
      output <- paste0(temp.root, "/", loc, ".pdf")
      arguments <- c("graph", loc, output)
      qsub(job_name=job_name, script=script, slots=slots, memory=memory, cluster_project=cluster_project, arguments=arguments)
    }
    
    ## HOLD
    job_hold(job_name=paste0("graph_", graph))
    
    ## APPEND
    locs.hierarchy <- get_location_hierarchy(79)[level>=3, .(location_id, region_id)]
    locs <- locs.hierarchy[order(region_id), location_id] %>% unique
    locs <- locs[locs %in% as.numeric(gsub(".pdf", "", list.files(temp.root, ".pdf")))]
    files <- gsub(",", "", toString(paste0(temp.root, "/", locs, ".pdf")))
    append_pdf(files, output.path)
    
    ## Clear
    unlink(temp.root)
    
    
  }
  
  ###########################################################################################################################
  
  if (args[1]=="graph") {
    ## Settings
    location_id <- args[2]
    output <- args[3]
    df <- load.data(mes)
    p <- plot.vacc(df, location_id)
    ## PRINT
    pdf(output, w=25, h=10)
    grid.arrange(p)
    dev.off()
  }  
  
  ###########################################################################################################################
  
}





