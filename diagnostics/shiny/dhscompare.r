###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: 
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(shiny, shinydashboard, plotly, data.table, dplyr, readxl)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Load
source(db_tools)

## Me DB
me.db <- paste0(code_root, "/reference/me_db.csv") %>% fread
## DHS
df.dhs <- read_excel(paste0(data_root, "/raw/statcompiler/statcompiler.xlsx")) %>% data.table
## Locations
locs <- get_location_hierarchy(location_set_version_id)

###########################################################################################################################
# 														thingy
###########################################################################################################################

prep_dhs <- function(df){
## Prep DHS
old <- c("Country Name", "Survey Year", "Indicator", "By Variable", "Characteristic Category")
new <- c("location_name", "year_id", "dhs_name", "by_var", "dhs_data")
setnames(df, old, new)
df <- df[, new, with=F]
## Merge me_name
df <- merge(df, me.db[, .(dhs_name, me_name)], by='dhs_name', all.x=TRUE)
df <- df[!is.na(me_name) & me_name != "" & !is.na(dhs_data)]
df <- df[, dhs_data := dhs_data/100]
## Subset to 5 years prior
df <- df[by_var=="Five years preceding the survey"]
df <- df[, c("by_var", "dhs_name") := NULL]
## Location names
df <- df[location_name == "Congo Democratic Republic", location_name := "Democratic Republic of the Congo"]
df <- df[location_name == "Gambia", location_name := "The Gambia"]
df <- df[location_name == "Kyrgyz Republic", location_name := "Kyrgyzstan"]
df <- merge(df, locs[, .(location_name, ihme_loc_id)], by='location_name', all.x=TRUE)
return(df)
}

## Prep DHS
df <- prep_dhs(df.dhs)[grepl("maternal", me_name)]
df.maternal <- readRDS(paste0(data_root, "/exp/collapsed/maternal.rds"))[['data']][grepl("DHS", survey_name)]
df <- merge(df, df.maternal, by=c("year_id", "ihme_loc_id", "me_name"), all.x=TRUE)

## Choices
me_names <- unique(df$me_name)

##########################################################################################
## UI

ui <- dashboardPage(
  ## HEADER
  dashboardHeader(
    title="DHS Compare"
  ),
  ## SIDEBAR
  dashboardSidebar(
    sidebarMenu(
      menuItem("DHS Scatter", tabName = "tab_scatter"),
      selectInput("me_name", "Entity", choices=me_names)
    )
  ),
  ## BODY
  dashboardBody(
    tabItems(
      tabItem(tabName="tab_scatter",
            plotlyOutput("plot_scatter")
      )
    )
  )
)

##########################################################################################
## SERVER

server <- function(input,output,session) {
  ## TIME SERIES PLOT
  output$plot_scatter <- renderPlotly({
    p <- ggplot(df[me_name==input$me_name]) +
        geom_point(aes(y=dhs_data, x=data, label=year_id, label2=ihme_loc_id, label3=file_path)) +
        geom_abline(slope=1, intercept=0) +
        coord_cartesian(ylim=c(0, 1), xlim=c(0, 1)) +
        ggtitle(input$me_name)
        
    ggplotly(p, tooltip=c("year_id", "ihme_loc_id"))
  })
  ## DATA COVERAGE
  # output$table_datacoverage <- renderDataTable(
  #   coverage,
  #   class = 'cell-border stripe', filter='top', selection='single',
  #   extensions = list('FixedColumns', 'Scroller'),
  #   rownames=FALSE,
  #   options = list(
  #     ## Scroller
  #     scroller = TRUE, deferRender = TRUE, scrollY = 250, scrollX = TRUE,
  #     ## Other
  #     paging = FALSE 
  #   )
  # )
}

shinyApp(ui,server)

