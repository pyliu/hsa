###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Dashboards
###				
###########################################################

###################
### Setting up ####
###################

rm(list=ls())
pacman::p_load(data.table, dplyr, ggplot2, gridExtra, grid)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Source
setwd(paste0(j, "/WORK/01_covariates/common/ubcov_central"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/utilitybelt/db_tools.r"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/cluster_tools.r"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/ubcov_tools.r"))

## Location
locs <- get_location_hierarchy(location_set_version_id)

## Me db
me.db <- paste0(code_root, "/reference//me_db.csv") %>% fread

## Model root
model_root <- paste0(data_root, "/exp/modeled/best")

## Entities

## FOR HSA
mes.hsa <- c("hsa", "hsa_capped", "hsa2")
mes.vacc <-  c("vacc_dpt3", "vacc_mcv1")
mes.maternal <- c("maternal_anc1", "maternal_anc4", "maternal_sba", "maternal_ifd")
mes.hsa.full <- c(mes.hsa, mes.vacc, mes.maternal)

## OTHER
mes.vacc <- c("vacc_dpt3", "vacc_mcv1", "vacc_bcg", "vacc_polio3", "vacc_hib3", "vacc_hepb3", "vacc_rotac", "vacc_pcv3")
mes.vacc_full <- c(mes.vacc, "vacc_full_nodelay", "vacc_full_2yr", "vacc_full_5yr", "vacc_full_10yr")

## GBD2016 Baseline Estimates
baseline.path <- paste0(data_root, "/exp/modeled/gbd2015")

###########################################################################################################################
# 														function
###########################################################################################################################

g_legend <- function(a.gplot){
  tmp <- ggplot_gtable(ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}

not.missing <- function(df, names) {
  out <- NULL
  vals <- intersect(names, names(df))
  for (val in vals) if (nrow(df[!is.na(get(val))])>0) out <- c(out, val)
  return(out)
}

get_cov <- function(me) {
  cov.name <- me.db[me_name==me]$covariate_name_short
  #df <- get_covariates(cov.name)
  if (file.exists(paste0(baseline.path, "/", cov.name, ".csv"))){
    df <- paste0(baseline.path, "/", cov.name, ".csv") %>% fread
    df <- df[, .(location_id, year_id, age_group_id, sex_id, mean_value)]
    setnames(df, "mean_value",  "gpr_2015")
    df <- df[, me_name := me]
    return(df)
  } 
}


load.data <- function(mes) {
## LOAD MODELED OUTPUT
df <- lapply(mes, function(x) {
  path <- paste0(model_root, "/", x, ".rds")
  df <- readRDS(path) %>% data.table
  df <- df[, me_name := x]
}) %>% rbindlist(., fill=TRUE)
## CLEAN
df <- df[, c("cv_intro", "cv_intro_years") := NULL]

## GET INPUT DATA
## MATERNAL
files <- paste0(data_root, "/exp/to_model/", mes.maternal, ".csv")
df.maternal <- lapply(files, fread) %>% rbindlist(., fill=TRUE)
df.maternal$location_id <- NULL
## Vaccinations
df.vacc <- readRDS(paste0(data_root, "/exp/to_model/vaccination.rds"))
df.data <- rbind(df.maternal, df.vacc, fill=TRUE) %>% data.table

## Merge
df.data <- df.data[, c("location_name", "location_id") := NULL]
df <- merge(df, locs[level>=3, .(location_id, location_name, ihme_loc_id, region_id, level, parent_id)], by='location_id', all.x=TRUE)
df <- merge(df, df.data, by=c('me_name', 'ihme_loc_id', 'year_id', 'age_group_id', 'sex_id'), all=TRUE)

## Pull covariates
gbd2015 <- lapply(mes, get_cov) %>% rbindlist(., use.names=TRUE)
df <- merge(df, gbd2015, by=c('location_id', 'year_id', 'age_group_id', 'sex_id', 'me_name'), all.x=TRUE)

## CLEAN
df <- df[cv_admin==1, admin := data]
df <- df[cv_admin==1, data := NA]

return(df)
}

plot.graph <- function(df, loc, sex, me, legend, data=TRUE) {
  #---------------------------------------------------------
  ## SUBSET
  df <- df[location_id==loc & sex_id == sex & me_name==me]
  #---------------------------------------------------------
  ## Setup
  obj.names <-  c("gpr_mean", "gpr_2015")
  obj.labels <- c("GPR" , "GPR 2015") 
  obj.colors <- c("#218944", "purple")
  if (data) {
  obj.names <- c(c("data", "admin", "outlier"), obj.names)
  obj.labels <- c(c("Data", "Admin", "Outlier"), obj.labels)
  obj.colors <- c(c("black", "red", "black"), obj.colors)
  }
  obj.df <- data.table(names=obj.names, labels=obj.labels, colors=obj.colors)
  ## Main Graphing Objects
  p.data <- geom_point(aes(y=data, x=year_id, color="Data"))
  p.outlier <- geom_point(aes(y=cv_outlier, x=year_id, color="Outlier"), shape=1)
  p.gpr_mean <- geom_line(aes(y=gpr_mean, x=year_id, color="GPR"))
  p.gpr_2015 <- geom_line(aes(y=gpr_2015, x=year_id, color="GPR 2015"))
  p.gpr_ci <- geom_ribbon(aes(ymin=gpr_lower, ymax=gpr_upper, x=year_id), fill=obj.df[names=="gpr_mean"]$color, alpha=0.2)
  ## Other
  p.cv_intro <- geom_vline(aes(xintercept=cv_intro), color="black", linetype='longdash')
  if (data) {
  p.admin <- geom_point(aes(y=admin, x=year_id, color="Admin"))
  p.admin_orig <- geom_point(data=df[cv_admin==1], aes(y=cv_admin_orig, x=year_id, color="Admin"), shape=3)
  p.admin_bias <- geom_ribbon(data=df[cv_admin==1], aes(ymin=admin, ymax=cv_admin_orig, x=year_id), alpha=0.1)
  }
  #--------------------------------------------------------
  ## Set axis range
  y.min <- 0
  y.max <- 1
  x.min <- 1980
  x.max <- 2016
  if (me == "hsa") {y.min <- -5; y.max <- 20}
  if (me == "hsa_capped") { y.min <- 0; y.max <- 5}
  if (me == "hsa2") { y.min <- -5; y.max <- 3}
  ## Set variables to graph
  obj.graph <- not.missing(df, obj.names)
  if (all(c("gpr_upper", "gpr_lower") %in% names(df))) obj.graph <- c(obj.graph, "gpr_ci")
  if ("cv_intro" %in% names(df)) obj.graph <- c(obj.graph, "cv_intro")
  if ("cv_admin_orig" %in% names(df)) if (nrow(df[!is.na(cv_admin_orig)])) obj.graph <- c(obj.graph, c("admin", "admin_bias"))
  if ("cv_outlier" %in% names(df)) obj.graph <- c(obj.graph, "outlier")
  #--------------------------------------------------------
  ## GRAPH
  p <- ggplot(df)
  ## Graph objects
  for (obj in obj.graph)  p <- p + get(paste0("p.", obj))
  ## Data
  if (data) p <- p + p.data
  ## Colors
  p <- p + scale_color_manual(values=setNames(obj.colors, obj.labels)) +
    ## Appearance
    xlab("") + ylab("") +
    coord_cartesian(ylim=c(y.min, y.max), xlim=c(x.min, x.max))  + 
    theme_bw()+
    theme(axis.title=element_text(),
          plot.title=element_text(size=10),
          strip.text=element_text(size=12, face ="bold"),
          strip.background=element_blank(),
          axis.text.x = element_text(size = 9),
          legend.position = legend,
          legend.title = element_blank(),
          legend.background = element_blank(),
          legend.key = element_blank()
    ) +
    ggtitle(me)
  return(p)
}


plot.vacc_full <- function(df, loc, me, legend="none") {
  ## Graph
  p <- ggplot(df[location_id==loc]) +
    geom_line(aes(y=gpr_mean, x=year_id, color=me_name, group=me_name)) +
    geom_line(data=df[location_id == loc & me_name == me], aes(y=gpr_mean, x=year_id), color="black", size=1, linetype=2) +
    geom_ribbon(data=df[location_id == loc & me_name == me], aes(ymin=gpr_lower, ymax=gpr_upper, x=year_id), fill="black", alpha=0.2) +
    ## Appearance
    xlab("") + ylab("") +
    coord_cartesian(ylim=c(0, 1), xlim=c(1980, 2016))  + 
    theme_bw()+
    theme(axis.title=element_text(),
          plot.title=element_text(size=10),
          strip.text=element_text(size=12, face ="bold"),
          strip.background=element_blank(),
          axis.text.x = element_text(size = 9),
          legend.position = legend,
          legend.title = element_blank(),
          legend.background = element_blank(),
          legend.key = element_blank()
    ) +
    ggtitle(me)
  return(p)
}


#---------------------------------------------------------------------------------------------------------------------------

make.hsa <- function(df, loc) {
  
  ## Get location data
  vars <- c("location_name", "ihme_loc_id", "region_id", "level", "parent_id")
  meta.loc <- lapply(vars, function(x) locs[location_id==loc][[x]])
  names(meta.loc) <- vars
  
  ## Plot
  plots <- lapply(mes.hsa.full, function(x) plot.graph(df, loc, 3, x, "none"))
  names(plots) <- mes.hsa.full
  legend <- plot.graph(df, 102, 3, "vacc_dpt3", "bottom") %>% g_legend
  
  ## Arrange
  grob.top <- arrangeGrob(plots$hsa, plots$hsa_capped, plots$hsa2, nrow=1)
  grob.bottom <- arrangeGrob(plots$maternal_anc1, plots$maternal_anc4, plots$vacc_dpt3, 
                             plots$maternal_sba, plots$maternal_ifd, plots$vacc_mcv1, nrow=2)
  grob.main <- arrangeGrob(grob.top, grob.bottom, nrow=2, heights=c(1.2,1))
  grob.title <- textGrob(paste0(meta.loc$ihme_loc_id, " (", meta.loc$location_name, ")"), gp=gpar(fontsize=20, font=3))
  grob.full <- arrangeGrob(grob.title, grob.main, legend, nrow=3, heights=c(1, 15, 0.5))
 
  return(grob.full)

}

make.vacc <- function(df, loc) {

  ## Get location data
  vars <- c("location_name", "ihme_loc_id", "region_id", "level", "parent_id")
  meta.loc <- lapply(vars, function(x) locs[location_id==loc][[x]])
  names(meta.loc) <- vars

  ## Plot
  plots <- lapply(mes.vacc, function(x) plot.graph(df, loc, 3, x, "none"))
  names(plots) <- mes.vacc
  legend <- plot.graph(df, 102, 3, "vacc_dpt3", "bottom") %>% g_legend

  ## Arrange
  grob.top <- arrangeGrob(plots$vacc_dpt3, plots$vacc_mcv1, plots$vacc_bcg, plots$vacc_polio3, nrow=1)
  grob.bottom <- arrangeGrob(plots$vacc_hepb3, plots$vacc_hib3, plots$vacc_pcv3, plots$vacc_rotac, nrow=1)
  grob.main <- arrangeGrob(grob.top, grob.bottom, nrow=2)
  grob.title <- textGrob(paste0(meta.loc$ihme_loc_id, " (", meta.loc$location_name, ")"), gp=gpar(fontsize=20, font=3))
  grob.full <- arrangeGrob(grob.title, grob.main, legend, nrow=3, heights=c(1, 15, 0.5))

  return(grob.full)
}

make.vacc_full <- function(df, loc) {
  
  ## Get location data
  vars <- c("location_name", "ihme_loc_id", "region_id", "level", "parent_id")
  meta.loc <- lapply(vars, function(x) locs[location_id==loc][[x]])
  names(meta.loc) <- vars
  
  ## Plot
  mes <- c("vacc_full_nodelay", "vacc_full_2yr", "vacc_full_5yr", "vacc_full_10yr")
  plots <- lapply(mes, function(me) plot.vacc_full(df, loc, me, legend="none"))
  names(plots) <- mes
  legend <- plot.vacc_full(df, loc, "vacc_full_nodelay", legend="bottom") %>% g_legend
  
  ## Arrange
  grob.top <- arrangeGrob(plots$vacc_full_nodelay, plots$vacc_full_2yr, nrow=1)
  grob.bottom <- arrangeGrob(plots$vacc_full_5yr, plots$vacc_full_10yr, nrow=1)
  grob.main <- arrangeGrob(grob.top, grob.bottom, nrow=2)
  grob.title <- textGrob(paste0(meta.loc$ihme_loc_id, " (", meta.loc$location_name, ")"), gp=gpar(fontsize=20, font=3))
  grob.full <- arrangeGrob(grob.title, grob.main, legend, nrow=3, heights=c(1, 15, 0.5))
  
  
}

###########################################################################################################################
# 														run
###########################################################################################################################

args <- commandArgs(trailingOnly = TRUE)

if (length(args)!=0) {

###########################################################################################################################
  
  if (args[1]=="launch") {
    
    ## ARGUMENTS
    graph <- args[2]
    if (!is.na(args[3])) location_id <- args[3] else location_id <- get_location_hierarchy(149)[level>=3]$location_id
    output.path <- paste0(diagnostics_root, "/", graph, ".pdf")

    ## PREP DIRECTORIES
    temp.root <- paste0("/share/scratch/users/pyliu/graph/", graph)
    if (is.na(args[2])) unlink(temp.root, recursive=TRUE)
    dir.create(temp.root)
    
    ## LAUNCH
    for (loc in location_id) {
      job_name <- paste0("graph_", graph, "_", loc)
      script <- paste0(code_root, "/diagnostics/dashboard.r")
      slots <- 2
      memory <- 4
      cluster_project <- "proj_covariates"
      output <- paste0(temp.root, "/", loc, ".pdf")
      arguments <- c("graph", loc, output, graph)
      qsub(job_name=job_name, script=script, slots=slots, memory=memory, cluster_project=cluster_project, arguments=arguments)
    }

    ## HOLD
    job_hold(job_name=paste0("graph_", graph))
    
    ## APPEND
    locs.hierarchy <- get_location_hierarchy(149)[level>=3, .(location_id, region_id)]
    locs <- locs.hierarchy[order(region_id), location_id] %>% unique
    locs <- locs[locs %in% as.numeric(gsub(".pdf", "", list.files(temp.root, ".pdf")))]
    files <- gsub(",", "", toString(paste0(temp.root, "/", locs, ".pdf")))
    append_pdf(files, output.path)
    
    ## Clear
    unlink(temp.root)
    
    
  }
  
###########################################################################################################################

  if (args[1]=="graph") {
    ## Settings
    location_id <- args[2]
    output <- args[3]
    graph <- args[4]
    ## HSA
    if (graph=="hsa") {
      df <- load.data(mes.hsa.full)
      p <- make.hsa(df, location_id)
    }
    ## New Vacc
    if (graph=="vacc") {
      df <- load.data(mes.vacc)
      p <- make.vacc(df, location_id)
    }
    if (graph=="vacc_full") {
      df <- load.data(mes.vacc_full)
      p <- make.vacc_full(df, location_id)
    }
    ## PRINT
    pdf(output, w=25, h=10)
    grid.arrange(p)
    dev.off()
  }  
  
###########################################################################################################################

}





