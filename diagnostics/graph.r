###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Graph
###				
###########################################################

###################
### Setting up ####
###################

rm(list=ls())
library(data.table); library(dplyr); library(ggplot2)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Source
setwd(paste0(j, "/WORK/01_covariates/common/ubcov_central"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/utilitybelt/db_tools.r"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/cluster_tools.r"))
source(paste0(j, "/WORK/01_covariates/common/ubcov_central/functions/ubcov_tools.r"))

###########################################################################################################################
# 														function
###########################################################################################################################

plot_ts <- function(input, output=NULL, location_id=NULL, bundle=NULL, facet=NA) {
  
  ###########################################################################################################################
  # GENERAL GRAPH SETTINGS
  
  ## Load
  if (class(input)[1] == "character") {
    if (grepl(".csv", input)) df <- fread(input)
    if (grepl(".rds", input)) df <- readRDS(input)
  } else if (grepl("data", class(input)[1])) {
    df <- input
  }
  
  ## Setup
  obj.names <- c("data",  "outlier", "prior",    "st",      "gpr_mean", "gpr_mean_unraked")
  obj.labels <- c("Data",   "Outlier", "Prior",   "ST", 	  "GPR",      "GPR Unraked") 
  obj.colors <- c("black",  "black",   "#F2465A", "#1E90FF", "#218944",  "#218944")
  obj.df <- data.table(names=obj.names, labels=obj.labels, colors=obj.colors)
  
  ## Main Graphing Objects
  p.data <- geom_point(aes(y=data, x=year_id, color="Data"))
  p.data_ci <- geom_pointrange(aes(y=data, ymin=data-(1.96*sqrt(variance)), ymax=data+(1.96*sqrt(variance)), x=year_id), color=obj.df[names=="data"]$color)
  p.outlier <- geom_point(aes(y=outlier, x=year_id, color="Outlier"), shape=4)
  p.prior <- geom_line(aes(y=prior, x=year_id, color="Prior")) 
  p.st <- geom_line(aes(y=st, x=year_id, color="ST")) 
  p.gpr_mean <- geom_line(aes(y=gpr_mean, x=year_id, color="GPR"))
  p.gpr_ci <- geom_ribbon(aes(ymin=gpr_lower, ymax=gpr_upper, x=year_id), fill=obj.df[names=="gpr_mean"]$color, alpha=0.2)
  
  ## Other
  p.gpr_mean_unraked <- geom_line(aes(y=gpr_mean_unraked, x=year_id, color="GPR Unraked"))
  p.cv_intro <- geom_vline(aes(xintercept=cv_intro), color="black", linetype='longdash')
  
  ## Locations names
  locs <- get_location_hierarchy(149)[, .(location_id, location_name, ihme_loc_id, region_id, level, parent_id)]
  
  ###########################################################################################################################
  # DATA SPECIFIC SETTINGS
  
  ## Merge locs
  df <- merge(df, locs, by="location_id", all.x=T)
  
  ## Set locations
  if (is.null(location_id))  location_id <- unique(df$location_id)
  
  ## Set Sexes
  sex_id <- unique(df$sex_id)
  
  ## Set axis range
  y.min <- 0
  y.max <- 1
  x.min <- min(df[,year_id])
  x.max <- max(df[,year_id])
  
  ## Set variables to graph
  obj.graph <- intersect(obj.names, names(df))
  if ("variance" %in% names(df)) obj.graph <- c(obj.graph, "data_ci")
  if (all(c("gpr_upper", "gpr_lower") %in% names(df))) obj.graph <- c(obj.graph, "gpr_ci")
  if ("cv_intro" %in% names(df)) obj.graph <- c(obj.graph, "cv_intro")
  
  
  ###########################################################################################################################
  ##  START PLOT
  
  if (!is.null(output)) pdf(paste0(output), w=15, h=10)
  for (loc in location_id) {
  for (sex in sex_id) {
  ###########################################################################################################################
  # PLOT SPECIFIC SETTINGS
  
  ## Get location name, ihme_loc_id
  vars <- c("location_name", "ihme_loc_id", "region_id", "level", "parent_id")
  meta.loc <- lapply(vars, function(x) locs[location_id==loc][[x]])
  names(meta.loc) <- vars

  ## Data count
  if ("data" %in% names(df)) {
    count <- nrow(df[location_id==loc & sex_id==sex & !is.na(data)])
    r.count <- nrow(df[region_id==meta.loc$region & sex_id==sex & !is.na(data)])		
  } else {
    count <- 0
    r.count <- 0
  }
  
  ###########################################################################################################################
  # MAKE GRAPH
  
  ## Start graph
  p <- ggplot(df[location_id==loc & sex_id==sex])
  ## Graph objects
  for (obj in obj.graph)  p <- p + get(paste0("p.", obj))
  ## Region data
  if (r.count > 0) p <- p + geom_point(data=df[region_id==meta.loc$region & sex_id==sex & location_id != loc,], aes(y=data, x=year_id), color="grey60", alpha=0.5)
  ## Data
  if (count > 0) p <- p + p.data + p.data_ci
  ## Introduction year
  ## Graph settings
  ## Facet
  if(!is.na(facet)) p <- p + facet_wrap(facet, ncol=2)
    ## Colors
    p <- p + scale_color_manual(values=setNames(obj.colors, obj.labels)) +
    ## Appearance
    xlab("Year") + 
    coord_cartesian(ylim=c(y.min, y.max), xlim=c(x.min, x.max)) + 
    theme_bw()+ 
    theme(axis.title=element_text(),
        plot.title=element_text(face="bold",size=18, hjust = 0.5),
         strip.text=element_text(size=12, face ="bold"),
         strip.background=element_blank(),
         axis.text.x = element_text(size = 9),
         panel.margin=unit(1,"lines"),
         legend.position = "bottom",
         legend.title = element_blank(),
         legend.background = element_blank(),
         legend.key = element_blank()
    )+
  ## Title
  ggtitle(paste0(meta.loc$location_name, " (", meta.loc$ihme_loc_id, ")"))
    
  if (!is.null(output)) print(p)

  ##  END GRAPH
  ###########################################################################################################################
  }
  }

  ## Finish
  if (!is.null(output)) dev.off()  else return(p)
    
}

###########################################################################################################################
# 														run
###########################################################################################################################

args <- commandArgs(trailingOnly = TRUE)

if (length(args)!=0) {

###########################################################################################################################
  
  if (args[1]=="launch") {
    
    bundle <- args[[2]]
    location_id <- args[3]
    
    ## ARGUMENTS
    if (!is.na(args[3])) location_id <- args[3] else location_id <- get_location_hierarchy(149)[level>=3]$location_id
    output.path <- paste0(diagnostics_root, "/", bundle, ".pdf")

    
    ## PREP DIRECTORIES
    temp.root <- paste0("/share/scratch/users/pyliu/graph/", bundle)
    if (is.na(args[3])) unlink(temp.root, recursive=TRUE)
    dir.create(temp.root)
    
    ## LAUNCH
    for (loc in location_id) {
      job_name <- paste0("graph_", bundle, "_", loc)
      script <- paste0(code_root, "/diagnostics/graph.r")
      slots <- 2
      memory <- 4
      cluster_project <- "proj_covariates"
      output <- paste0(temp.root, "/", loc, ".pdf")
      arguments <- c("graph", bundle, loc, output)
      qsub(job_name=job_name, script=script, slots=slots, memory=memory, cluster_project=cluster_project, arguments=arguments)
    }

    ## HOLD
    job_hold(job_name=paste0("graph_", bundle))
    
    ## APPEND
    locs.hierarchy <- get_location_hierarchy(149)[level>=3, .(location_id, region_id)]
    locs <- locs.hierarchy[order(region_id), location_id] %>% unique
    locs <- locs[locs %in% as.numeric(gsub(".pdf", "", list.files(temp.root, ".pdf")))]
    files <- gsub(",", "", toString(paste0(temp.root, "/", locs, ".pdf")))
    append_pdf(files, output.path)
    
    
  }
  
###########################################################################################################################

  if (args[1]=="graph") {
    
    bundle <- args[2]
    location_id <- args[3]
    output <- args[4]
    
    path <- paste0(data_root, "/exp/modeled")
    if (grepl("#", bundle)) {
      bundle <- gsub("#", "", bundle)
      files <- list.files(path, pattern= bundle, full.names = TRUE)
    } else {
      files <- paste0(path, "/", bundle, ".rds")
    }
    
    df <- lapply(files, function(x) readRDS(x) %>% data.table) %>% rbindlist(., fill=TRUE)
    plot_ts(df, location_id=location_id, facet='me_name', output=output)
    
    
  }  
  
  
###########################################################################################################################

}





