###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Graph
###				
###########################################################

###################
### Setting up ####
###################

rm(list=ls())
pacman::p_load(data.table, dplyr, ggplot2, ggthemes, rje, rgdal, maptools, RColorBrewer)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Source
## Map function
source(paste0(code_root, "/diagnostics/map/global_map.R"))
source(db_tools)

## Shapefile
# load(paste0(j, "/DATA/SHAPE_FILES/GBD_geographies/master/GBD_2016/inset_maps/allSubs/GBD_WITH_INSETS_ALLSUBS_PREPPED.RData"))
# source(paste0(j, "/DATA/SHAPE_FILES/GBD_geographies/master/GBD_2016/inset_maps/allSubs/GBD_WITH_INSETS_MAPPING_FUNCTION.R"))
# map@data <- map@data %>% data.table

## Locations
locs <- get_location_hierarchy(location_set_version_id)[, .(ihme_loc_id, location_id)]

## Intro
intro <- paste0(data_root, "/exp/reference/vaccine_intro.rds") %>% readRDS %>% data.table

###########################################################################################################################
# 														function
###########################################################################################################################

args <- commandArgs(trailingOnly = TRUE)

if (length(args)!=0) {
###########################################################################################################################
## GRAPH

if (args[1]=="graph") {
  
  ## Settings
  me <- args[2]
  
  me <- "vacc_mcv2"
  disp <- "MCV2"
  df <- paste0(data_root, "/exp/modeled/best/", me, ".rds") %>% readRDS %>% data.table
  if ("ihme_loc_id" %in% names(df)) df$ihme_loc_id <- NULL
  df <- merge(df, locs, by='location_id', all.x=TRUE)
  if (!("cv_intro_years" %in% names(df))) df <- merge(df, intro, by=c("ihme_loc_id", "year_id", "me_name"), all.x=TRUE)
  df <- df[, gpr_mean := ifelse(cv_intro_years==0 & !is.na(cv_intro_years), NA, gpr_mean) ]
 
  output.path <- paste0(diagnostics_root, "/current/map/")
 
    ## Kelly Graph
  years <- c(1980, 1985, 1990, 1995, 2000, 2005, 2010, 2016)
  for (year in years) {
  png(paste0(output.path, "/", me, "_", year, ".png"), w=20, h=10, unit="in", res=100)
  global_map(data=df[year_id==year], 
                  map.var="gpr_mean", 
                  subnat=TRUE,
                  plot.title=paste0(disp, " ", year), 
                  scale="cont",
                  limits=c(0,1),
                  na.col="grey")
  dev.off()
  }
              
  
}
###########################################################################################################################
}


# ## Graph
# png(paste0(output.path, "_2000.png"), w=20, h=10, unit="in", res=100)
# wmap(chloropleth_map=map,
#      data=df,
#      geog_id="ID",
#      variable="mapvar",
#      map_title=me,
#      title_font_size=30,
#      legend_bar_width=.6,
#      override_scale=c(0,1),
#      histogram=FALSE,
#      legend_position="right",
#      series_dimension="year_id",
#      series_sequence=2000,
#      color_ramp=brewer.pal(7,"RdYlGn"))
# dev.off()
# 
# ## Introduction years
# intro <- intro[year_id==2016, .(ihme_loc_id, me_name, cv_intro)] %>% unique
# intro <- intro[cv_intro==9999, cv_intro := NA]
# intro <- intro[cv_intro>2016, cv_intro := NA]
# 
# output <- paste0(diagnostics_root, "/current/intro")
# 
# me <- "vacc_hib3"
# png(paste0(output, "/", me, ".png"), w=20, h=10, unit="in", res=100)
# global_map(data=intro[me_name==me],
#             map.var="cv_intro",
#            subnat=TRUE,
#            plot.title=paste0("Hib3 Introduction Dates"),
#            scale='cont',
#            limits=c(1985, 2016),
#            # limits=c(1985, 1990, 1995, 2000, 2005, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2020),
#            # labels=c("1985-1990", "1990-1995", "1995-2000", "2000-2005", "2005-2010", "2010", "2011", "2012", "2013", "2014", "2015", "2016"),
#            na.col="grey")
# dev.off()
# 
# me <- "vacc_pcv3"
# png(paste0(output, "/", me, ".png"), w=20, h=10, unit="in", res=100)
# global_map(data=intro[me_name==me],
#            map.var="cv_intro",
#            subnat=TRUE,
#            plot.title=paste0("PCV3 Introduction Dates"),
#            scale="cont",
#            limits=c(2000, 2016),
#            na.col="grey")
# dev.off()
# 
# me <- "vacc_rotac"
# png(paste0(output, "/", me, ".png"), w=20, h=10, unit="in", res=100)
# global_map(data=intro[me_name==me],
#            map.var="cv_intro",
#            subnat=TRUE,
#            plot.title=paste0("RotaC Introduction Dates"),
#            scale="cont",
#            limits=c(2000, 2016),
#            na.col="grey")
# dev.off()
# 
# me <- "vacc_mcv2"
# png(paste0(output, "/", me, ".png"), w=20, h=10, unit="in", res=100)
# global_map(data=intro[me_name==me],
#            map.var="cv_intro",
#            subnat=TRUE,
#            plot.title=paste0("RotaC Introduction Dates"),
#            scale="cont",
#            limits=c(2000, 2016),
#            na.col="grey")
# dev.off()
# 




