###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Data Coverage
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr, ggplot2)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Source
source(db_tools)

## Paths
root <- paste0(data_root, "/exp/to_model")
me.db <- paste0(code_root, "/reference/me_db.csv") %>% fread

## Location hierarchy
locs <- get_location_hierarchy(location_set_version_id)[level>=3][, .(ihme_loc_id, location_id, super_region_id, region_id)]
intro <- readRDS("J:/WORK/01_covariates/02_inputs/hsa/data/exp/reference/vaccine_intro.rds")

## Map function
source(paste0(code_root, "/diagnostics/map/global_map.R"))

##--FUNCTIONS-------------------------------------------------------

prep.coverage <- function(type="source", by=c("ihme_loc_id", "me_name", "cv_admin")) {
  ## Load
  df <- paste0(root, "/vaccination.rds") %>% readRDS %>% data.table
  df$location_id <- NULL
  ## Square
  loc_ids <- locs$ihme_loc_id
  square <- expand.grid(ihme_loc_id=loc_ids, me_name=unique(df$me_name), cv_admin=c(0, 1)) %>% data.table
  ## Merge data onto location hierarchy
  df <- merge(square, df, by=c("ihme_loc_id", "me_name", "cv_admin"), all.x=TRUE)
  ## Count by the by
  if (type=="source") df <- df[, count := length(unique(nid[!is.na(nid)])), by=by]
  if (type=="point") df <- df[, count := length(me_name[!is.na(data)|!is.na(cv_outlier)]), by=by]
  ## Subset
  df <- df[, c(by, "count"), with=F] %>% unique
  ## Set to NA if not introduced yet
  df <- merge(df, intro, by=c("ihme_loc_id", "me_name"), all.x=TRUE)
  df <- df[cv_intro==9999, count := NA]
  return(df)
}

map.coverage <- function(me, subnat=FALSE, admin=1) {
  df <- prep.coverage(type="point")[me_name==me][cv_admin==admin]
  me.disp <- me.db[me_name==me]$display_name
  a <- ifelse(admin==1, "Admin", "Survey")
  title <- paste0(me.disp, " Input Data Coverage (", a, ")")
  p <- global_map(data=df, 
           map.var="count", 
           subnat=subnat,
           plot.title=title, 
           limits=c(0, 1, 5, 20, 30, 50, 999), 
           na.col="grey",
           labels=c("No Data", "1-4 points", "5-19 points", "20-29 points", "30-49 points",  "50+ points"))
  return(p)
}

##-----------------------------------------------------------------


output <- paste0(diagnostics_root, "/current/coverage")
for (admin in c(0, 1)) {
  for (subnat in c(TRUE, FALSE)) {
    for (me in c("vacc_dpt3", "vacc_mcv1", "vacc_hib3", "vacc_pcv3", "vacc_rotac")) {
      a <- ifelse(admin==0, "survey", "admin")
      s <- ifelse(subnat==TRUE, "subnat", "nat")
      png(paste0(output, "/", me, "_", a, "_", s, ".png"), w=20, h=10, units="in", res=100)
      p <- map.coverage(me, subnat=subnat, admin=admin)
      dev.off()
    }
  }
  
}
