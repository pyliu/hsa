###########################################################
### Author: Patrick Liu (pyliu@uw.edu)
### Date: 
### Project: hsa
### Purpose: Clean Pubmed sheets and collate into a larger sheet for screening
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
pacman::p_load(data.table, dplyr, readxl)

os <- .Platform$OS.type
if (os == "windows") {
  j <- "J:/"
  h <- "H:/"
} else {
  j <- "/home/j/"
  user <- Sys.info()[["user"]]
  h <- paste0("/snfs2/HOME/", user)
}

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

## Suppress messages on readxl
read_excel <-  function(...) {
  quiet_read <- purrr::quietly(readxl::read_excel)
  out <- quiet_read(...)
  if(length(c(out[["warnings"]], out[["messages"]])) == 0)
    return(out[["result"]])
  else readxl::read_excel(...)
}


###########################################################################################################################
# 														
###########################################################################################################################

date <- "Y2017M03D07"

## Set root
root <- paste0(data_root, "/rr/reviews/pubmed/", date)

## Collate screening sheets

#--REVIEW------------------------------------------------------------
files <- list.files(paste0(root, "/clean/review"), full.names=TRUE)
df.review <- lapply(files, function(x) {
				sheet <- read_excel(x)
				sheet <- data.table(sheet)
				me_name <- basename(x) %>% gsub("pubmed_result_", "", .) %>% gsub(".xls", "", .)
				sheet <- sheet[, me_name := me_name]
				return(sheet)
			}) %>% rbindlist

review <- df.review[,.(me_name, PMID)] %>% unique
review <- review[, review := 1]

#--FULL SET----------------------------------------------------------
files <- list.files(paste0(root, "/clean/expanded"), full.names=TRUE)
df <- lapply(files, function(x) {
  sheet <- read_excel(x)
  sheet <- data.table(sheet)
  me_name <- basename(x) %>% gsub("pubmed_result_", "", .) %>% gsub(".xls", "", .)
  sheet <- sheet[, me_name := me_name]
  return(sheet)
}) %>% rbindlist

## Tag full hits with those that were tagged as reviews/meta-analyses
df <- merge(df, review, by=c("me_name", "PMID"), all.x=TRUE)
df <- df[is.na(review), review := 0]

#--CLEAN-------------------------------------------------------------

## Create new columns
cols <- c("title_screen", "title_exclusion", "abstract_screen", "abstract_exclusion", "fulltext_screen", "fulltext_exclusion")
df <- df[, (cols) := NA]
df <- df[, search_date := date]

## Save
write.csv(df, paste0(root, "/screening_sheet.csv"), na="", row.names=F)




