###########################################################
### Author: Patrick Liu
### Date: 10/19/2016
### Project: hsa
### Purpose: Update screening sheet after pulling from cooper
###				
###########################################################

###################
### Setting up ####
###################
rm(list=ls())
library(data.table)
library(dplyr)

## Initialize
source(paste0(j, '/WORK/01_covariates/02_inputs/hsa/code/init.r'))

get_con <- function(dbname, host) {
	con <- suppressWarnings(src_mysql(dbname = dbname, host = host, user = "dbview", password = "E3QNSLvQTRJm"))
	return(con)
}

run_query <- function(dbname, host, query) {
	con <- get_con(dbname, host)
	con %>% tbl(sql(query)) %>% collect(n=Inf) %>% data.table
}

get_nids <- function() {
	dbname <- "shared"
	host   <- "modeling-cod-db.ihme.washington.edu"
	query <- "SELECT * FROM shared.mv_nid_file"
	df <- suppressWarnings(run_query(dbname, host, query))
	return(df)
}

nids <- get_nids()
## Foward slashes
nids <- nids[, file_location := gsub("[\\]", "/", file_location)]
## Get unique by nid file_loc
nids <- nids[, .(nid, file_location)] %>% unique

###########################################################################################################################
# 														functions
###########################################################################################################################

load.file <- function(file, db) {
	df <- fread(file)
	proj.nid <- unique(df$project_nid)
	me_short <- db[nid==proj.nid]$me_short
	df <- df[, (me_short) := 1]
	## Remove project ids
	df <- df[, c("project_nid", "project_title") := NULL]
	## Set acceptance status
	df <- df[, acceptance_status := "To be reviewed"]
	return(df)
}

###############################################
# Collate Cooper
#
# Function to pull the most updated 
# hits from cooper (in the sourcing folder)
# and combine to make a sheet ready for
# upload to the google drive
###############################################

collate_cooper <- function() {

##########
# Setup
##########

## Files
cooper.projects <- paste0(code_root, "/sourcing/cooper_projects.csv") %>% fread
cooper.downloads <- paste0(sourcing_root, "/cooper_download")

## Find most recent download
dates <- list.files(cooper.downloads)
dates.clean <- as.Date(dates, format="%m-%d-%Y")
i <- which(dates.clean==max(dates.clean))
folder <- paste0(cooper.downloads, "/", dates[i])
print(paste0("Most recent download: ", max(dates.clean)))

## Load files
file.paths <- list.files(paste0(folder, "/raw"), full.names=TRUE)
files <- lapply(file.paths, function(x) load.file(x, cooper.projects))

## Check if number of files = number of me
if (length(files) != nrow(cooper.projects)) print("STAHHHHHHHHHP")

################
# Merge files
################

## Common cols
cols <- intersect(names(files[[1]]), names(files[[2]]))

## Create an appended list
all <- files %>% rbindlist %>% unique(., by="nid")
all <- all[, c("_all") := NULL]

## Merge the flags
for (i in 1:length(files)) {
	sub <- files[[i]][, c("nid", setdiff(names(files[[i]]), cols)), with=F]
	if (i == 1) df <- sub
	if (i != 1) df <- merge(df, sub, by="nid", all=TRUE)
}

## Set 0 if not 1
for (i in names(df)) df <- df[is.na(get(i)), (i) := 0]

## Merge onto all
all <- merge(all, df, by="nid", all=TRUE)
setcolorder(all, c(cols, cooper.projects$me_short))

################
# Get Paths
################
all <- merge(nids,  all, by="nid", all.y=T)

## Save
write.csv(all, paste0(folder, "/combined.csv"), row.names=FALSE, na="")
print(paste0("Saved in  ", paste0(folder, "/combined.csv")))

## Return the path to the new sheet
return(paste0(folder, "/combined.csv"))

}


